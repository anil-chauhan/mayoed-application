/* functions used all across the theme */

function initSelect2(id, url) {
    $(id).select2({
        delay: 250, // The number of milliseconds to wait for the user to stop typing before
        width: '100%',
        theme: 'material',
        minimumInputLength: 7,
        placeholder: "Search for a patient",
        dataType: 'json',
        ajax: {
            url: url,
            data: function (term, page) {
                return {
                    q: term, // search term
                    page: page, // page number
                    page_limit: 10
                }
            },
            processResults: function (data, page) {
                obj = $.parseJSON(data);

                if (!obj.length || !obj[0].inPas) {
                    displayPatientDialog(this.$element);
                    //return empty results
                    obj = [];
                }
      
                return {results: obj};
            }
        },
        escapeMarkup: function (m) {
            return m;
        }
    });
}

function displayPatientDialog(select2Input) {
    //disable select2 ( which also hides the dropdown )
    select2Input.prop("disabled", true);

    swal({
        title: "Patient was not found in PAS",
        type: 'warning',
        html: '<p style="margin-bottom: 20px;">The patient you searched for was no found. Please input his name below, or click cancel and try to search again!</p>' +
            '<div class="col-sm-8 col-sm-offset-2">' +
                '<div class="form-group">' +
                    '<div class="fg-line">' +
                        '<input type="text" id="swal-select2-input-field" class="input-small form-control fg-input" name="swal-select2-input-field" placeholder="Input Patient Name">' +
                    '</div>' +
                '</div>' +
            '</div>',
        showCancelButton: true,
        closeOnConfirm: false,
        allowOutsideClick: false
    }, function (confirmed) {

        patientName = $('#swal-select2-input-field').val();
        if(!confirmed  || !patientName){
            if(confirmed && !patientName) {
                swal({
                    html: "Patient name can't be blank!",
                    type: 'error'
                });
            }

            $(".select2-hidden-accessible").prop("disabled", false);
            return;
        }

        //show the hidden field and set its value
        toggleSelect2SwapperInput(patientName);

        swal({
            html: 'Patient name set as <strong>' + patientName + '</strong>',
            type: 'success'
        });

    });

    setTimeout(function(){
        $("#swal-select2-input-field").focus();
    },500);
}

function toggleSelect2SwapperInput(val){

    //hide the disabled select2's container/row
    $('.select2-container--disabled').closest('.form-group').hide();

    input = $("#select2Swapper").focus();
    input.removeAttr('disabled');
    setTimeout(function(){
        input.focus();
        input.val(val);
    },250);

    container = $("#select2SwapperContainer");
    container.removeClass('hidden');
}


$(document).ready(function(){
    $('#episodeModal').on('click', '.focused-row', function() {
        $('#patienttreatments-consultant_code').val($(this).data('ecode'));
        $('#consultant_code_text').html($(this).data('ecode'));
        
        $('#patienttreatments-episode_number').val($(this).data('enum'));
        $('#episode_num_text').html($(this).data('enum'));
        
        $('#patienttreatments-admission_date').val($(this).data('edate'));
        $('#admission_date_text').html($(this).data('edate'));
        
        $('#patienttreatments-sp_db').val($('#patienttreatments-sp_db_temp').val());
        $('#sp_db_text').html($('#patienttreatments-sp_db_temp').val());
        
        $('#patienttreatments-cons_speciality').val($('#patienttreatments-cons_speciality_temp').val());
        $('#cons_speciality_text').html($('#patienttreatments-cons_speciality_temp').val());
        
        $('#patienttreatments-board_number').val($('#patienttreatments-board_number_temp').val());
        $('#board_number_text').html($('#patienttreatments-board_number_temp').val());
        
        /*$('#clinic_name').html($(this).data('cname'));
        $('#clinic_num').html($(this).data('cnum'));
        $('#clinic_speciality').html($('#patientschedule-clinic_speciality').val());*/
        
        jQuery('#episodeModal').modal('hide');
        $('.episodes').addClass('hide');
        $('.episode_1').removeClass('hide');
        $('.episode_detail').css('display', '');
        $('#episode_button').html('Change Episode');
        jQuery('#episode_list').html('');
    });
});

    
function toggleForm(element){
    customValidator(element);
}

function customValidator(element) {
    currentElement = element;
    var requiredFiels = $(element).data('required');
    if(requiredFiels) {
        console.log(requiredFiels);
        if(requiredFiels == 'sp_db_temp') {
            if(!$('#patienttreatments-sp_db_temp option:selected').val()){
                $('.field-patienttreatments-sp_db_temp').addClass('has-error').find('.help-block').html('Database cannot be blank.')
                validatorCallBack(element, false);
            } else {
                console.log('s');
                $('.field-patienttreatments-sp_db_temp').removeClass('has-error').find('.help-block').html('');
                validatorCallBack(element,  true);
            }
            
        }

        if(requiredFiels == 'cons_speciality_temp') {
            if(!$('#patienttreatments-cons_speciality_temp').val()){
                $('.field-patienttreatments-cons_speciality_temp').addClass('has-error').find('.help-block').html('Please select consultant speciality');
                validatorCallBack(element, false);
            } else {
                $('.field-patienttreatments-cons_speciality_temp').removeClass('has-error').find('.help-block').html('');
                validatorCallBack(element,  true);
            }
        }
        
        if(requiredFiels == 'board_number_temp') {
            if(!$('#patienttreatments-board_number_temp').val()){
                $('.field-patienttreatments-board_number_temp').addClass('has-error').find('.help-block').html('Please select board number');
                validatorCallBack(element, false);
            } else {
                $('.field-patienttreatments-board_number_temp').removeClass('has-error').find('.help-block').html('');
                getEpisodeData();
                validatorCallBack(element,  true);
            }
        }
    }
    
    if(!requiredFiels) {
        validatorCallBack(element,  true);
    }
}

function validatorCallBack(element, validate){
    if(validate) {
        var showId = $(element).data('show_id');
        $('.episodes').addClass('hide');
        $('.' + showId).removeClass('hide');
    }
}

function getEpisodeData() {
    jQuery.ajaxSetup({async:true});
                
    $.post(episodeDetailUrl, {
        spDb           : $('#patienttreatments-sp_db_temp').val(),
        consSpeciality : $('#patienttreatments-cons_speciality_temp').val(),
        boardNumber    : $('#patienttreatments-board_number_temp').val()
    },
    function(data) { 
        var html = JSON.parse(data);
        jQuery('#episode_list').html(html.data);
    });
    jQuery.ajaxSetup({async:false});
}