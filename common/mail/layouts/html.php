<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>
    </head>
    <body style="margin:0; padding:0;">
<?php $this->beginBody() ?>

        <table style="border-collapse:collapse; background-color:#cecece;width:100%;border:0px;border-spacing:0">
            <tr >
                <td class="container" style="padding-left:15px; padding-right:15px;">

                    <table  style="margin:auto;border:0px;border-collapse:collapse; text-align:left; border-spacing:0;width:610px;background-color:#ffffff; max-width:100%;">
                        <tr >
                            <td colspan="2" style="background-color:#cecece;width:100%;padding-top:30px; padding-bottom:5px;">

                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0;width:50%;height:5px;vertical-align:top;background-color:#296bc6;font-size:2px; line-height:0px;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderTopLeft.png"  style="width:5px;height:5px;border:0;display:inline-block; margin:0;" /></td>
                            <td style="padding:0;width:50%;height:5px;vertical-align:top;background-color:#296bc6;font-size:2px; line-height:0px;text-align:right;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderTopRight.png"   style="width:5px;height:5px;border:0;display:inline-block; margin:0;" /></td>
                        </tr>
                        <tr>
                            <td  colspan="2" style="width:100%;padding:0;">
                                <table style="width:100%;border-collapse:collapse; text-align:left; border-spacing:0; max-width:100%; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:100%; color:#777777;">
                                    <tr>
                                        <td  style="width:40%;padding:0;">

                                            <!--<img alt="Logo" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/logo.png" style="display:block; max-width:100%; height:auto !important;margin:0;border:0;" />-->

                                        </td>

                                        <td  style="width:60%;padding:0;font-family:OpenSans, Arial, Helvetica, sans-serif; font-size:25px; line-height:100%; text-align:center; color:#4a4a4b;" >
                                            Hello <?php echo $this->params['userName'];?>,
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"  style="width:100%;vertical-align:top;background-color:#cecece;padding-bottom:25px; font-size:2px; line-height:0px; text-align:center;"></td>
                        </tr>
                    </table>

                    <table   style="margin:auto;border:0;width:610px;background-color:#ffffff;border-collapse:collapse; text-align:left; border-spacing:0; max-width:100%;">
                        <tr>
                            <td  style="padding:0;vertical-align:top;height:5px;width:50%;font-size:2px; line-height:0px;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderTopLeft2.png"   style="border:0;width:5px;height:5px;display:inline-block; margin:0;" /></td>
                            <td  style="padding:0;vertical-align:top;height:5px;width:50%;font-size:2px; line-height:0px;text-align:right;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderTopRight2.png"   style="border:0;width:5px;height:5px;display:inline-block; margin:0;" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width:100%;padding-top:15px; padding-right:30px; padding-left:30px;">
                                <p style="margin-top:0px; margin-bottom:10px !important; padding-top:0px; font-family:'Segoe UI', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:18px; line-height:22pt; font-weight:normal;color:#296BC6; ">
                                    <span  style="font-weight:bold;">Referral Assigned!</span>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width:100%;padding-right:30px; padding-bottom:10px; padding-left:30px;">
                                <p style="margin-top:0px; margin-bottom:10px !important; padding-top:0px; padding-bottom:10px; font-family:'Segoe UI', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:14px; font-weight:normal; ">
                                    <?= $content ?>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width:100%;padding-right:30px;padding-bottom:40px;  padding-left:30px;">
                                <p style="margin-top:0px; margin-bottom:10px !important; padding-top:0px; padding-bottom:10px; font-family:'Segoe UI', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:14px; font-weight:normal; ">
                                    Thanks, <br/><span style="color:#296BC6;font-weight:bold;">CONNOLLY</span>
                                </p>
                            </td>
                        </tr>

                        <tr>
                            <td  style="padding:0;vertical-align:bottom;width:50%;height:5px;font-size:2px; line-height:0px;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderBottomLeft2.png"  style="width:5px;height:5px;display:inline-block; margin:0;border:0;" /></td>
                            <td  style="padding:0;vertical-align:bottom;width:50%;height:5px;font-size:2px; line-height:0px;text-align:right;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderBottomRight2.png"  style="width:5px;height:5px;display:inline-block; margin:0;border:0;" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="vertical-align:top;background-color:#cecece;width:100%;padding-bottom:20px; font-size:2px; line-height:0px; text-align:center;"><img alt="Shadow" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/shadow_610.png"  style="width:100% !important; height:10px !important;border:0;margin:0;" /></td>
                        </tr>
                    </table>          
                    <table style="margin:auto;border:0;background-color:#ffffff;width:610px;border-collapse:collapse; text-align:left; border-spacing:0; max-width:100%;">
                        <tr>
                            <td  style="padding:0;vertical-align:top;height:5px;width:50%;font-size:2px; line-height:0px;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderTopLeft2.png"   style="border:0;width:5px;height:5px;display:inline-block; margin:0;" /></td>
                            <td  style="padding:0;vertical-align:top;height:5px;width:50%;font-size:2px; line-height:0px;text-align:right;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderTopRight2.png"   style="border:0;width:5px;height:5px;display:inline-block; margin:0;" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="width:100%;padding-top:23px; padding-right:0; padding-bottom:20px; padding-left:30px;font-family:'Segoe UI', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:24px; line-height:100%;  font-weight:300;">
                                <a style="text-decoration:none;color:#000;margin-right: 180px;" href="#"></a>
                            </td>
                        </tr>
                        <tr>
                            <td  style="padding:0;vertical-align:bottom;background-color:#296bc6;height:5px;width:50%;font-size:2px; line-height:0px;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderBottomLeft.png"  style="height:5px;width:5px;display:inline-block; margin:0;border:0;" /></td>
                            <td  style="padding:0;vertical-align:bottom;background-color:#296bc6;height:5px;width:50%;font-size:2px; line-height:0px;text-align:right;"><img alt="Border" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/borderBottomRight.png"  style="height:5px;width:5px;display:inline-block; margin:0;border:0;" /></td>
                        </tr>
                        <tr>
                            <td colspan="2"  style="padding:0;vertical-align:top;background-color:#cecece;width:100%;padding-bottom:20px; font-size:2px; line-height:0px; text-align:center;"><img alt="Shadow" src="<?php echo $_SERVER['SERVER_NAME'] . '/' . Yii::$app->homeUrl?>www/mails/shadow_610.png"  style="width:100% !important; height:10px !important;border:0;margin:0;" /></td>
                        </tr>
                        <tr>
                            <td  colspan="2" style="background-color:#cecece;width:100%;padding:0px 10px 25px 10px; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:13pt; color:#000; text-align:center; -webkit-text-size-adjust:none;">
                                © Copyright <?php echo date('Y')?> CONNOLLY<br/>
                            </td>
                        </tr>
                    </table>        
                </td>
            </tr>
        </table>


        
<?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
