<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StandardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['userName'] = $contactName;
?>

<p>
    A new referral has been assigned to you from the Connolly Platform.
</p>
<p>
    <b>Date</b>: <?php echo $date?>
</p>
<p>
    <b>Subject</b>: <?php echo $subject?>
</p>
<p>
    <b>Comment</b>: <?php echo $comment?>
</p>
<h2>Patient Information</h2>
<p>
    <b>Name</b> : <?php echo $patientName?>
</p>
<p>
    <b>Phone</b>: <?php echo $patientEmail?>
</p>
<p>
    <b>Email</b>: <?php echo $patientPhone?>
</p>
