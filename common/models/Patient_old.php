<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use common\components\ArrayHelpers;
use common\models\PatientReferral;
use common\models\Logs;
use common\models\CoordinationCode;


/**
 * This is the model class for table "patient".
 *
 * @property int $Patient_id
 * @property string $patient_name
 * @property string $board_number
 * @property string $Tracker_admission_ed_time
 * @property string $surname
 * @property string $forename
 * @property int $age
 * @property string $current_bed_code
 * @property string $cons_name_current
 * @property string $dob
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 * @property string $episode_number
 * @property string $address
 * @property int $is_deleted
 * @property string $ed_stream
 * @property int $ed_location
 * @property int $ed_doctor
 * @property int $ed_nurse
 * @property string $sex
 * @property string $Tracker_timer
 * @property string $code_name
 * @property int $is_awaiting_admission
 * @property string $referral_type
 * @property string $coordination_code
 * @property string $coordination_code_order
 * @property string $current_ward_code
 * @property int $is_discharge
 * @property int $is_paed_minor
 * @property string $infection_status
 * @property string $Tracker_awaiting_bed_time
 * @property string $phone
 * @property string $mobile_number
 * @property int $is_triage
 * @property string $AEModeOfArrivalCode
 * @property int $rpat_active
 * @property string $Tracker_rpat_time
 * @property string $PASAEAttendanceDate
 * 
 * @property EdDoctor $edDoctor
 * @property EdLocation $edLocation
 * @property EdNurse $edNurse
 * @property Logs[] $logs
 * @property PatientReferrals $patientReferrals
 */
class Patient extends \yii\db\ActiveRecord
{
    public $code;
    public $ed_nurse_from;
    public $ed_nurse_to;
    public $commentUpdate;
    public $comment_code;
    public $discharge_code;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patients';
    } 

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date("Y-m-d H:i:s", strtotime('now')),
            ],
        ];
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [       
                [
                    'is_deleted', 'ed_location',
                    'ed_doctor', 'ed_nurse', 'is_awaiting_admission', 'is_discharge',
                     'is_triage',  'is_rpat' ,'referral_type'
                ],
                'integer'
            ],
            [
                [
                    'patient_name', 'board_number', 'surname', 'forename',
                    'current_bed_code', 'cons_name_current', 'comment', 'episode_number',
                    'address', 'sex',
                    'coordination_code','current_ward_code', 
                    'infection_status',  'PhoneNumber', 'Mobile', 'AEModeOfArrivalCode',
                    'created_at', 'updated_at', 'Tracker_awaiting_bed_time', 'Tracker_rpat_time',
                    'PASAEAttendanceDate'
                ],
                'string'
            ],
            [
                [
                    'Tracker_timer','Tracker_admission_ed_time','dob','ed_stream'
                ],
                'safe'
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Patient_id' => 'ID',
            'patient_name' => 'Patient Name',
            'board_number' => 'Board Number',
            'Tracker_admission_ed_time' => 'Admission Date',
            'surname' => 'Surname',
            'forename' => 'Forename',
            'age' => 'Age',
            'current_bed_code' => 'Current Bed Code',
            'cons_name_current' => 'Cons Name Current',
            'dob' => 'Dob',
            'comment' => 'Comment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'episode_number' => 'Episode Number',
            'address' => 'Address',
            'is_deleted' => 'Is Deleted',
            'ed_stream' => 'Ed Stream',
            'ed_location' => 'Ed Location',
            'ed_doctor' => 'Ed Doctor',
            'ed_nurse' => 'Ed Nurse',
            'sex' => 'Sex',
            'Tracker_timer' => 'Timer',
            'code_name' => 'Code Name',
            'is_awaiting_admission' => 'Is Awaiting Admission',
            'referral_type' => 'Referral Type',
            'coordination_code' => 'Coordination Code',
            'coordination_code_order' => 'Coordination Code Order',
            'current_ward_code' => 'Current Ward Code',
            'is_discharge' => 'Is Discharge',
            'is_paed_minor' => 'Is Paed Minor',
            'infection_status' => 'Infection Status',
            'Tracker_awaiting_bed_time' => 'Bed Awaiting Time',
            'phone' => 'Phone',
            'mobile_number' => 'Mobile Number',
            'is_triage' => 'Is Triage',
            'AEModeOfArrivalCode' => 'Ae Mode Of Arrival',
            'rpat_active' => 'Rpat Active',
        ];
    }
    
    public function getEdDoctor() {
        return $this->hasOne(EdDoctor::className(), ['id' => 'ed_doctor']);
    }

    public function getEdNurse() {
        return $this->hasOne(EdNurse::className(), ['id' => 'ed_nurse']);
    }

    public function getEdLocation() {
        return $this->hasOne(EdLocation::className(), ['id' => 'ed_location']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs() {
        return $this->hasMany(Logs::className(), ['board_number' => 'board_number'])->orderBy(['created_at' => SORT_DESC]);
    }
    public static function getActivePatients($type = NULL) {
        $query = self::find()->where(['is_deleted' => 0]);
        $returnData = NULL;
        
        switch ($type) {
            case 'array':
               $returnData = $query->asArray()->all();
                break;
            case 'board':
                $modelData = $query->asArray()->all();
                $returnData = ArrayHelper::map($modelData, 'board_number', 'board_number');
                break;
            default:
               $returnData = $query->all();
                break;
        }
        return $returnData;
    }
    
    public static function countInfoEd() { 
        $data = [
            'edPatients'                => 0,
            'awaitingTriage'            => 0,
            'awaitingTriageAmbulance'   => 0,
            'resus-major'               => 0,
            'ran-rat'                   => 0,
            'ambulatory'                => 0,
            'paediatrics'               => 0,
            'mau_waiting'               => 0,
            'awaitingBed'               => 0,
            'red'                       => 0,
            'orange'                    => 0,
            'green'                         => 0,
            'yellow'                    => 0,
            'blue'                      => 0,
            'bedsAvailable'             => 0,
            'patients_under_16'         => 0,
            'longestInEd'   => strtotime('now'),
            'longestInPab'   => strtotime('now'),
            '0-4'   => 0,
            '4-8'   => 0,
            '>8'   => 0,
            '75+p>9'   => 0,
            'C1' => 0,
            'C2' => 0,
            'C3' => 0,
            'C4' => 0,
            'C5' => 0,
            'M1' => 0,
            'M2' => 0,
            'M3' => 0,
            'M4' => 0,
            'M5' => 0,
            'NC' => 0,
            '75+p>24' => 0,
            '>9' => 0,
            '>24' => 0,
            'not_coord' => 0,
            '75+p' => 0
        ];
       
        $selectedFields = [
            'coordination_code',
            'ed_stream',
            'is_mau',
            'is_awaiting_admission',
            'age',
            'PASAEAttendanceDate',
            'created_at',
            'Tracker_allocated_bed_time',
            "COALESCE(coordination_code,'') as coordination_code",
        
        ];
        
       
        $models = Patient::find()->select($selectedFields)->where(['PASAEDischargeDate' => '1900-01-01 00:00:00.000'])->all();
        if (!empty($models)) { 
            foreach ($models as $model) {
                $data['edPatients'] ++;
                
                if (!trim($model->coordination_code)) {
                    $data['awaitingTriage'] ++;
                    if ($model->AEModeOfArrivalCode == 'A') {
                        $data['awaitingTriageAmbulance'] ++;
                    }
                } else {
                    $PASAEAttendanceDate = strtotime($model->PASAEAttendanceDate);
                    $coordOrder = CoordinationCode::getCoordinationCodeOrder($model->coordination_code);
                    $data[ $coordOrder ? $model->coordination_code : 'not_coord'] ++;
                    
                    if (isset(Yii::$app->params['streamDataCount'][$model->ed_stream])) {
                        $data[Yii::$app->params['streamDataCount'][$model->ed_stream]] ++;
                    }

                    if ($model->is_mau == '1') {
                        $data['mau_waiting'] ++;
                    }
                    if ($model->is_awaiting_admission == '1') {
                        $data['awaitingBed'] ++;
                    }
                    if ($model->Tracker_allocated_bed_time && $model->Tracker_allocated_bed_time < $data['longestInPab']) {
                        $data['longestInPab'] = $model->Tracker_allocated_bed_time;
                    }
                    if ($model->age < '16') {
                        $data['patients_under_16'] ++;
                    }
                    if ( $PASAEAttendanceDate < $data['longestInEd']) {
                        $data['longestInEd'] = $PASAEAttendanceDate; 
                    }
                    $time = (strtotime("now") - $model->created_at) / 3600;
                    if ($time < 4) {
                        $data['0-4'] ++;
                    } else if ($time < 8) {
                        $data['4-8'] ++;
                    } else {
                        $data['>8'] ++;
                    }

                    if ($model->age > 75) {
                        if ($time > 9 && $time < 24) {
                            $data['75+p>9'] ++;
                            $data['>9'] ++;
                        } else if ($time > 24) {
                            $data['75+p>24'] ++;
                            $data['>24'] ++;
                        }
                        $data['75+p'] ++;
                    } else {
                        if ($time > 9 && $time < 24) {
                            $data['>9'] ++;
                        } else if ($time > 24) {
                            $data['>24'] ++;
                        }
                    }
                    if ($model->coordination_code) {
                        
                        $codeColor = CoordinationCode::getCoordinationCodeOrderColor($model->coordination_code);
             
                        if (isset($data[$codeColor])) {
                            $data[$codeColor] ++;
                        } 
                    }
                }
            }
        }
       
       return $data; 
    }
    
    public function getAge($dob, $age = '') {
        $current = strtotime("now");

        $val = (int) abs(($current - $dob) / (60 * 60 * 24 * 30));
        $years = (int) ($val / 12);
        $months = $val % 12;

        if ($years < 16) {
            $years = ($years < 10) ? '0' . $years : $years;
            $months = ($months < 10) ? '0' . $months : $months;

            return $years . ' : ' . $months;
        }
        return $age;
    }

    public static function countMailInfoEd() {
         $data['edPatients'] = 0;
        $data['awaitingTriage'] = 0;
        $data['awaitingTriageAmbulance'] = 0;
        $data['patients_under_16'] = 0;
        $data['awaitingDischarge'] = 0;
        
        $data['pnab'] = 0;
        $data['pnab_p>9'] = 0;
        $data['pnab_p>24'] = 0;
        $data['pnab_>75'] = 0;
        $data['pnab_75+p>9'] = 0;
        $data['pnab_75+p>24'] = 0;
        $data['pnab_0-4'] = 0;
        $data['pnab_4-8'] = 0;
        $data['pnab_>8'] = 0;
        $data['pnab_longest'] = strtotime('now');
        $data['longestInEd'] = strtotime('now');
        
        $data['pab'] = 0;
        $data['pab_p>9'] = 0;
        $data['pab_p>24'] = 0;
        $data['pab_>75'] = 0;
        $data['pab_75+p>9'] = 0;
        $data['pab_75+p>24'] = 0;
        $data['pab_0-4'] = 0;
        $data['pab_4-8'] = 0;
        $data['pab_>8'] = 0;
        $data['pab_longest'] = strtotime('now');
        
        
        $data['C1'] = 0;
        $data['C2'] = 0;
        $data['C3'] = 0;
        $data['C4'] = 0;
        $data['C5'] = 0;
        $data['M1'] = 0;
        $data['M2'] = 0;
        $data['M3'] = 0;
        $data['M4'] = 0;
        $data['M5'] = 0;
        $data['OtherCoordCode'] = 0;

        $selectedFields = [
            'coordination_code',
            'ed_stream',
            'is_mau',
            'is_awaiting_admission',
            'age',
            'PASAEAttendanceDate',
            'created_at',
            'Tracker_allocated_bed_time',
            'coordination_code',
            'is_triage',
            'Tracker_awaiting_bed_time'
        ];  
        
        $models = Patient::find()->select($selectedFields)->where(['not', ['is_discharge' => 1]])->orWhere(['is_discharge' => null])->all();
        $data['awaitingDischarge'] = Patient::find()->select(['Patient_id'])->where(['is_discharge' => 1])->count();
        if (!empty($models)) {
            foreach ($models as $model) {
                //$spData = json_decode($model->sp_object, true);
                
                $data['edPatients'] ++;//***
                if ($model->is_triage) {
                    $data['awaitingTriage'] ++;//***
                    if (trim($model->AEModeOfArrivalCode) == 'A') {
                        $data['awaitingTriageAmbulance'] ++;//***
                    }
                } else {
                    $PASAEAttendanceDate = $model->Tracker_timer;
                    $flagIsBed = ($model->is_bed_allocated) ? true : false;
                    
                    if(trim($model->coordination_code) !='NC'){
                            if(isset($data[$model->coordination_code])){
                            $data[isset($model->coordination_code) ? $model->coordination_code : 'not_coord'] ++;
                            }
                    } else {                    
                        $data['OtherCoordCode']++;
                    }
                    
                    if ($model->age < '16') {
                        $data['patients_under_16'] ++;
                    }
                    
                    if ($PASAEAttendanceDate < $data['longestInEd']) {
                        $data['longestInEd'] = $PASAEAttendanceDate;
                    }
                    
                    $time = (strtotime("now") - $model->created_at) / 3600;
                    
                    if ($flagIsBed) {
                        $data['pnab'] ++;
                        if ($model->age > 75) {
                            $data['pnab_>75'] ++;
                            if ($time > 9 && $time < 24) {
                                $data['pnab_75+p>9'] ++;
                            } else if ($time > 24) {
                                $data['pnab_75+p>24'] ++;
                            }
                        } else {
                            if ($time > 9 && $time < 24) {
                                $data['pnab_p>9'] ++;
                            } else if ($time > 24) {
                                $data['pnab_p>24'] ++;
                            }
                        }
                        if ($time < 4) {
                            $data['pnab_0-4'] ++;
                        } else if ($time < 8) {
                            $data['pnab_4-8'] ++;
                        } else {
                            $data['pnab_>8'] ++;
                        }
                        if ($PASAEAttendanceDate && $PASAEAttendanceDate < $data['pnab_longest']) {
                            $data['pnab_longest'] = $PASAEAttendanceDate;
                        }
                    }  else {
                        $data['pab'] ++;                  
                        if ($model->age > 75) {
                            $data['pab_>75'] ++;
                            if ($time > 9 && $time < 24) {
                                $data['pab_75+p>9'] ++;
                            } else if ($time > 24) {
                                $data['pab_75+p>24'] ++;
                            }
                        } else {
                            if ($time > 9 && $time < 24) {
                                $data['pab_p>9'] ++;
                            } else if ($time > 24) {
                                $data['pab_p>24'] ++;
                            }
                        }
                        if ($time < 4) {
                            $data['pab_0-4'] ++;
                        } else if ($time < 8) {
                            $data['pab_4-8'] ++;
                        } else {
                            $data['pab_>8'] ++;
                        }
                        
                        if ($PASAEAttendanceDate && $PASAEAttendanceDate < $data['pab_longest']) {
                            $PASAEAttendanceDate = $PASAEAttendanceDate;
                        }
                    }
                }
            }
        }
        
        return $data;
    }
    
    public static function getUnassignedPatientModel() {

        $patients = self::find()
                        ->select([
                            'Patient_id as id', 
                            "CONCAT(patient_name + ' - ' , board_number) as patient_name",
                            'board_number',
                            'patient_name',
                            'coordination_code',
                            'Tracker_awaiting_bed_time',
                            'Tracker_allocated_bed_time',
                            'infection_status',
                            'Tracker_timer',
                            'ed_doctor.name as doctor',
                            'ed_nurse.name as nurse',
                            'dob',
                            'address',
                            'age',
                            'Tracker_admission_ed_time',
                            'ed_location',
                            'ed_doctor',
                            'ed_nurse',
                            'ed_stream',
                            'forename',
                            'surname',
                            'is_deleted',
                        ])
                        ->leftJoin('ed_doctor', 'ed_doctor.id = patients.ed_doctor')
                        ->leftJoin('ed_nurse', 'ed_nurse.id = patients.ed_nurse')
                        ->where(['is_deleted' => 0])
                        ->andWhere(['or', 
//Waiting Room ID
                        'ed_location = '.EdLocation::getWaitingRoom(),
                        'ed_location IS NULL'])
                        ->andWhere(['or',
                        'is_discharge <> 1',
                        'is_discharge IS NULL'])
                        ->orderBy(["CONCAT(patient_name + ' - ' , board_number)" => SORT_ASC])->all();
   
        return $patients; 
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatientReferrals() {
        return $this->hasMany(PatientReferral::className(), ['patient_id' => 'Patient_id']);
    }
    
    
//    public function getBedInfo() {
//        $bed_location = '';
//        if ($this->bed_status) {
//            $Tracker_allocated_bed_time = '';
//            $Tracker_awaiting_bed_time = '';
//            if ($this->Tracker_awaiting_bed_time) {
//                $Tracker_awaiting_bed_time = ArrayHelpers::getHourMin($this->Tracker_awaiting_bed_time);
//            }
//            if ($this->Tracker_allocated_bed_time) {
//                $Tracker_allocated_bed_time = ArrayHelpers::getHourMin($this->Tracker_allocated_bed_time);
//            }
//            if ($Tracker_allocated_bed_time && $Tracker_awaiting_bed_time) {
//                $bed_location = $this->bed_status . ' ' . $Tracker_allocated_bed_time . ' (' . $Tracker_awaiting_bed_time . ')';
//            } else if ($Tracker_allocated_bed_time) {
//                $bed_location =  $this->bed_status . ' ' . $Tracker_allocated_bed_time;
//            } else if ($Tracker_awaiting_bed_time) {
//                $bed_location = $this->bed_status . ' ' . $Tracker_awaiting_bed_time;
//            }
//        }
//        return $bed_location;
//    }
    
    public function getPatientName() {
        return $this->forename.' '.$this->surname;
    }
    
}
 