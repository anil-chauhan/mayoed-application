<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "ed_doctor".
 *
 * @property int $id
 * @property string $name
 * @property int $created_at
 * @property int $updated_at
 * @property int $alter_user_id
 */
class EdDoctor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    } 
    
    public static function tableName()
    {
        return 'ed_doctor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['name'], 'required'],
            ['name', 'unique', 'targetClass' => '\common\models\EdDoctor', 'message' => 'This name has already been taken.'],
            [['created_at', 'updated_at', 'alter_user_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'alter_user_id' => 'Alter User ID',
        ];
    }
    
      /**
     * To get the doctor list with a blank nurse
     */
    public static function getDoctorList($list = false)
    {
        $listDoctors = EdDoctor::find()->asArray()->orderBy(['name' => SORT_ASC])->all();
        if($list){
            return $listDoctors;
        }
        $doctor[] = [
            'id' => '0',
            'name' => 'None',
            'created_at' => '', 
            'updated_at' => '',
            'alter_user_id' => ''
        ];
        $doctor = array_merge($doctor,$listDoctors);
        return $doctor;
    }
      /**
     * To get the doctor list with a blank nurse
     */
    public static function getDoctorName($id)
    {
        $doctor = EdDoctor::findOne($id);
        return $doctor->name;
    }
}
