<?php
namespace common\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $email;
    public $firstname;
    public $lastname;
    public $address1;
    public $address2;
    public $county;
    public $phone;
    public $dateofbirth;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            
            [['firstname', 'lastname', 'address1', 'county'], 'required'],
            [['dateofbirth'], 'safe'],
            [['dateofbirth'], 'date', 'format' => 'M/d/Y'],
            
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->firstname   = $this->firstname;
            $user->lastname    = $this->lastname;
            $user->address1    = $this->address1;
            $user->address2    = $this->address2;
            $user->county      = $this->county;
            $user->phone       = $this->phone;
            $user->dateofbirth = $this->dateofbirth ? date(strtotime('Y-m-d')) : $this->dateofbirth;
            
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
}
