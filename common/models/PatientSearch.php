<?php

namespace common\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Patient;

/**
 * PatientSearch represents the model behind the search form of `common\models\Patient`.
 */
class PatientSearch extends Patient
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Patient_id','created_at', 'updated_at', 'is_deleted', 'ed_location', 'ed_doctor', 'ed_nurse', 'is_awaiting_admission', 'is_discharge','Tracker_awaiting_bed_time', 'is_triage', 'is_rpat'], 'integer'],
            [['Tracker_rpat_time','patient_name', 'board_number', 'Tracker_admission_ed_time', 'surname', 'forename', 'age', 'current_bed_code', 'cons_name_current', 'dob', 'comment', 'episode_number', 'address', 'ed_stream', 'sex', 'code_name', 'referral_type', 'coordination_code', 'coordination_code_order', 'current_ward_code', 'infection_status', 'bed_status', 'PhoneNumber', 'Mobile', 'AEModeOfArrivalCode','PASIncidentDetailsSeqNumber'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    
    public function search($params)
    {   
       if(isset($params['all'])){
       
         $query = Patient::find()->orderBy(['Patient_id'=> SORT_DESC]); 
      
       } else if(isset($params['review'])) {
           
           $query = Patient::find()->where(['or',
                                       ['<>', 'PASLogoutDate', NULL],
                                       ['<>','PASLogoutDate','1900-01-01 00:00:00.000']])
                                    ->andWhere(['PASAEDischargeDate'=> '1900-01-01 00:00:00.000']);
       } else { 
             //$query = Patient::find()->where(['<>','is_discharge',0]);
           $query = Patient::find()->select('max(Patient_id)')->where(['PASAEDischargeDate' => '1900-01-01 00:00:00.000'])
                  
                   ->andWhere([
//                    'or',[
                        'or',
                       ['PASLogoutDate'=> NULL],
                       ['PASLogoutDate'=>'1900-01-01 00:00:00.000']
//                     ],['>','PASIncidentDetailsSeqNumber','1']
                     ])
//                ->andWhere("PASAEAttendanceDate > CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21) OR (Not( [coordination_code] = '' ) AND PASAEAttendanceDate < CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21))")
                  ->andWhere("PASAEAttendanceDate > CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21) OR (Not( [coordination_code] = ''))")
                  ->groupBy(['board_number','episode_number']);
       }
        
        
        $flagPagination = (isset($params['tab']) && $params['tab'] == "allpatient" ) ? true : ( isset($params['tab']) ? false : true);
        $flagPagination = (isset($params['pdf']) && $params['pdf']) ? false : $flagPagination;
        //add conditions that should always apply here
      
       if(isset($params['review']) || isset($params['all'])){
           $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $flagPagination ? [
                'pageSize' => 20,
            ] : false,
            'sort'=> [
                'defaultOrder' => ['is_triage' => SORT_ASC,'Tracker_timer' => SORT_ASC]]
          ]); 
           
      } else {
       
        $subQuery = Patient::find()->where(['in','Patient_id',$query]);
        $dataProvider = new ActiveDataProvider([
            'query' => $subQuery,
            'pagination' => $flagPagination ? [
                'pageSize' => 20,
            ] : false,
            'sort'=> [
                'defaultOrder' => ['is_triage' => SORT_ASC,'Tracker_timer' => SORT_ASC]]
          ]);
       }
       
 
        $flagTriage = true;
        if(isset($params['searchPatient'])) {  
            $query->andWhere(['like', 'patient_name', $params['searchPatient']])
                    ->orWhere(['like', 'board_number', $params['searchPatient']]);
            $flagTriage = false;
        }
        if($this->load($params)){
            $flagTriage = false;
        }
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
     
         
        // grid filtering conditions
        $query->andFilterWhere([
            'Patient_id' => $this->Patient_id,
            //'user_id' => $this->user_id,
            'Tracker_admission_ed_time' => $this->Tracker_admission_ed_time,
            'dob' => $this->dob,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_deleted' => $this->is_deleted,
            'ed_location' => $this->ed_location,
            'ed_doctor' => $this->ed_doctor,
            'ed_nurse' => $this->ed_nurse,
            'Tracker_timer' => $this->Tracker_timer,
            'is_awaiting_admission' => $this->is_awaiting_admission,
            'is_discharge' => $this->is_discharge,
            //'is_paed_minor' => $this->is_paed_minor,
            'Tracker_awaiting_bed_time' => $this->Tracker_awaiting_bed_time,
            'is_rpat' => $this->is_rpat,
            'Tracker_rpat_time' => $this->Tracker_rpat_time,
        ]);

        $query->andFilterWhere(['like', 'patient_name', $this->patient_name])
            ->andFilterWhere(['like', 'board_number', $this->board_number])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'forename', $this->forename])
            ->andFilterWhere(['like', 'age', $this->age])
            ->andFilterWhere(['like', 'current_bed_code', $this->current_bed_code])
            ->andFilterWhere(['like', 'cons_name_current', $this->cons_name_current])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'episode_number', $this->episode_number])
            ->andFilterWhere(['like', 'address', $this->address])
//            ->andFilterWhere(['like', 'ed_stream', $this->ed_stream])
            ->andFilterWhere(['like', 'sex', $this->sex])
//          ->andFilterWhere(['like', 'code_name', $this->code_name])
            ->andFilterWhere(['like', 'referral_type', $this->referral_type])
            ->andFilterWhere(['like', 'coordination_code', $this->coordination_code])
//           ->andFilterWhere(['like', 'coordination_code_order', $this->coordination_code_order])
            ->andFilterWhere(['like', 'current_ward_code', $this->current_ward_code])
            ->andFilterWhere(['like', 'infection_status', $this->infection_status])
            ->andFilterWhere(['like', 'is_bed_allocated', $this->is_bed_allocated])
            ->andFilterWhere(['like', 'PhoneNumber', $this->PhoneNumber])
            ->andFilterWhere(['like', 'Mobile', $this->Mobile])
            ->andFilterWhere(['like', 'AEModeOfArrivalCode', $this->AEModeOfArrivalCode]);
           
        if(isset($params['ed_stream']) && !empty($params['ed_stream']) && $params['ed_stream']){
            
            if($params['ed_stream'] == 'minor_injury'){
               // $query->andFilterWhere(['like', 'coordination_code', 'M'])->orFilterWhere(['is_triage'=> 1]);
          $query->andWhere(['or',['like', 'coordination_code', 'M'],['is_triage'=> 1]]);
                } else  {
                $query->andFilterWhere(['like', 'ed_stream', $params['ed_stream']]);
                $flagTriage = false;
            }
        }
          
        if(isset($params['is_mau']) && !empty($params['is_mau'] )){
            $query->andFilterWhere(['like', 'is_mau', $params['is_mau']]);
            $flagTriage = false;
        }
        if(isset($params['is_awaiting_admission']) && !empty($params['is_awaiting_admission'] )){
            $query->andFilterWhere(['like', 'is_awaiting_admission', $params['is_awaiting_admission']]);
            $flagTriage = false;
        }
//        echo '<pre>';print_r($params);die;
        if(isset($params['PatientSearch']['Tracker_timer']) && !empty($params['PatientSearch']['Tracker_timer'] )){
            $Tracker_timer = $params['PatientSearch']['Tracker_timer'];
            $query->andFilterWhere(['like', 'TIME_FORMAT(SEC_TO_TIME( UNIX_TIMESTAMP() - Tracker_timer),"%H:%i")', $Tracker_timer]);
            $flagTriage = false;
        } 
        
        if(isset($params['location']) && $params['location']){
            $query->leftJoin('ed_location as ed', 'ed.id = patients.ed_location')->orderBy(['priority' => SORT_ASC]);
            $flagTriage = false;
        } 
        
        if(isset($params['current_ward_code']) && $params['current_ward_code']){
            $query->andFilterWhere(['like', 'current_ward_code', $params['current_ward_code']]);
            $flagTriage = false;
        }   
        
        if(isset($params['age_under_16']) && $params['age_under_16']){
            $query->andFilterWhere(['<', 'age', 16])->andFilterWhere(['<>', 'is_triage', 1]);
            $flagTriage = false;
        }
        
        if(isset($params['infection_status']) && $params['infection_status']){
           $query->andWhere(['<>', 'infection_status', '']);
            $flagTriage = false;
        } 
        
        if(isset($params['not_ed_doctor']) && $params['not_ed_doctor']){
           $query->andWhere(['ed_doctor' => NULL]);
            $flagTriage = false;
        }
        if(isset($params['Patient']['is_discharge']) && isset($params['Patient']['is_discharge'])){
            $query->andWhere(['is_discharge' => 1]);
            $flagTriage = false;
        }
        if(isset($params['PatientSearch']['is_triage'])){
            $query->andwhere(['is_triage'=>1])->andWhere(['coordination_code' => '']);
        }else {
            $query->andWhere(['<>', 'coordination_code', '']);
        }
//      
        return $dataProvider;
      
      } 
 }  
