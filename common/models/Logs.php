<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
//use yii\behaviors\SluggableBehavior;
//use yii\behaviors\BlameableBehavior;
/**
 * This is the model class for table "logs".
 *
 * @property int $id
 * @property int $user_id
 * @property int $patient_id
 * @property int $log_type
 * @property string $replace_old
 * @property string $replace_new
 * @property string $description
 * @property string $board_number
 * @property string $episode_number
 * @property Accesspass $username
 * @property string $code_name
 * @property int $created_at
 * @property string $created_at
 * @property User $alterUser
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logs';
    }
    
    /**
     * @inheritdoc
     */
    
    
     public function behaviors()
         {
             return [
                 'timestamp' => [
                     'class' => 'yii\behaviors\TimestampBehavior',
                     'attributes' => [
                         ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                         //ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                     
                     ],
                      'value' => date("Y-m-d H:i:s", strtotime('now')),
                 ],
                
                 
             ];
         }

         

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'patient_id', 'log_type'], 'integer'],
            [['created_at','comment','board_number', 'replace_old', 'replace_new', 'episode_number'], 'string'],
            [['code_name','updated_at','PASIncidentDetailsSeqNumber'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'patient_id' => 'Patient ID',
            'log_type' => 'Log Type',
            'replace_old' => 'Last Value',
            'replace_new' => 'Updated Value',
            'description' => 'Description',
            'board_number' => 'Board Number',
            'episode_number' => 'Episode Number',
            'admission_date' => 'Admission Date',
            'code_name' => 'Code Name',
            'created_at' => 'Created At',
            
        ];
    }
    public static function add($logModel,$model, $user = Null){
        return true;
        //$logModel->user_id = isset($user) && $user ? $user->id : '';
        //$logModel->patient_name = $model->patient_name;
        $logModel->board_number = $model->board_number;
        $logModel->episode_number = $model->episode_number;
       //$logModel->description = $model->comment;
        $logModel->patient_id = $model->Patient_id;
        $logModel->save();
        if($logModel->save()){
            return $logModel->save();
        }
    }   
    
    
    
     public static function addDischarge($logModel,$model, $user = Null){
        
        $logModel->board_number    = $model->board_number;
        $logModel->episode_number  = $model->episode_number;
        $logModel->PASIncidentDetailsSeqNumber = $model->PASIncidentDetailsSeqNumber;
        //$logModel->description = $model->comment;
        $logModel->patient_id = $model->Patient_id;
        if($logModel->save()){
           return $logModel->save();
        }
    }  
    
    public static function addLog($logModel,$model,$comment=NULL){
        
        //$logModel->user_id = isset($user) && $user ? $user->id : '';
        //$logModel->patient_name = $model->patient_name;
        $logModel->board_number   = $model->board_number;
        $logModel->episode_number = $model->episode_number;
        $logModel->PASIncidentDetailsSeqNumber = $model->PASIncidentDetailsSeqNumber;
        $logModel->comment        = $comment;
        $logModel->patient_id     = $model->Patient_id;
        
       if($logModel->save(false)){
           
           return $logModel->save();
        }
    }  
    
    public static function getType($type){
        $logModel = self::find()->where(['log_type' => $type])->one();
        
        if($logModel){
            return $logModel->log_type;
        }
    }   
    
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlterUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
     public static function Doctor($id) {
        $name = EdDoctor::findOne($id);
        return $name['name'];      
    }
    public static function Location($id) {
        $name = EdLocation::findOne($id);
        return $name['name'];           
    }
    public static function Nurse($id) {
        $name = EdNurse::findOne($id);
        return $name['name'];          
    }
    
    
}
