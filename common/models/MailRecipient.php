<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mail_recipients".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 */
class MailRecipient extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mail_recipients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            ['email', 'email'],
            [['name','email'],'required'],
            ['email', 'unique', 'targetClass' => '\common\models\MailRecipient', 'message' => 'This email has already been taken.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
        ];
    }
}
