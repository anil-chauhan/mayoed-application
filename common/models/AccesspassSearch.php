<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Accesspass;

/**
 * AccesspassSearch represents the model behind the search form of `common\models\Accesspass`.
 */
class AccesspassSearch extends Accesspass
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Accesspass_id', 'is_active', 'user_id'], 'integer'],
            [['name', 'password'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Accesspass::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'Accesspass_id' => $this->Accesspass_id,
            'is_active' => $this->is_active,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'password', $this->password]);
       
        return $dataProvider;
    }
}
