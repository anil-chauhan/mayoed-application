<?php

namespace common\models;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "ed_location".
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property int $alter_user_id
 * @property int $priority
 * @property int $location_type
 * @property int $location_order
 * @property int $location_column_order
 * @property int $location_column_suborder
 * 
 * @property Patient $patient
 * @property Patient[] $patients
 */
class EdLocation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    
   
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date("Y-m-d H:i:s", strtotime('now')),
            ],
        ];
    }
    
    
    public static function tableName()
    {
        return 'ed_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['name'],'required'],
            ['name', 'unique', 'targetClass' => '\common\models\EdLocation', 'message' => 'This name has already been taken.'],
            [['alter_user_id', 'priority', 'location_type', 'location_order', 'location_column_order', 'location_column_suborder'], 'integer'],
        ];
    } 

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'alter_user_id' => 'Alter User ID',
            'priority' => 'Priority',
            'location_type' => 'Location Type',
            'location_order' => 'Location Order',
            'location_column_order' => 'Location Column Order',
            'location_column_suborder' => 'Location Column Suborder',
        ];
    }
     
    public static function getWaitingRoom()
    {
        $model = EdLocation::find()->select(['id'])->where(['name' => 'WAITING'])->one();
        if($model){
            return $model->id;
        } 
    }
    
    
    public static function getLocationList()
    {
        $loc[] = [
            'id' => '0',
            'name' => 'None',
            'created_at' => '', 
            'updated_at' => '',
            'alter_user_id' => ''
        ];
        $loc = array_merge($loc,  EdLocation::find()->asArray()->orderBy('name')->all());
        return $loc;
    } 
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {   
        return $this->hasOne(Patient::className(), ['ed_location' => 'id'])
                ->andOnCondition('is_discharge <> 1')
                ->andOnCondition(['PASAEDischargeDate' => '1900-01-01 00:00:00.000'])
                ->andOnCondition(['or',['PASLogoutDate'=> NULL],['PASLogoutDate'=>'1900-01-01 00:00:00.000']])
                ->andOnCondition("PASAEAttendanceDate > CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21) OR (Not( [coordination_code] = '' ) AND PASAEAttendanceDate < CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21))")
                ->orderBy(['Patients.created_at'=>SORT_ASC]);
    }
 
    public function getPatients()
    {   
         return $this->hasMany(Patient::className(), ['ed_location' => 'id'])
                 ->andOnCondition('is_discharge <> 1')
                 ->andOnCondition(['PASAEDischargeDate' => '1900-01-01 00:00:00.000'])
                 ->andOnCondition(['or',['PASLogoutDate'=> NULL],['PASLogoutDate'=>'1900-01-01 00:00:00.000']])
                 ->andOnCondition("PASAEAttendanceDate > CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21) OR (Not( [coordination_code] = '' ) AND PASAEAttendanceDate < CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21))")
                 ->orderBy(['Patients.created_at'=>SORT_ASC]);
    }
    
}
