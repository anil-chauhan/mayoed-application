<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ed_stream".
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property string $color
 */
class EdStream extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ed_stream';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'value', 'color'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
            'color' => 'Color',
        ];
    }
}
