<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "coordination_code".
 *
 * @property int $id
 * @property string $code
 * @property string $color
 * @property string $code_order
 * @property string $color_code
 */
class CoordinationCode extends \yii\db\ActiveRecord
{
    /** 
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coordination_code';
    } 
   
    /**
     * {@inheritdoc}
     */
    
    public function rules()
    {
        return [
            [[ 'code', 'color'], 'string'],
            [[ 'code', 'code_order'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'color' => 'Color',
            'code_order' => 'Order',
        ];
    }
    
    public static function getCoordinationCodeOrder($code) {
        $orderValue = self::find()->select(['code_order'])->where(['code' => $code])->asArray()->one();
        
        return ($orderValue) ? $orderValue['code_order'] : '';
    }
    public static function getCoordinationCodeOrderColor($code) {
        $orderValue = self::find()->select(['color'])->where(['code' => $code])->asArray()->one();
        
        return ($orderValue) ? $orderValue['color'] : ''; 
    }
    public static function getCoordinationColorCode($code) {
        $orderValue = self::find()->select(['color_code'])->where(['code' => $code])->asArray()->one();
        
        return ($orderValue) ? $orderValue['color_code'] : '';
    }
    
     public static function getCodeList()
    {
//        $code[] = [
//            'id' => '0',
//            'code' => 'None',
//            'code_order' => '', 
//            'color' => '',
//            'alter_user_id' => ''
//        ];
        //$loc = array_merge($code, CoordinationCode::find()->asArray()->orderBy('code')->all());
          $loc = CoordinationCode::find()->asArray()->orderBy('code')->all();
         return $loc;
    } 
}
