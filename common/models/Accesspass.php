<?php

namespace common\models;
use Yii;

/**
 * This is the model class for table "accesspass".
 *
 * @property int $Accesspass_id
 * @property string $name
 * @property string $password
 * @property int $is_active
 * @property int $user_id
 */
class Accesspass extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    
    public $primaryKey= "Accesspass_id";


    public static function tableName()
    {
        return 'accesspass';
    }  
    
   
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'password'], 'string'],
            [['name', 'password'], 'required'],
            ['name', 'unique', 'targetClass' => '\common\models\Accesspass', 'message' => 'This name has already been taken.'],
            [['is_active', 'user_id'], 'integer'],
        ];
    } 
    
    
//      public function fields()
//    {
//        return [
//            'Accesspass_id' => 'id'
//            
//      ];
//    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Accesspass_id' => 'ID',
            'name' => 'Name',
            'password' => 'Password',
            'is_active' => 'Is Active',
            'user_id' => 'User ID',
        ];
    }
    
    public static function UserName($id){
       
        return self::find()->select(['name'])->where(['Accesspass_id'=>$id])->one();
    }
    
}
   
