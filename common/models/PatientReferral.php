<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "patient_referral".
 *
 * @property int $id
 * @property int $patient_id
 * @property int $referral_type
 * @property string $patient_name
 * @property string $board_number
 * @property string $episode_number
 * @property string $code
 * @property string $sex
 * @property string $age
 * @property string $comment
 * @property int $created_at
 * @property int $updated_at
 * @property string $Tracker_admission_ed_time
 * @property Patient $patient
 * @property User $user
 * @property Location $location
 */
class PatientReferral extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => date("Y-m-d H:i:s", strtotime('now')),
            ],
        ];
    }
    
    
    
    public static function tableName()
    { 
        return 'patient_referral';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['patient_id', 'referral_type','age'], 'integer'],
            [['created_at', 'updated_at','patient_name', 'board_number', 'episode_number', 'code', 'sex','comment','Tracker_admission_ed_time'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'patient_id' => 'Patient ID',
            'referral_type' => 'Referral Type',
            'patient_name' => 'Patient Name',
            'board_number' => 'Board Number',
            'episode_number' => 'Episode Number',
            'code' => 'Code',
            'sex' => 'Sex',
            'age' => 'Age',
            'comment' => 'Comment',
            'Tracker_admission_ed_time' => 'Admission Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatient()
    {
        return $this->hasOne(Patient::className(), ['Patient_id' => 'patient_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getLocation(){
       
        return $this->hasOne(EdLocation::className(), ['id' => 'ed_location'])->via('patient');
    
    }
}
