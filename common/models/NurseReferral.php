<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "nurse_referral".
 *
 * @property int $id
 * @property int $user_id
 * @property int $created_at
 * @property int $updated_at
 * @property string $patient_weight
 * @property int $commence_two_day_food_chart
 * @property int $compact_protein_125ml_bd
 * @property int $is_delete
 * @property string $nurse_name
 * @property string $contact_details
 * @property string $board_number
 * @property string $episode_number
 * @property string $patient_name
 * @property string $ward
 * 
 * @property User $alterUser
 * @property Consultant $consultant
 */
class NurseReferral extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nurse_referral';
    }
    public static function getDb() {
        return Yii::$app->dbDietetics;
    }
    
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at', 'commence_two_day_food_chart', 'compact_protein_125ml_bd'], 'integer'],
            [['patient_weight', 'board_number', 'episode_number'], 'string', 'max' => 128],
            [['nurse_name','patient_name'], 'string', 'max' => 256],
            [['patient_weight','nurse_name','contact_details'], 'required'],
            [['ward','contact_details'], 'string', 'max' => 512],
        ];
    } 

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'patient_weight' => 'Patient Weight',
            'commence_two_day_food_chart' => 'Commence Two Day Food Chart',
            'compact_protein_125ml_bd' => 'Compact Protein 125ml Bd',
            'nurse_name' => 'Nurse Name',
            'contact_details' => 'Contact Details',
            'board_number' => 'Board Number',
            'episode_number' => 'Episode Number',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlterUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsultant()
    {
        return $this->hasOne(Consultants::className(), ['board_number' => 'board_number']);
    }
}

