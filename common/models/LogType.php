<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "log_type".
 *
 * @property int $id
 * @property string $LogTypeID
 * @property string $LogTypeDescription
 */
class LogType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'LogTypes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['LogTypeID', 'LogTypeDescription'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'LogTypeDescription' => 'Log ID',
            'LogTypeDescription' => 'Value',
        ];
    }
     
    public static function getLogArr(){
        $models = self::find()->all();
        
        if($models){
            return ArrayHelper::map($models,'LogTypeID','LogTypeDescription');
        }
    }   
     
    public static function getName($type){
        $logModel = self::find()->where(['LogTypeID' => $type])->one();
        
        if($logModel){
            return $logModel->LogTypeDescription;
        }
    }   
     
    public static function getId($value){
        $logModel = self::find()->where(['LogTypeDescription' => $value])->one();
        
        if($logModel){
            return $logModel->log_id;
        }
    }   
}
