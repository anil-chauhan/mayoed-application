<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property integer $hospital_id
 * @property string $firstname
 * @property string $lastname
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property string $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property PatientTreatments[] $patientTreatments
 * @property PatientSocials[] $patientSocials
 * @property PatientQuestionnaires[] $patientQuestionnaires
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    
    const TYPE_SUPER = 'super';
    const TYPE_ADMIN = 'admin';
    const TYPE_USER  = 'user';
     
    public $password;
    public $confirm_password;
    public $name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [ 
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],
            [['firstname','lastname','type','status','email'],'required'],
            [['password', 'confirm_password'], 'required', 'except' => 'edit_registration'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
           //['password', 'compare', 'compareAttribute' => 'confirm_password', 'message' => "Passwords don't match"],
            [['dateofbirth', 'phone', 'address2'], 'safe'],
            [['dateofbirth'], 'date', 'format' => 'Y-M-d'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }
    
    /*public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['edit_registration'] = ['name', 'password'];//Scenario Values Only Accepted
        return $scenarios;

    }*/
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname'   => 'First Name',
            'lastname'    => 'Last Name',
            'phone'       => 'Phone',
            'email'       => 'Email',
            'dateofbirth' => 'Date of Birth',
            'address1'    => 'Address 1',
            'address2'    => 'Address 2',
            'county'      => 'County',
        ];
    } 

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
   {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
   public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
    
    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public function getUserName()
    {
        return trim(ucwords(implode(' ', array($this->firstname, $this->lastname))));
    }
    
    public function getType() {
        return Yii::$app->params['userType'][$this->type];
    }
    
    public function getStatus() {
        return Yii::$app->params['userStatus'][$this->status];
    }

    public function isSuperAdmin() {
        return $this->type == self::TYPE_SUPER;
    }
    
    public function isBasicUser() {
        return $this->type == self::TYPE_USER;
    }
    
    public static function getUserList($id = null) {
        
        if($id) {
            return User::find()->where(['id' => $id])->select(['name' => 'concat(firstname + " ", lastname)', 'id'])->orderBy('firstname');
        }
        return User::find()->select(['name' => 'concat(firstname + " ", lastname)', 'id'])->orderBy('firstname');
    }
    
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatientSocials()
    {
        return $this->hasMany(PatientSocials::className(), ['alter_user_id' => 'id']);
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatientQuestionnaires()
    {
        return $this->hasMany(PatientQuestionnaires::className(), ['alter_user_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatientTreatments()
    {
        return $this->hasMany(PatientTreatments::className(), ['alter_user_id' => 'id']);
    }
    
    public static function getLogData($logs){
        
        $data = [
            'first' => [
                'name' => '',
                'time' => ''
            ],
            'last' => [
                'name' => '',
                'time' => ''
            ],
        ]; 

        $clinicianLog = [];
        if(!empty($logs)){
            foreach($logs as $log){
                if(LogType::getName($log->log_type) == 'Doctor'){
                    $clinicianLog[] = $log->getAttributes();
                }
            }
        }
        if(!empty($clinicianLog)){
            
            $firstClinician = EdDoctor::getDoctorName($clinicianLog[0]['updated_value']);
            $data['first']['name'] = (!empty($firstClinician)) ? $firstClinician : 'Empty';
            $data['first']['time'] = date("Y-m-d \& H:i:s", $clinicianLog[0]['created_at']);
            
            $lastClinician = EdDoctor::getDoctorName(end($clinicianLog)['updated_value']);
            $data['last']['name'] = (!empty($lastClinician)) ? $lastClinician : 'Empty';
            $data['last']['time'] = date("Y-m-d \& H:i:s", end($clinicianLog)['created_at']);
        }
    
        return $data;       
    } 
}
