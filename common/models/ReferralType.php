<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "referral_type".
 *
 * @property int $id
 * @property string $code
 * @property string $name
 */
class ReferralType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'referral_type';
    } 
   /**
     * {@inheritdoc}
     */
   
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['code', 'name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
        ];
    }
    public static function getReferralList( $list = false)
    {
        $listReferral = ReferralType::find()->asArray()->orderBy(['name' => SORT_ASC])->all();
        if($list){
            return $listReferral;
        }
        $referral[] = [
            'id' => '0',
            'code' => 'None',
            'name' =>'None'
        ];
        $referral = array_merge($referral,$listReferral);
        return $referral;
//        echo '<pre>';print_r($nurse);die;
    }
}
