<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PatientReferral; 


/**
 * PatientReferralSearch represents the model behind the search form of `common\models\PatientReferral`.
 */
class PatientReferralSearch extends PatientReferral
{
    /**
     * {@inheritdoc}
     */
    
    public $ed_location;  
    public function rules()
    {
        return [
            [['id', 'patient_id', 'referral_type','age'], 'integer'],
            [['created_at', 'updated_at'],'string'],
            [['patient_name', 'board_number', 'episode_number', 'code', 'sex', 'comment'], 'safe'],
        ];
    }   

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PatientReferral::find();
        $query->joinWith(['location']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
      
   
       
         if (!($this->load($params)) && $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
       
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'patient_id' => $this->patient_id,
            'referral_type' => $this->referral_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'patient_name', $this->patient_name])
            ->andFilterWhere(['like', 'board_number', $this->board_number])
            ->andFilterWhere(['like', 'episode_number', $this->episode_number])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'age', $this->age])
            ->andFilterWhere(['like', 'comment', $this->comment]) 
            ->andFilterWhere(['like', 'patients.ed_location', $this->ed_location]);
            return $dataProvider;
    }
    
}
