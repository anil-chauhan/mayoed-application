<?php

namespace common\components;

class ArrayHelpers
{
    public static function sortArray( $array, $type = 'asc' ){
        if($type =='asc'){
            asort($array);
        } else {
            arsort($array);
        }
        return $array;
    }
    public static function getHourMin( $seconds ){
        $sec = strtotime('now') - $seconds;
        $hour = (int) abs($sec/(60*60));
        $min = (int) abs(($sec - $hour * 60 *60)/60);
        return substr('0'.$hour,-2) .':'. substr('0'.$min,-2);

    }
    public static function getHourMinSec( $sec ){
        
        $hour = (int) abs($sec/(60*60));
        $min = (int) abs(($sec - $hour * 60 *60)/60);
        return substr('0'.$hour,-2) .':'. substr('0'.$min,-2);

    }
}