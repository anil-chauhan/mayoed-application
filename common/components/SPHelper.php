<?php

namespace common\components;
use Yii;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class SPHelper {

    public static function getAllEdPatients($boardNum = false) {
        $patientsArray = array();
        $spName = 'sp_EDT_Client_Query';
        if (!isset(Yii::$app->params['noSP'])) {
            $command = Yii::$app->uchgED->createCommand("[UCHG_DW].[dbo].[".$spName."]");
            $patientsArray = $command->queryAll();
        } else {
            $patientsArray = file_get_contents(Yii::$app->basePath."/../www/spdata/".$spName.".js");
            $patientsArray = json_decode($patientsArray, true);
        }
        
        if ($boardNum) {
            foreach ($patientsArray as $patient) {
                if ($patient['BoardNum'] == $boardNum)
                    return $patient;
            }
            return NULL;
        }
        return $patientsArray;
    } 
    
    public static function getBedStatus($boardNum = false) {
        $patientsArray = array();
        $spName = 'NEWED_Tracker_BedStatus';
        
        if (!isset(Yii::$app->params['noSP']) ) {
            $command = Yii::$app->uchgWD->createCommand("[UCHG_DW].[dbo].[NEWED_Tracker_BedStatus]");
            $patients = $command->queryAll();
        } else {
            $patients = file_get_contents(Yii::$app->basePath."/../www/spdata/".$spName.".js");
            $patients = json_decode($patients, true);
        }


        if ($boardNum) {
            foreach ($patients as $patient) {
                if ($patient['BoardNum'] == $boardNum && strpos($patient['Bed Status'], 'Awaiting Bed') !== false) {
                    return true;
                }
            }
            return NULL;
        }
        return $patients;
    }
    public static function filterEdSP(){
        $spArray = self::getAllEdPatients();
        $filterArray = [];
        $newFilterArray = [];
        if(count($spArray)){
            foreach ($spArray as $key => $row) {
                if(isset($filterArray[$row['BoardNum']])){
                    $filterArray[$row['BoardNum']]['INF'] .= $row['INF'];
                    $filterArray[$row['BoardNum']]['INH'] .= $row['INH'];
                    $filterArray[$row['BoardNum']]['PASH'] .= $row['PASH'];
                } else {
                    $filterArray[$row['BoardNum']] = $row;  
                }
            }
            $i = 0;
            foreach ($filterArray as $value) {
                $newFilterArray[$i++] = $value; 
            }
        }
        return $newFilterArray;
    }
    
    public static function getInfectionStatus($infection){
        
        if(strpos($infection, '#') !== false){
            return '#';
        }else if(strpos($infection, '$')!== false){
            return '$';
        }
        return $infection;
   } 
    
   
    public static function getPatients($boardNum = null) {
        $spName = 'OPD_Epis_Detail'; 
        if (!Yii::$app->params['noSP']) {
            
            $command = Yii::$app->uchgWD->createCommand("[UCHG_DW].[WDC].[".$spName."]:query");
            $command->bindParam(':query', strtoupper($boardNum));
            $patients = $command->queryAll();
        } else {
            
            $patients = file_get_contents(Yii::$app->basePath."/../www/spdata/".$spName.".js");
            $patients = json_decode($patients,true);
        }
       return $patients;
    }

}
