<?php

namespace common\components;

use Yii;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class NameHelper {

    // Function to return Initials from the full name
    public static function getInitials($first_name, $lastname) {
//        $words = explode(" ", $name);
//        $acronym = "";
//
//        foreach ($words as $w) {
//            $acronym .= isset($w[0]) ? $w[0] : "";
//        }
//        
//        return $acronym;

        $fname = isset($first_name) ? $first_name[0] : "";
        $lname = isset($lastname) ? $lastname[0] : "";

        return $fname . " " . $lname;
    }

    // Function to return Initials from the full name with three letters from the surname
    public static function getInitialsWithThreeLettersFromSurname($first_name, $lastname) {
//        $words = explode(",", $name);
//        if(!empty($words)){
//            $first_name = isset($words[0]) ? $words[0][0] : "";
//        $last_name = isset($words[1]) ? substr($words[1], 0, 3) : "";
//      
//        return $first_name." ".$last_name; 
//        }

        $fname = isset($first_name) ? $first_name[0] : "";
        $lname = isset($lastname) ? substr($lastname, 0, 3) : "";

        return $fname . " " . $lname;
    }

    // Function to return Initials from the full name with three letters from the surname
    public static function getReverseName($name) {
        $words = explode(",", $name);
        if (!empty($words)) {
            $first_name = isset($words[1]) ? $words[1] : "";
            $last_name = isset($words[0]) ? $words[0] : "";

            return $first_name . " " . $last_name;
        }

        return $name;
    }

    // Function to return fullname
    public static function getFullName($first_name, $lastname) {

        return $first_name . " " . $lastname;
    }

}
