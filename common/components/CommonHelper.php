<?php

namespace common\components;

use Yii;
use common\components\NameHelper;
use common\models\CoordinationCode;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CommonHelper {

    public static function getEmptyCell($location) {
        return '<div class="div">
            <div class="assignPatient c-white bgm-white outer-div" style=" margin-top:10px;" data-ed_location="'.$location->id.'">
            <div class="innerBox text-left" style=" background-color: #556f7d;">
                <h4 class="c-white p-t-10" style="font-size: 33px;">'.$location->name.'
                </h4>
            </div>
            </div>
        </div>';
    }

    public static function getPatientCell($locationName, $patient, $ward = false) {
        $color = CoordinationCode::getCoordinationColorCode($patient->coordination_code);
        $textColor = ($color == '#f9ea00') ? 'color: #756e6e!important;' : '' ;
        $flagIsBed = $patient->bedInfo; 
        //$flagIsBed = false;
        $patientInitial = NameHelper::getInitialsWithThreeLettersFromSurname($patient->forename,$patient->surname). " ".$patient->infection_status;
        $dataAttributes = 'data-id = "'.$patient->Patient_id.'" data-name = "'. $patient->patientName.'
                 " data-boardNumber = "'.$patient->board_number. '" data-dob = "'.$patient->dob.' "data-episodeNumber = "'.$patient->episode_number.'
                 " data-address = "'.$patient->address. '" data-age = "'.$patient->age.'
                 " data-admission_date = "'.$patient->Tracker_admission_ed_time.'
                 " data-ed_location = "'.$patient->ed_location. '
                 " data-ed_doctor = "'.$patient->ed_doctor.'
                 " data-ed_nurse = "'.$patient->ed_nurse. '" data-ed_stream = "'.$patient->ed_stream.'
                 " data-toggle = "modal" data-target-color ="bluegray" '; 
        if($ward && $flagIsBed){
            return '';
        } 
        
        
        return '<div class="div">
        <div class="replaceClass c-white bgm-bluegray outer-div" style=" margin-top:10px;" '.$dataAttributes.'>
            <div class="innerBox text-left" style=" background-color:'.(($flagIsBed)? "purple" : $color).'!important; padding-top:10px;">
                <table style="width: 100%;" >
                    <tr>
                        <th rowspan="2">
                            <span class="h4 c-white" style="'.(($flagIsBed)? "" : $textColor).' font-size: 33px;"><b>'.$locationName.'</b></span>                            
                            <span class="h4 c-white float-right" style="'.(($flagIsBed)? "" : $textColor).'">'.$patientInitial.'</span>
                        </th>
                    </tr>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="p-5">'.
                $patient->board_number
                .'<span class="h4 c-white float-right" >'.
                        ArrayHelpers::getHourMin(strtotime($patient->PASAEAttendanceDate))
                .'</span><br/>'.
                        (($patient->edDoctor['name']) ? $patient->edDoctor['name'].'<br/>' : '').
                        (($patient->edNurse['name']) ? $patient->edNurse['name'].'<br/>' : '').
//                    '<span style="font-size:19px">'.$patient->bedInfo.'</span>
                    
            '</div>
        </div>
    </div>';
    }
    public static function getWaitingPatientCell($patient) {
        $color = CoordinationCode::getCoordinationColorCode($patient->coordination_code);
        $color = $color ? $color : '#607d8b!important;';
        $textColor = 'color: '.(($color == '#f9ea00') ? '#756e6e!important;' : '#ffffff!important' );
        $flagIsBed = false;//($patient->bedInfo) ? true : false;       
        $patientInitial = NameHelper::getInitialsWithThreeLettersFromSurname($patient->forename,$patient->surname). " ".$patient->infection_status;
        $dataAttributes = 'data-id = "'.$patient->Patient_id.'" data-name = "'. $patient->patientName.'
                       " data-boardNumber = "'.$patient->board_number. '" data-dob = "'.$patient->dob.' "data-episodenumber = "'.$patient->episode_number.'
                       " data-address = "'.$patient->address. '" data-age = "'.$patient->age.'
                       " data-admission_date = "'.$patient->Tracker_admission_ed_time.'
                       " data-ed_location = "'.$patient->ed_location. '
                       " data-ed_doctor = "'.$patient->ed_doctor.'
                       " data-ed_nurse = "'.$patient->ed_nurse. '" data-ed_stream = "'.$patient->ed_stream.'
                       " data-toggle = "modal" data-target-color ="bluegray" '; 
        return '
            <div class="m-l-5">
                <div class="c-white replaceClass "  style="margin-top:10px;" '.$dataAttributes.'>
                    <div class="waitingBox  " style="padding:10px; background-color: '.$color.'; min-height:50px; ">
                        <table style="width:100%; font-size: 25px; '.$textColor.'">
                            <tr>
                                <td align="left"><b>WR:</b></td>
                                <td align="right">'.$patientInitial.' '. $patient->board_number.'</td>
                            </tr>
                            <tr>
                                <td align="left">'.$patient->edDoctor["name"].'</td>
                                <td align="right">'.$patient->edNurse["name"].'</td>
                            </tr>
                            <tr>
                            </tr>
                        </table>
                    </div>
                </div>    
            </div>';
        //<td colspan="2" align="left">'.$patient->bedInfo.'</td>
    }

}
