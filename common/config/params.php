<?php

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    
    'userType' => [ 
        'super'   => 'Super Admin',
        'admin'   => 'Admin',
        'user'    => 'User', 
    ],
    'userStatus' => [
        '10' => 'Active',
        '0'  => 'In Active',
    ],
    
    'streamDataCount' => [ 
        'resus'   => 'resus-major',
        'triage'    => 'ran-rat', 
        'y_stream'    => 'ambulatory', 
        'paediatrics'    => 'paediatrics'
    ],
     'edStream'  =>  [
        'resus'   => 'Resus/Major', 
        'triage'   => 'RAN/RAT', 
        'y_stream'   => 'Ambulatory Care', 
        'minor_injury'   => 'Minor Injury', 
        'paediatrics'   => 'Paeds ED', 
//        'awaiting_mau'   => 'Awaiting MAU', 
    ],
    
    'referralType' => [ 
        '0' => 'AMU: Acute Medical Unit',
        '1' => 'MED: Medical On-Call',
        '2' => 'CVA: Stroke Thrombolysis',
        '3' => 'CARDIO: Cardiology On-Call',
        '4' => 'ONCO: Oncology On-Call',

        '5' => 'SURG: Surgery On-Call',
        '6' => 'ORTHO: Orthopaedics On-Call',
        '7' => 'URO: Urology On-Call',
        '8' => 'PLAS: Plastics On-Call',
        '9' => 'MAXFAX: Maxillofacial Surgery On-Call',

        '10' => 'GYNAE: Obs & Gynae On-Call',
        '11' => 'ENT: Otorhinolaryngology On-Call',
        '12' => 'EYES: Ophthalmology On-',
        '13' => 'PAED: Paediatrics On-',
        '14' => 'PSYCH: Psychiatry Liaison / On-Call',

        '15' => 'PHYSIO: Physiotherapy',
        '16' => 'MSW: Medical Social Worker',
        '17' => 'CIT: Community Intervention Team',

        '18' => 'Dermatology',
        '19' => 'Neurology',
        '20' => 'Haematology',
        '21' => 'Immunology',
        '22' => 'Geriatrics',

        '23' => 'OTHER: Please Specify in "Comments"'
    ],
    
    
    
    'poweredBy' => 'Successive Software',
    
    
    'widgetOptions' => [
        'gridView' => [
            'tableOptions' => ['class' => 'table table-striped table-bordered'],
            'layout' => '{items}<div id="data-table-basic-footer" class="bootgrid-footer container-fluid"><div class="row"><div class="col-sm-6">{pager}</div><div class="col-sm-6 infoBar"><div class="infos">{summary}</div></div></div></div>'
        ],
        'detailView' => [
            'template' => '<dl class="dl-horizontal"><dt>{label}</dt><dd>{value}</dd></dl>'
        ],
        'detailViewExtra' => [
            'template' => '<dl class="dl-horizontal extra"><dt>{label}</dt><dd>{value}</dd></dl>'
        ]
    ],
    'paginationOptions' => [
        'itemCount' => 5
    ],
    

    'mssqlDatabasePrefix'=>'[UCHG_DW].[WDC].',
    
    
    'htmlTemplate' => [
        'input' => [
            'template'     => "<div class='fg-line'>{input}</div>\n{label}\n{hint}\n{error}",
            'labelOptions' => ['class' => 'fg-label'],
            'inputOptions' => ['class' => 'input-small form-control fg-input'],
            'options'      => ['class' => 'form-group fg-float']
        ],
        'inputPassword' => [
            'template'     => "<div class='fg-line'>{input}</div>\n{label}\n{hint}\n{error}",
            'labelOptions' => ['class' => 'fg-label'],
            'inputOptions' => ['class' => 'input-small form-control fg-input pw'],
            'options'      => ['class' => 'form-group fg-float']
        ],
        'input-offset1' => [
            'template'     => "<div class='fg-line'>{input}</div>\n{label}\n{hint}\n{error}",
            'labelOptions' => ['class' => 'fg-label'],
            'inputOptions' => ['class' => 'input-small form-control fg-input'],
            'options'      => ['class' => 'form-group fg-float col-sm-offset-1']
        ],
        'input-m-b-5' => [
            'template'     => "<div class='fg-line fg-hide'>{input}</div>\n{label}\n{hint}\n{error}",
            'labelOptions' => ['class' => 'fg-label'],
            'inputOptions' => ['class' => 'input-small form-control fg-input '],
            'options'      => ['class' => 'form-group fg-float m-b-5']
        ],
         'hidden' => [
           
        ],
        
        'hiddenInput' => [
            'template'     => "{input}",
            'inputOptions' => ['class' => 'input-small form-control fg-input'],
        ],
        'pdob' => [
            'template'     => "<div class='fg-line dtp-container'>{input}</div>\n{label}\n{hint}\n{error}",
            'labelOptions' => ['class' => 'fg-label'],
            'inputOptions' => [
                'class'         => 'input-small form-control fg-input date-picker',
                'data-toggle'   => 'dropdown',
                'aria-expanded' => 'false',
            ],
            'options'      => ['class' => 'form-group fg-float']
        ],
        'datetime-picker' => [
            'template'     => "<div class='fg-line dtp-container'>{input}</div>\n{label}\n{hint}\n{error}",
            'labelOptions' => ['class' => 'fg-label'],
            'inputOptions' => [
                'class'         => 'input-small form-control fg-input datetime-picker',
                'data-toggle'   => 'dropdown',
                'aria-expanded' => 'false',
            ],
            'options'      => ['class' => 'form-group fg-float']
        ],
        'select' => [
            'template'     => "<div class='fg-line'><div class='select'>{input}</div></div>\n{label}\n{hint}\n{error}",
            'labelOptions' => ['class' => 'fg-label'],
            'inputOptions' => ['class' => 'input-small form-control'],
            'options'      => ['class' => 'form-group fg-float']
        ],
'select2' => [
				'template'     => "<div class='fg-line'><div class='select'>{input}</div></div>\n{label}\n{hint}\n{error}",
				'options'      => ['class' => 'form-group fg-float']
			],
        'select2SwapperInput' => [
				'template'     => "<div class='fg-line'>{input}</div>\n{label}\n{hint}\n{error}",
				'labelOptions' => ['class' => 'fg-label'],
				'inputOptions' => ['class' => 'input-small form-control fg-input', 'disabled'=>true, 'id'=>'select2Swapper'],
				'options'      => ['class' => 'form-group fg-float hidden', 'id'=>'select2SwapperContainer']
			],
        'textarea' => [
            'template'     => "<div class='fg-line'>{input}</div>\n{label}\n{hint}\n{error}",
            'labelOptions' => ['class' => 'fg-label'],
            'inputOptions' => ['class' => 'input-small form-control fg-input'],
            'options'      => ['class' => 'form-group fg-float']
        ],
        'checkbox' => [
            'template'     => "{input}<i class='input-helper'></i>{label}",
            'labelOptions' => ['class' => ''],
            'inputOptions' => ['class' => ''],
            'options'      => ['class' => 'checkbox m-b-15']
        ],
        'checkbox-header' => [
            'template'     => "<div class='fg-line fg-toggled'>{input}<i class='input-helper'></i></div>\n{label}",
            'labelOptions' => ['class' => 'fg-label'],
            'inputOptions' => ['class' => ''],
            'options'      => ['class' => 'form-group fg-float checkbox m-b-15']
        ],
        'username' => [
            'template'     => "<span class='input-group-addon'><i class='md md-person'></i></span><div class='fg-line'>{input}</div>\n{error}",
            'inputOptions' => ['class' => 'input-small form-control fg-input'],
            'options'      => ['class' => 'input-group m-b-20']
        ],
        'phone' => [
            'template'     => "<span class='input-group-addon'><i class='md md-phone'></i></span><div class='fg-line'>{input}</div>\n{error}",
            'inputOptions' => ['class' => 'input-small form-control fg-input'],
            'options'      => ['class' => 'input-group m-b-20']
        ],
        'email' => [
            'template'     => "<span class='input-group-addon'><i class='md md-mail'></i></span><div class='fg-line'>{input}</div>\n{error}",
            'inputOptions' => ['class' => 'input-small form-control fg-input'],
            'options'      => ['class' => 'input-group m-b-20']
        ],
        'address' => [
            'template'     => "<span class='input-group-addon'><i class='md md-location-city'></i></span><div class='fg-line'>{input}</div>\n{error}",
            'inputOptions' => ['class' => 'input-small form-control fg-input'],
            'options'      => ['class' => 'input-group m-b-20']
        ],
        'password' => [
            'template'     => "<span class='input-group-addon'><i class='md md-accessibility'></i></span><div class='fg-line'>{input}</div>\n{error}",
            'inputOptions' => ['class' => 'input-small form-control fg-input'],
            'options'      => ['class' => 'input-group m-b-20']
        ],
        'dob' => [
            'template'     => "<span class='input-group-addon'><i class='md md-access-time'></i></span><div class='dtp-container dropdown fg-line'>{input}</div>\n{error}",
            'inputOptions' => [
                'class'         => 'input-small form-control date-picker',
                'data-toggle'   => 'dropdown',
                'aria-expanded' => 'false',
            ],
            'options'      => ['class' => 'input-group m-b-20']
        ],
        
        
        'radioList' => [
            'item' => function($index, $label, $name, $checked, $value) {

                $return = '<div class="radio m-b-15">';
                $return .= '<label class="modal-radio">';
                $return .= $checked ? '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3" checked >' : '<input type="radio" name="' . $name . '" value="' . $value . '" tabindex="3">' ;
                $return .= '<i class="input-helper"></i>';
                $return .= ucwords($label);
                $return .= '</label>';
                $return .= '</div>';

                return $return;
            }
        ],
    ],
            
    
];