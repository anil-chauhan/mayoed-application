<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace common\widgets;
use yii\bootstrap\Tabs;

class MyTabs extends Tabs
{
    public $navType = 'tab-nav';

}
