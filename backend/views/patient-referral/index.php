<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\components\ArrayHelpers;


/* @var $this yii\web\View */
/* @var $searchModel common\models\PatientReferralSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Patient Referrals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= $this->render( '/partials/list-header', ['hidden'=>true] ); ?>

    <?= GridView::widget(array_merge([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export' => false,
       
        'responsive' => true,
        'striped' => true,
        'bootstrap'=>true,
        'hover'=>true,
        'toggleData'=>true,
        'pjax' => true, // pjax is set to always true for this demo
        'pjaxSettings'=>['neverTimeout'=>true,],
        'columns' => [
                [
                    'class' => 'kartik\grid\ExpandRowColumn',
                    'value' => function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    //'enableRowClick' => true,
                    'allowBatchToggle'=>true,
                    'detail'=> function ($model, $key, $index, $column) {
                        return Yii::$app->controller->renderPartial('partial_view', ['model'=> $model]);
                    },
                    'detailOptions'=>[ 'class'=> 'kv-state-enable',],
                    'expandOneOnly' => true,
                    'expandTitle' => 'Click to view detail',
                    'collapseTitle' => 'Click to hide detail',    
                    'detailAnimationDuration' => 150 ,
                    'width' =>'70px',

                ],
                'patient_name',
                'board_number',
                [
                    'attribute' => 'referral_type',
                    'value' => function($model){
                        return isset(Yii::$app->params['referralType'][$model['referral_type']]) ? Yii::$app->params['referralType'][$model['referral_type']]:'';
                    },
                    'filter' => Html::activeDropDownList($searchModel, 'referral_type', Yii::$app->params['referralType'], ['class'=>'form-control','prompt' => 'Priority']),
                ],
                'comment',
                'sex',
                'age',
//                [
//                    'header' => 'ED Location',
//                    'format' => 'raw',
//                    'value' => function ($model,$key,$index){
//                        return ($model->patient->ed_location) ? $model->patient->edLocation['name'] : 'None';
//                    }, 
//                    'filter' => Html::activeDropDownList($searchModel, 'ed_location', ArrayHelper::map(common\models\EdLocation::find()->all(), 'id', 'name'), ['class'=>'form-control','prompt' => 'Select']),
//                ],  
                [
                    'format' => 'raw',
                    'attribute' => 'timer',
                    'header' => 'Time in <br>ED',
                    'value' => function ($data) {
                        if(!empty($data['timer'])){
                           return '<div id="'.$data['id'].'" class="second" data-second="'.$data['timer'].'" data-to_diff="'.(strtotime("now")-$data['timer']).'"></div>';
                        }
                        return;
                    },
                ], 
//                [
//                    'header' => 'Bed Location',
//                    'format'    =>  'raw',
//                    'value' => function($model){
//                        if($model->bed_status) {
//                            $bed_time = '';
//                            $bed_awaiting_time = '';
//                            if( $model->bed_awaiting_time ){
//                                $bed_awaiting_time = ArrayHelpers::getHourMin($model->bed_awaiting_time);
//                            }
//                            if($model->bed_time){                            
//                                $bed_time = ArrayHelpers::getHourMin($model->bed_time);
//                            } 
//                            if($bed_time && $bed_awaiting_time){
//                                return $model->bed_status.' '.$bed_time.' ('.$bed_awaiting_time.')';
//                            } else if($bed_time){
//                                return $model->bed_status.' '.$bed_time;
//                            } else if($bed_awaiting_time){
//                                return $model->bed_status.' '.$bed_awaiting_time;
//                            }
//
//                        }
//                        return '';
//                    }
//                ],
//                [
//                    'attribute' => 'ed_doctor',
//                    'header' => 'Clinician',
//                    'format' => 'raw',
//                    'value' => function ($model,$kew,$index){
//                       return ($model->edDoctor) ? $model->edDoctor['name'] : 'None';
//                     },
//                    'filter' => Html::activeDropDownList($searchModel, 'ed_doctor', ArrayHelper::map(common\models\EdDoctor::getDoctorList(true), 'id', 'name'), ['class'=>'form-control','prompt' => 'Select']),
//                   
//                ],

//                [
//                    'attribute' => 'ed_nurse',
//                    'header' => 'ED Nurse',
//                    'format' => 'raw',
//                    'value' => function ($model,$kew,$index){
//                        
//                        return ($model->edNurse) ? $model->edNurse['name'] : 'None';
//                     },
//                    'filter' => Html::activeDropDownList($searchModel, 'ed_nurse', ArrayHelper::map(common\models\EdNurse::getNurseList(true), 'id', 'name'), ['class'=>'form-control','prompt' => 'Select']),
//                ],
//                 [
//                    'attribute' => 'ed_stream',
//                    'header' => 'Stream',
//                    'format' => 'raw',
//                    'value' => function ($model,$key,$index){
//                        return isset(Yii::$app->params['edStream'][$model->ed_stream]) ? Yii::$app->params['edStream'][$model->ed_stream] : 'None';
//                     },
//                    'filter' => Html::activeDropDownList($searchModel, 'ed_stream', Yii::$app->params['edStream'], ['class'=>'form-control','prompt' => 'Select']),
//                ],
//                'present_complaints',
//                [
//                    'label' => '',
//                    'format' => 'raw',
//                    'header' => 'Order Forms',
//                    'value' => function ($model){
//                     return Html::a('Order Forms', 
//                            ['/patient/' . $model['board_number'] . '?ward=' . Yii::$app->request->getQueryParam('CurrentWardCode')],
//                            ['class' => 'create-consultant-button btn btn-primary btn-sm']
//                        );
//                    },
//                ],
        ],
        ], Yii::$app->params['widgetOptions']['gridView'])); ?>

</div>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/www/js/jquery.tinytimer.min.js',['depends' => [\backend\assets\AppAsset::className()]]); ?>


<?php
    $this->registerJs("
 $(document).on('ready pjax:success', function () {
    $('.second').each(function(e){
        var id = $(this).attr('id');
        var second = $(this).data('to_diff');
        if(second) {
          var d = new Date();
          d.setSeconds(d.getSeconds() - second);
          $('#'+id).tinyTimer({ from: d, format: '%0H{ : } %0m' }); 
        }

    });
});");
?>