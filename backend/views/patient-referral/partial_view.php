<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */

if(!$model->patient){
?>
<div class="card hospital-groups-view" id="profile-main">
    <div class="pull-right col-xs-12 col-sm-2">
        
        
        
    </div>
    <div class="clearfix"></div>
    <div class="card-body card-padding" style="color:black!important">
        <div class="pmb-block">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    No data found
                </div>
            </div>
        </div>
    </div>
   
</div>

<?php 
    }else{ 
        $patient = $model->patient;
?>
<div class="card hospital-groups-view" id="profile-main">
    <div class="pull-right col-xs-12 col-sm-2">
        
    </div>
    <div class="clearfix"></div>
    <div class="card-body card-padding" style="color:black!important">
        <div class="pmb-block">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <div class="row">
                                        
                        <div class="col-sm-6 col-xs-12">
                            <?php if($patient->dob):?>
                            <dl class="dl-horizontal">
                                <dt>Date of Birth</dt>
                                <dd> <?= $patient->dob ?> </dd>
                            </dl>
                            <?php endif;if($patient->coordination_code):?>
                             <dl class="dl-horizontal">
                                <dt>CoordinationCategoryCode</dt>
                                <dd> <?= $patient->coordination_code ?> </dd>
                            </dl>
                            <?php endif;?>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <?php if($patient->PresentComplaint):?>
                            <dl class="dl-horizontal">
                                <dt>PresentComplaint</dt>
                                <dd> <?= $patient->PresentComplaint ?> </dd>
                            </dl>
                            <?php endif;if($patient->IncidentDetailsScrnNotesLine1):?>
                             <dl class="dl-horizontal">
                                <dt>Incident details 1</dt>
                                <dd> <?= $patient->IncidentDetailsScrnNotesLine1 ?> </dd>
                            </dl>
                            <?php endif;if($patient->IncidentDetailsScrnNotesLine2):?>
                             <dl class="dl-horizontal">
                                <dt>Incident details 2</dt>
                                <dd> <?= $patient->IncidentDetailsScrnNotesLine2 ?> </dd>
                            </dl>
                            <?php endif;if($patient->PASCoordinatedDate):?>
                             <dl class="dl-horizontal">
                                <dt>Coordinated Date </dt>
                                <dd> <?= $patient->PASCoordinatedDate ?> </dd>
                            </dl>
                            <?php endif;?>
                        </div>
                        

                    </div>
                    <?php // if(isset($showLog)): ?>
                    <h3 class="">Log Data </h3>
                     <?php
                     echo $this->render('/log/view', [
                                    'model' => $patient,

                                ]);
                        ?>
                    <?php // endif;?>
                </div>
            </div>
        </div>
    </div>
   
</div>
<?php } ?>