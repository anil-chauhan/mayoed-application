<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Patient Name</th>
            <th>Date of birth</th>
            <th>Address</th>
            <th>Admission Date</th>
            <th>Subject</th>
            <th>Referral Type</th>
<!--            <th>Comment</th>-->
        </tr>
    </thead>
    <tbody>
    <?php
    if(count($model)){
        foreach($model as $key => $referral){ ?>
            <tr>
                <td><?=$referral['patient_name']?></td>
                <td><?=$referral['dob']?></td>
                <td><?=$referral['address']?></td>
                <td><?=$referral['Tracker_admission_ed_time']?></td>
<!--                <td><?php //$referral['subject']?></td>-->
                <td>
                    <?php
                       $data =  isset(Yii::$app->params['referralType'][$referral['referral_type']]) ? Yii::$app->params['referralType'][$referral['referral_type']]:'' ;
                       echo $data;
                    ?>
                </td>
                <td><?=$referral['comment']?></td>
            </tr>
        <?php
        }
    } else {?>
        <tr>
            No data found.
        </tr>
    <?php }
    ?>  
    </tbody>
</table>