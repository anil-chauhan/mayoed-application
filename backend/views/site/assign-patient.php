<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Patient;

/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */
?>  
<?php
$form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['patient/update-location-data']),
            'options' => [
                'data' => [
                    'on-done' => 'saved-consultant'
                ],
            ],
            'id' => 'save-update-location-data-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ]);  
//print_r($ward_list);die;
?>
<div class="modal-header">
    <h3 id="referral-form-heading" class="modal-title">Add Patient Location</h3>
</div>

<div class="modal-body bgm-white  p-t-5">            


    <div class=" m-t-10 " id="part_1">
        <div class ="p-l-0">
            <input type="hidden" id="assign-id" name="Patient[ed_location]" value="" ><br />
            <div  class=" c-gray center ">
                <?php
                echo $form->field($model, 'Patient_id', Yii::$app->params['htmlTemplate']['select'])
                        ->dropDownList(ArrayHelper::map(Patient::getUnassignedPatientModel(), 'Patient_id', 'patient_name'), ['prompt' => '', 'style' => 'margin-top:10px;'])->label('Patient Name');
                ?>            
            </div>
         </div> 
        <div class="clearfix"></div>
        <div class=" m-t-10 c-gray " >
            <h4 class="m-b-20">Enter Verification Code</h4>
            <?php echo $form->field($model, 'code', Yii::$app->params['htmlTemplate']['inputPassword'])->passwordInput(['autocomplete' => 'off', 'maxlength' => true, 'onkeyup' => 'fncheckpass( this.value, "' . \yii\helpers\Url::to(['accesspass/checkpass']) . '" , "checkcode")'])->label(false); ?>
            <div style='color:red;' class='fill_error'>Please Fill the correct code</div>
            <div class="center col-sm-12 m-b-20">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg pull-right accesspass-btn' : 'btn btn-primary btn-lg pull-right', 'disabled' => 'disabled']) ?>
            </div>
        </div>
    </div> 
    <div class="clearfix"></div>
</div>  


<div class="clearfix"></div>

<div id="dismiss" class="modal-footer">
    <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
</div>
<input type='hidden' value='0' id='checkcode' />
<?php ActiveForm::end(); ?>
<div class="clearfix"></div>


