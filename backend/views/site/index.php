<?php
/* @var $this yii\web\View */
use common\components\NameHelper;
use common\components\ArrayHelpers;
use common\models\CoordinationCode;

$this->title = 'UHG Emergency Department Patient Tracking System';
?>

<style>
    .mini-charts-item{
        height: 80px;
    }
    .innerBox {
        height: 60px;
        width: inherit;
    }
    .p-t-65 {
        padding-top: 65px!important;
    }

    h4, .h4 {
        font-size: 12px;
        padding-left: 5px;
    }
    .float-right{
        float: right;
    }
    .float-left{
        float: left ;
    }

</style>

<?php
    echo $this->render('../partials/partial-pab', [
        'data' => $data,
    ]);
?>

<div class="modal fade" data-modal-color="bluegray" id="replace-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-lg">
        <div id="replaceData" class="modal-content">
            <?= $this->render('../forms/replace-form', ['model' => $model]) ?>

        </div>
    </div>
</div>

<div class="modal fade" data-modal-color="bluegray" id="assign-patient" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="AssignPatient" class="modal-content">
            <?= $this->render('assign-patient', ['model' => $model]) ?>

        </div>
    </div>
</div>

<div class="row">
<?php
foreach ($locations as $location):
    $backColor = (!empty($location->patient) && !$location->patient->is_discharge) ?  "" : "#ffffff" ;
    $bed_location = "";
    $isPatient = false;
    $class = 'assignPatient';
    $dataAttributes = "data-ed_location = '".$location->id."'";
    $border = "";

    if (!empty($location->patient) && !$location->patient->is_discharge){
        $isPatient = true;
        
        $border = "border-style:solid; border-color: red;";
        $class = 'replaceClass';
        $patient = $location->patient;
        $dataAttributes = 'data-id = "'.$patient->Patient_id.'" data-name = "'.NameHelper::getFullName($patient->forename, $patient->surname).'
                          " data-boardNumber = "'.$patient->board_number. '" data-dob = "'.$patient->dob.'
                          " data-episodeNumber = "'.$patient->episode_number. '"    
                          " data-address = "'.$patient->address. '" data-age = "'.$patient->age.'
                          " data-admission_date = "'.$patient->Tracker_admission_ed_time.'
                          " data-ed_location = "'.$patient->ed_location. '" data-ed_doctor = "'.$patient->ed_doctor.'
                          " data-ed_nurse = "'.$patient->ed_nurse. '" data-ed_stream = "'.$patient->ed_stream.'
                          " data-toggle = "modal" data-target-color ="bluegray" '; 
    
        if ($patient->is_bed_allocated) {
            $bed_time = '';
            $bed_awaiting_time = '';
            if ($patient->Tracker_awaiting_bed_time) {
                $bed_awaiting_time = $patient->Tracker_awaiting_bed_time;//ArrayHelpers::getHourMin($patient->Tracker_awaiting_bed_time);
            }
            if ($patient->Tracker_allocated_bed_time) {
                $bed_time = $patient->Tracker_allocated_bed_time;//ArrayHelpers::getHourMin($patient->Tracker_awaiting_bed_time);
            }
            if ($bed_time && $bed_awaiting_time) {
                $bed_location = $patient->is_bed_allocated . ' ' . $bed_time . ' (' . $bed_awaiting_time . ')';
            } else if ($bed_time) {
                $bed_location =  $patient->is_bed_allocated . ' ' . $bed_time;
            } else if ($bed_awaiting_time) {
                $bed_location = $patient->is_bed_allocated . ' ' . $bed_awaiting_time;
            }
        }
   }
    
?>

    <div class=" col-xs-6 col-sm-2 col-lg-1 div <?=$class?>"  <?=$dataAttributes?>>
        <div class="c-white bgm-bluegray" style="min-height: 200px; font-size: 11px; <?= ($bed_location)? $border : "" ?>  background-color: <?= $backColor ?>!important; margin-top:10px;">
        <?php if ($isPatient):
            $color = CoordinationCode::getCoordinationColorCode($patient->coordination_code);
            $color = ($color) ? $color : '';
            $textStyle = ($color == '#f9ea00') ? 'style = "color: #756e6e!important;"' : '' ;
            ?>
            <div class="innerBox " style=" background-color:<?= $color ?>!important; padding-top:1px;">
                <h4 class="c-white text-center" <?=$textStyle?>><?= $location->name ?>
                </h4>
                <h4  class=" c-white float-left" <?=$textStyle?>><?=NameHelper::getInitials($location->patient->forename, $location->patient->surname). " ".$location->patient->infection_status ?></h4>
                <h4  class=" c-white float-right" <?=$textStyle?>><?=ArrayHelpers::getHourMin($location->patient->Tracker_timer) ?></h4>
            </div>
            <div class="clearfix"></div>
            <div class="p-5">
            <?= "<b>B/N: </b>" . $location->patient->board_number ?><br/>
            <?= "<b>Clin: </b>" . (isset($location->patient->ed_doctor)? $location->patient->edDoctor['name'] : "Not Assigned") ?><br/>
            
                <?= "<b>Bed Location: </b>" . $bed_location ?><br/>
            </div>
            <?php
            
            else:  ?>
            <div class="innerBox text-left" style=" background-color: #556f7d;">
                <h4 class="c-white p-t-10"><?= $location->name ?>
                </h4>
            </div>
            <?php endif;?>
        </div>    
    </div>

<?php endforeach; ?>
</div>



<div class="modal fade " data-modal-color="bluegray" id="select-field" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-elg">
        <div id="" class="modal-content">
           <?= $this->render('../forms/select-field', ['model' => $model]) ?>
        </div>
    </div>
</div>
<!-- Episodes modal -->
<div class="modal fade" data-modal-color="bluegray" id="episodes-view" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog  modal-lg">
        <div class="modal-header bgm-gray">
            <h4 id="" class="modal-title">Episodes View</h4>
        </div>
        <div id="episodesData" class="modal-body bgm-white p-t-10">

        </div>
        <div id="dismiss" class="modal-footer bgm-gray">
            <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        </div>

    </div>
</div>
<div class="modal fade" data-modal-color="bluegray" id="discharge-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="dischargeData" class="modal-content">
           <?= $this->render('../forms/discharge-form', ['model' => $model]) ?>
        </div>
    </div>
</div>


<div class="modal fade" data-modal-color="bluegray" id="dropdown-view" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-lg">
        <div class="modal-header row bgm-gray">
            <div id="" class=" col-sm-6 modal-title h4">Patient View</div>
            <div class="col-sm-6">
                <button type="button" class="btn btn-link pull-right c-white" data-dismiss="modal"><span class="md-close md-2x"></span></button>
            </div>
        </div>
        <div id="dropdownData" class="modal-body bgm-white p-t-10">

        </div>
        <div id="dismiss" class="modal-footer bgm-gray">
            <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        </div>

    </div>
</div>
<script type="text/javascript">
    $('.replaceClass').click(function(){
        var element = $(this);
        name = element.data('name');
        boardNumber = element.data('boardnumber');
        episodeNumber = element.data('episodenumber');
        dob = element.data('dob');
        address = element.data('address');
        age = element.data('age');
        id = element.data('id');
        edStream = element.data('ed_stream');
        edLocation = element.data('ed_location');
        edDoctor = element.data('ed_doctor');
        edNurse = element.data('ed_nurse');
        
        admission_date = element.data('admission_date');
        
        //For Episode Popup
        $('#episodeAnchor').data('id',id);
        
        $('#dropdownAnchor').data('id',id);
        $('#episodeAnchor').data('board_number',boardNumber);
        
        //For Discharge Popup
        $('#dischargeAnchor').data('id',id);
        $('#dischargeAnchor').data('boardnumber',boardNumber);
        $('#dischargeAnchor').data('name',name);
        $('#dischargeAnchor').data('age',age);
        
        $('#dietetianAnchor').data('boardnumber', boardNumber)
        $('#dietetianAnchor').data('episodeNumber', episodeNumber)
        $('#dietetianAnchor').data('name', name)
        
        $('#patient-patient_name').val(name);
        $('#patient-dob').val(dob);
        $('#patient-age').val(age);
        $('#patient-address').val(address);
        $('#patient-admission_date').val(admission_date);
        $('#patient-board_number').val(boardNumber);
        $('#replace-id').val(id);
        $('#patient-ed_location').val($.trim(edLocation));
        $('#patient-ed_stream').val($.trim(edStream));
        $('#patient-ed_doctor').val($.trim(edDoctor));
        $('#patient-ed_nurse').val(edNurse);
        $('.toggle').addClass('hide');
        $('#part_1').removeClass('hide');
        focusInputs();
        $('#replace-form').modal({
            show: 'true',
            keyboard: true
        });
   }); 
    
    $('.dietetianClass').click(function(e){
        $('#nursereferral-board_number').val($('#dietetianAnchor').data('boardnumber'));
        $('#nursereferral-episode_number').val($('#dietetianAnchor').data('episodeNumber'));
        $('#nursereferral-patient_name').val($('#dietetianAnchor').data('name'));
                       
        $('#loader').hide();
        $('#dietetian-form').modal({
              show: 'true',
              keyboard: true 
         }); 

   });
    
    $('.assignPatient').click(function(){
       
        id = $(this).data('ed_location');
        $('#assign-id').val(id);
        $('#assign-patient').modal({
            show: 'true',
            keyboard: true
        });
        
    });
    $('#save-replace-form').on('submit', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();
        $('#fill_error').hide();
        currentElement = e;
        var showId = $(e).data('show_id');
        flag = true;
        if ($('#checkcode').val() == '1') {

            $('#fill_error').hide();
        } else {
            flag = false;
            $('#fill_error').show();
        }
        if (flag) {
            $('#loader').show();
            $('#patient-ed_doctor').attr("disabled", false);             
            $('#patient-ed_location').attr("disabled", false);             
            $('#patient-ed_nurse').attr("disabled", false);             
            $('#patient-referral_type').attr("disabled", false);  
            submitFormHandle(e);
        }
    });
    
    $('#save-update-location-data-form').on('submit', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();
        $('#fill_error').hide();
        currentElement = e;
        var showId = $(e).data('show_id');
        flag = true;
        if ($('#checkcode').val() == '1') {

            $('#fill_error').hide();
        } else {
            flag = false;
            $('#fill_error').show();
        }
        if (flag) {
            $('#loader').show();
            submitFormHandle(e);
        }
    });
    
    $('.dropdownClass').click(function(){
        id = $(this).data('id');
        var url = '<?=Yii::$app->urlManager->createUrl(['patient/get-dropdowndata?id='])?>' + id;
//        console.log(url);
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            success: function(data) {
                if(data.success) {
                    $('#dropdownData').html(data.dropdownData);
                    $('#loader').hide();
                    $('#dropdown-view').modal({
                        show: 'true',
                        keyboard: true
                    });
                } 
            }
        });
        
        
    });
        
        
$('.referralSelectClear').click(function(e){
   $('#patient-referral_type').val('');
   $('#select-field').modal('toggle');
});

    </script>