<?php 
use common\components\ArrayHelpers;
?>



<h3>Emergency Department (All Patients) - Current Status </h3><br>
Total Patients: <?=$data['edPatients']?> (Longest: <?=ArrayHelpers::getHourMin($data['longestInEd'])?>) <br>
Total Admitted: <?=$data['pnab']?><br>
Total Not Admitted:<?=$data['pab']?><br>
Awaiting Discharge:<?=$data['awaitingDischarge']?><br>

Triage: <?php echo $data['awaitingTriage'].' ('.$data['awaitingTriageAmbulance'].' Ambulance)';?> <br>
Under-16(Paeds): <?=$data['patients_under_16']?> <br> 
<br>
<h3>ED Patients (Not Admitted) - Current Status </h3><br>
Total Patients: <?=$data['pab']?> (Longest: <?=ArrayHelpers::getHourMin($data['pab_longest'])?>) <br>
Over 9 Hours: <?=$data['pab_p>9']?> <br>
Over 24 Hours: <?=$data['pab_p>24']?> <br>
Patients over 75: <?=$data['pab_>75']?><br>
Over 75(Greater Than 9 Hours): <?=$data['pab_75+p>9']?> <br>
Over 75(Greater Than 24 Hours): <?=$data['pab_75+p>24']?> <br>
<br>
Number of Patients 0-4 hours: <?=$data['pab_0-4']?> <br>
Number of Patients 4-8 hours: <?=$data['pab_4-8']?> <br>
Number of Patients >8 hours: <?=$data['pab_>8']?> <br>
<br>

<h3>ED Patients (Admitted) - Current Status</h3> <br>
Total Patients: <?=$data['pnab']?>  (Longest: <?=ArrayHelpers::getHourMin($data['pnab_longest'])?>) <br>
Over 9 Hours: <?=$data['pnab_p>9']?> <br>
Over 24 Hours: <?=$data['pnab_p>24']?> <br>
Patients over 75: <?=$data['pnab_>75']?><br>
Over 75(Greater Than 9 Hours): <?=$data['pnab_75+p>9']?> <br>
Over 75(Greater Than 24 Hours): <?=$data['pnab_75+p>24']?> <br>
<br>
Number of Patients 0-4 hours: <?=$data['pnab_0-4']?> <br>
Number of Patients 4-8 hours: <?=$data['pnab_4-8']?> <br>
Number of Patients >8 hours: <?=$data['pnab_>8']?> <br>
<br>
<h3>Co-ordinated Patients</h3>  <br>
C1 : <?=$data['C1']?> <br>
C2 : <?=$data['C2']?> <br>
C3 : <?=$data['C3']?> <br>
C4 : <?=$data['C4']?> <br>
C5 : <?=$data['C5']?> <br>
M1 : <?=$data['M1']?> <br>
M2 : <?=$data['M2']?> <br>
M3 : <?=$data['M3']?> <br>
M4 : <?=$data['M4']?> <br>
M5 : <?=$data['M5']?> <br>

Other Category : <?=$data['OtherCoordCode']?>


