<?php 
use common\components\ArrayHelpers;
?>

<style>

/*    .ed-stats-color{
        background-color: #607d8b!important;    
    }
    #mail-table {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    #mail-table td, #mail-table th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    #mail-table tr:nth-child(even){background-color: #f2f2f2;}

    #mail-table tr:hover {background-color: #ddd;}

    #mailtable th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #4CAF50;
        color: white;
    }*/
/*    .center {
        text-align: center;
        margin-right: 1em;
      }*/
/*  @media (max-width: 768px) {
        .cu-col-sm-2 {
            width: 100%;
            float: left;
        }
    }
  @media (min-width: 768px) {
        .cu-col-sm-2 {
            width: 60%;
            float: left;
        }
    }
    
    .cu-col-sm-2 {
        position: relative;
        min-height: 1px;
        padding-left: 12px;
        padding-right: 12px;
    }*/
</style>
Current Total Patients : <?=$data['edPatients']?> <br>
Triage : <?php echo $data['awaitingTriage'].' ('.$data['awaitingTriageAmbulance'].' Ambulance)';?> <br>
Patients Awaiting Beds : <?php echo $data['awaitingBed'].' (Longest: '.ArrayHelpers::getHourMin($data['longestInEd']).')'?> <br>
Patient over 9 Hours : <?php echo $data['>9']?> <br>
Patient over 24 Hours : <?php echo $data['>24'];?> <br>
Patient over 75 : <?=$data['75+p']?> <br>
Patients over 75 greater than 9 hours : <?=$data['75+p>9']?><br>
Patients over 75 greater than 24 hours : <?=$data['75+p>24']?> <br>
Number of patients 0-4 hours : <?=$data['0-4']?> <br>
Number of patients 4-8 hours : <?=$data['4-8']?> <br>
Number of patients >8 hours : <?=$data['>8']?> <br>
Patients under 16 Yrs : <?= $data['patients_under_16'].'<br>'.(($data['edPatients']) ? '('. round((100*$data['patients_under_16']/$data['edPatients']),2).'%)' : 0 );?>  <br>
<h3>Co-ordinated Patients</h3>  <br>
C1 : <?=$data['C1']?> <br>
 C2 : <?=$data['C2']?> <br>
 C3 : <?=$data['C3']?> <br>
C4 : <?=$data['C4']?> <br>
C5 : <?=$data['C5']?> <br>
 M1 : <?=$data['M1']?> <br>
M2 : <?=$data['M2']?> <br>
M3 : <?=$data['M3']?> <br>
M4 : <?=$data['M4']?> <br>
M5 : <?=$data['M5']?>

