<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm; 

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
?>

  
<div class="lc-block toggled" id="l-login">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    
    <?= $form->field($model, 'username', Yii::$app->params['htmlTemplate']['email'])->input('text', ['placeholder' => 'Email Address']) ?>
    <?= $form->field($model, 'password', Yii::$app->params['htmlTemplate']['password'])->passwordInput(['placeholder' => 'Password']) ?>

    <?= $form->field($model, 'rememberMe', Yii::$app->params['htmlTemplate']['checkbox'])->checkbox(array(), false) ?>

    <button class="btn btn-login btn-danger btn-float" type="submit"><i class="md md-arrow-forward"></i></button>

    <ul class="login-navigation">
        <li class="bgm-red"><?= Html::a('Register', ['/site/register']) ?></li>
        <li class="bgm-orange">Forgot Password?</li>
    </ul>
    <?php ActiveForm::end(); ?>
</div>
