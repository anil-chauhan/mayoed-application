<?php
/* @var $this yii\web\View */
use common\components\NameHelper;
use common\components\ArrayHelpers;
use common\components\CommonHelper;
$this->title = 'UHG Emergency Department Patient Tracking System';
?>


<style>
    .mini-charts-item{
        height: 80px;
    }
    .innerBox {
        height: 62px;
        width: inherit;
    }
    .waitingBox {
        height: auto!important;
        width: inherit;
    }
    .p-t-65 {
        padding-top: 65px!important;
    }

    h4, .h4 {
        font-size: 24px;
        padding-left: 5px;
        margin-top: 0px;
    }
    .col-custom{
        width: 9.5%;
        float:left;
    }
    .float-right{
        float: right;
    }
    .float-left{
        float: left;
    }
    .m-r-2{
        margin-right: 2px!important;
    }
    .outer-div{
        min-height: 210px;
        font-size: 22px;
    }
    @media (min-width: 1300px){
        .modal-elg {
            width: 1800px;
        }
    }
</style>

<?php
    echo $this->render('../partials/partial-pab', [
        'data' => $data,
    ]);
?>

<div class="modal fade" data-modal-color="bluegray" id="replace-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-lg">
        <div id="replaceData" class="modal-content">
             
            <?= $this->render('../forms/replace-form', ['model' => $model]) ?>

        </div>
    </div>
</div>

<div class="modal fade" data-modal-color="bluegray" id="assign-patient" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="AssignPatient" class="modal-content">
            <?= $this->render('assign-patient', ['model' => $model]) ?>

        </div>
    </div>
</div>
<!-- Episodes modal -->
<div class="modal fade" data-modal-color="bluegray" id="episodes-view" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog  modal-lg">
        <div class="modal-header bgm-gray">
            <h4 id="" class="modal-title">Episodes View</h4>
        </div>
        <div id="episodesData" class="modal-body bgm-white p-t-10">

        </div>
        <div id="dismiss" class="modal-footer bgm-gray">
            <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        </div>

    </div>
</div>

<div class="modal fade" data-modal-color="bluegray" id="discharge-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="dischargeData" class="modal-content">
            <?= $this->render('../forms/discharge-form', ['model' => $model]) ?>

        </div>
    </div>
</div>
<div class="row m-l-5">
<?php
$column = null;
foreach ($locations as $location) :
    if($column != $location->location_column_order){
        if(!$column){
            $column = $location->location_column_order;
        } else {
            $column = $location->location_column_order;
            //If there is previous col-custom div then closing it
            echo '</div>';
        }
        //Starting a new row on change of column order
        echo '<div class =" col-custom m-r-5">';
    }
       
    if(!empty($location->patients)):
        $flagDischarge = true;
        //$patient = $location->patients;
         foreach ($location->patients as $patient):
         if(!$patient->is_discharge ):
                $flagDischarge = false;
                echo CommonHelper::getPatientCell($location->name, $patient);
         endif;
        endforeach;
        if($flagDischarge):
            echo CommonHelper::getEmptyCell($location);
        endif;
    else:
        echo CommonHelper::getEmptyCell($location);
    endif;
endforeach;
?>
<!--Closing the last column ie; <div class="col-custom">-->
</div> 

<!--Waiting Room Patients Listing-->
<div class="col-custom ">
<?php 

$halfCount = (int) (count($patients) / 2);
$patientCount = 0;
foreach ($patients as $patient) {
    $patientCount++;
    echo CommonHelper::getWaitingPatientCell($patient);
    if ($patientCount == $halfCount) { 
        echo '</div><div class="col-custom">';
    }
}
?>

<div class="modal fade " data-modal-color="bluegray" id="select-field" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-elg">
        <div id="" class="modal-content">
             <?= $this->render('../forms/select-field', ['model' => $model]) ?>
        </div>
    </div>
</div>
    

<div class="modal fade" data-modal-color="bluegray" id="dropdown-view" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-lg">
        <div class="modal-header row bgm-gray">
            <div id="" class=" col-sm-6 modal-title h4">Patient View</div>
            <div class="col-sm-6">
                <button type="button" class="btn btn-link pull-right c-white" data-dismiss="modal"><span class="md-close md-2x"></span></button>
            </div>
        </div>
        <div id="dropdownData" class="modal-body bgm-white p-t-10">

        </div>
        <div id="dismiss" class="modal-footer bgm-gray">
            <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        </div>

    </div>
</div>
<script type="text/javascript">
    $('.replaceClass').click(function(){ 
        var element = $(this);
        name = element.data('name');
        boardNumber = element.data('boardnumber');
        episodeNumber = element.data('episodenumber');
        dob = element.data('dob');
        address = element.data('address');
        age = element.data('age');
        id = element.data('id');
        edStream = element.data('ed_stream');
        edLocation = element.data('ed_location');
        edDoctor = element.data('ed_doctor');
        edNurse = element.data('ed_nurse');
        
        admission_date = element.data('admission_date');
        
        //For Episode Popup
        $('#episodeAnchor').data('id',id);
        
        $('#dropdownAnchor').data('id',id);
        $('#episodeAnchor').data('board_number',boardNumber);
        
        //For Discharge Popup
        $('#dischargeAnchor').data('id',id);
        $('#dischargeAnchor').data('boardnumber',boardNumber);
        $('#dischargeAnchor').data('name',name);
        $('#dischargeAnchor').data('age',age);
        
        $('#dietetianAnchor').data('boardnumber', boardNumber)
        $('#dietetianAnchor').data('episodeNumber', episodeNumber)
        $('#dietetianAnchor').data('name', name)

        
        $('#patient-patient_name').val(name);
        $('#patient-dob').val(dob);
        $('#patient-age').val(age);
        $('#patient-address').val(address);
        $('#patient-admission_date').val(admission_date);
        $('#patient-board_number').val(boardNumber);
        $('#replace-id').val(id);
        $('#patient-referral_type').val('');
        $('#replace-id').val(id);
        $('#patient-ed_location').val($.trim(edLocation));
        $('#patient-ed_stream').val($.trim(edStream));
        $('#patient-ed_doctor').val($.trim(edDoctor));
        $('#patient-ed_nurse').val(edNurse);
        $('.toggle').addClass('hide');
        $('#part_1').removeClass('hide');
        focusInputs();
        $('#replace-form').modal({
            show: 'true',
            keyboard: true
        });
        
    });
    
    $('.dietetianClass').click(function(e){
        $('#nursereferral-board_number').val($('#dietetianAnchor').data('boardnumber'));
        $('#nursereferral-episode_number').val($('#dietetianAnchor').data('episodeNumber'));
        $('#nursereferral-patient_name').val($('#dietetianAnchor').data('name'));
                       
        $('#loader').hide();
        $('#dietetian-form').modal({
              show: 'true',
              keyboard: true 
         }); 

   });
    
    $('.assignPatient').click(function(){
       
        id = $(this).data('ed_location');
        console.log(id);
        $('#assign-id').val(id);
        $('#assign-patient').modal({
            show: 'true',
            keyboard: true
        });
        
    });
    $('#save-replace-form').on('submit', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();
        $('#fill_error').hide();
        currentElement = e;
        var showId = $(e).data('show_id');
        flag = true;
        if ($('#checkcode').val() == '1') {

            $('#fill_error').hide();
        } else {
            flag = false;
            $('#fill_error').show();
        }
        if (flag) {
            $('#loader').show();
            $('#patient-ed_doctor').attr("disabled", false);             
            $('#patient-ed_location').attr("disabled", false);             
            $('#patient-ed_nurse').attr("disabled", false);             
            $('#patient-referral_type').attr("disabled", false);  
            submitFormHandle(e);
        } 
    });
    
    $('#save-update-location-data-form').on('submit', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();
        $('#fill_error').hide();
        currentElement = e;
        var showId = $(e).data('show_id');
        flag = true;
        if ($('#checkcode').val() == '1') {

            $('#fill_error').hide();
        } else {
            flag = false;
            $('#fill_error').show();
        }
        if (flag) {
            $('#loader').show();
            submitFormHandle(e);
        }
    });

    $('.dropdownClass').click(function(){
        id = $(this).data('id');
        var url = '<?=Yii::$app->urlManager->createUrl(['patient/get-dropdowndata?id='])?>' + id;
//        console.log(url);
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            success: function(data) {
                if(data.success) {
                    $('#dropdownData').html(data.dropdownData);
                    $('#loader').hide();
                    $('#dropdown-view').modal({
                        show: 'true',
                        keyboard: true
                    });
                } 
            }
        });
        
        
    });
    
        
$('.referralSelectClear').click(function(e){
   $('#patient-referral_type').val('');
   $('#select-field').modal('toggle');
});

    
    </script>
    
<?= $this->registerJs('reload(30000, 30000);');?>