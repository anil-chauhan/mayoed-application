<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Login';
?>


<div class="lc-block toggled" id="l-login">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
    
    <?= $form->field($model, 'firstname', Yii::$app->params['htmlTemplate']['username'])->textInput(['placeholder' => 'First Name'])?>
    <?= $form->field($model, 'lastname', Yii::$app->params['htmlTemplate']['username'])->textInput(['placeholder' => 'Last Name']) ?>
    
    <?= $form->field($model, 'address1', Yii::$app->params['htmlTemplate']['address'])->textInput(['placeholder' => 'Address 1']) ?>
    <?= $form->field($model, 'address2', Yii::$app->params['htmlTemplate']['address'])->textInput(['placeholder' => 'Address 2']) ?>
    <?= $form->field($model, 'county', Yii::$app->params['htmlTemplate']['address'])->textInput(['placeholder' => 'County']) ?>
    <?= $form->field($model, 'phone', Yii::$app->params['htmlTemplate']['phone'])->textInput(['placeholder' => 'Phone Number']) ?>
    <?= $form->field($model, 'dateofbirth', Yii::$app->params['htmlTemplate']['dob'])->textInput(['placeholder' => 'Date of Birth']) ?>
    
    <?= $form->field($model, 'email', Yii::$app->params['htmlTemplate']['email'])->textInput(['placeholder' => 'Email']) ?>
    <?= $form->field($model, 'password', Yii::$app->params['htmlTemplate']['password'])->passwordInput(['placeholder' => 'Password']) ?>

    <button class="btn btn-login btn-danger btn-float" type="submit"><i class="md md-arrow-forward"></i></button>

    <ul class="login-navigation">
        <li class="bgm-red"><?= Html::a('Login', ['/site/login']) ?></li>
        <li class="bgm-orange">Forgot Password?</li>
    </ul>
    <?php ActiveForm::end(); ?>
</div>
