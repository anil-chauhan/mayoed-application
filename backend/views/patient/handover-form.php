<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EdNurse;

/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */

$form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['patient/create-handover']),
            'options' => [
                'data' => [
                    'on-done' => 'saved-consultant'
                ],
            ],
            'id' => 'save-handover-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ]);
?>


<div class="modal-header">
    <h4 id="referral-form-heading" class="modal-title">Handover Form</h4>
</div>
<div class="modal-body bgm-white  p-t-5">            

    <div class="toggle m-t-10" id="part_31">
        <div class ="p-l-0">
            <div class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown">
                Select User Nurse
            </div>

            <div  class="col-sm-12 c-gray center handover-nurse">
                <?php
                echo $form->field($model, 'ed_nurse_from', Yii::$app->params['htmlTemplate']['select'])
                        ->dropDownList(ArrayHelper::map(EdNurse::getNurseList(true), 'id', 'name'), ['prompt' => '', 'style' => 'margin-top:10px;']);
                ?>            
            </div>
            <div class="center col-sm-12">
                <a class="btn btn-sm bgm-green pull-right" data-required="from_nurse" data-show_id="part_32" onclick="toggleForm(this)">Next</a>        
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>        
    </div> 

    <div class="toggle m-t-10 hide " id="part_32">
        <div class ="p-l-0">
            <div class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown">
                Handover Patient List
            </div>            
            <div  class="col-sm-12 c-gray center" id="handover-list">

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="center col-sm-12">
            <a class="btn btn-sm bgm-red pull-left" data-show_id="part_31" onclick="toggleForm(this)">Prev</a>
            <a class="btn btn-sm bgm-green pull-right" data-required="patient_list" data-show_id="part_33" onclick="toggleForm(this)">Next</a>        
        </div>
        <div class="clearfix"></div>
    </div> 

    <div class="toggle m-t-10 hide" id="part_33">
        <div class ="p-l-0">
            <div class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown">
                Select User Nurse
            </div>

            <div  class="col-sm-12 c-gray center handover-nurse">
                <?php
                echo $form->field($model, 'ed_nurse_to', Yii::$app->params['htmlTemplate']['select'])
                        ->dropDownList(ArrayHelper::map(EdNurse::getNurseList(true), 'id', 'name'), ['prompt' => '', 'style' => 'margin-top:10px;']);
                ?>            
            </div>

            <div class="clearfix"></div>
        </div>
        <div class="center col-sm-12">
            <a class="btn btn-sm bgm-red pull-left" data-show_id="part_32" onclick="toggleForm(this)">Prev</a>
            <a class="btn btn-sm bgm-green pull-right" data-required="to_nurse" data-show_id="part_34" onclick="toggleForm(this)">Next</a>        
        </div>
        <div class="clearfix"></div>   
    </div> 

    <div class=" m-t-10 toggle hide" id="part_34">
        <div class="row">
            <div class="col-sm-5 c-black  h3">Nurse Transferring</div> <div class="h1 col-sm-1"> | </div>
            <div class="col-sm-5 c-gray h3" id="review-ed_nurse_from"></div>
        </div> 
        <div class="row">
            <div class="col-sm-5 c-black h3">Transferring to</div> <div class="h1 col-sm-1"> | </div>
            <div class="col-sm-5 c-gray h3" id="review-ed_nurse_to"></div>
        </div>  
        <div class="row">
            <div class="col-sm-12 c-black h3">Patients to Handover</div> 
            <div class="col-sm-12 c-gray h3" id="review-list"></div>
        </div> 
        <div class="clearfix"></div>
        <div class=" m-t-10 c-gray " >
            <h4 class="m-b-20">Enter Verification Code</h4>
            <?php echo $form->field($model, 'code', Yii::$app->params['htmlTemplate']['inputPassword'])->textInput(['autocomplete' => 'off', 'maxlength' => true, 'onkeyup' => 'fncheckpass( this.value, "'.\yii\helpers\Url::to(['accesspass/checkpass']).'" , "checkcode-handover")'])->label(false); ?>
            <div style='' class="fill_error c-red " id=''>Please fill the correct code</div>


            <div class="center col-sm-12 m-b-20">
                `   <a class="btn btn-sm bgm-red pull-left" data-show_id="part_33" onclick="toggleForm(this)">Prev</a>
                <?= Html::submitButton('Create', ['class' => 'btn btn-sm btn-success btn-lg pull-right accesspass-btn', 'disable' => 'disabled']) ?>
            </div>
        </div>
    </div> 
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>

<div id="dismiss" class="modal-footer bgm-gray">
    <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
</div>
<input type='hidden' value='0' id='checkcode-handover' />
<?php ActiveForm::end(); ?>
<div class="clearfix"></div>



