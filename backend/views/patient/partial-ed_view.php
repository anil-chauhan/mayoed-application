<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\User;

/* @var $this yii\web\View */
/* @var $model common\models\Emergency */

//$modelSP = json_decode($model->sp_object, true);
//$data = common\models\User::getLogData($model->logs);
?>

<div class="card hospital-groups-view" id="profile-main">
    <div class="pull-right col-xs-12 col-sm-2">
        
        
        
    </div>
    <div class="clearfix"></div>
    <div class="card-body card-padding" style="color:black!important">
        <div class="pmb-block">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <div class="row m-b-30">
                        <h3 class="">Referral Data</h3>
                         <?php
                                echo $this->render('/patient-referral/referral-view', [
                                    'model' => $model->patientReferrals,
                                ]);
                            ?>
                    </div>
                    <h3 class="m-t-30">Patient Data </h3>
                                        
                        <div class="col-sm-6 col-xs-12">
                            <?php //if($modelSP['DateOfBirth']):?>
                            <dl class="dl-horizontal">
                                <dt>Date of Birth</dt>
                                <dd> <?=  $model->dob ?> </dd>
                            </dl>
                            <?php // endif; if( isset($modelSP['Phone']) && $modelSP['Phone']):?>
                            <dl class="dl-horizontal">
                                <dt>Phone</dt>
                                <dd> <?=  $model->PhoneNumber ?> </dd>
                            </dl>
                            <?php //endif; if($modelSP['AddrLine1']):?>
                            <dl class="dl-horizontal">
                                <dt>Address</dt>
                                <dd> <?= $model->address ?> </dd>
                            </dl>
                            <?php // endif; if($modelSP['AddrLine2']):?>
<!--                            <dl class="dl-horizontal">
                                <dt>Address line 2</dt>
                                <dd> <?php // $modelSP['AddrLine2'] ?> </dd>
                            </dl>
                            <?php //endif; if($modelSP['AddrLine3']):?>
                            <dl class="dl-horizontal">
                                <dt>Address line 3</dt>
                                <dd> <?php // $modelSP['AddrLine3'] ?> </dd>
                            </dl>
                            <?php // endif; if($modelSP['AddrLine4']):?>
                            <dl class="dl-horizontal">
                                <dt>Address line 4</dt>
                                <dd> <?php // $modelSP['AddrLine3'] ?> </dd>
                            </dl>
                            <?php // endif; if($modelSP['AreaOfResCode']):?>
                            <dl class="dl-horizontal">
                                <dt>Residence Code</dt>
                                <dd> <?php // $modelSP['AreaOfResCode'] ?> </dd>
                            </dl>-->
                            <?php //endif; if(isset($modelSP['NextOfKinName'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of Kin</dt>
                                <dd> <?= $model->NextOfKinName ?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['CoordinationCategoryCode'])):?>
<!--                            <dl class="dl-horizontal">
                                <dt>Next of kin address 1</dt>
                                <dd> <?php //date('d-m-Y', strtotime($modelSP['CoordinationCategoryCode'])); ?> </dd>
                            </dl>-->
                            <?php // endif; if(isset($modelSP['NextOfKinAddrLine1'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin address 1</dt>
                                <dd> <?= $model->NextOfKinAddrLine1 ;?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['NextOfKinAddrLine2'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin address 2</dt>
                                <dd> <?= $model->NextOfKinAddrLine2 ?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['NextOfKinAddrLine3'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin address 3</dt>
                                <dd> <?= $model->NextOfKinAddrLine3 ?> </dd>
                            </dl>
							 
                            <?php // endif; if(isset($modelSP['NextOfKinAddrLine4'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin address 4</dt>
                                <dd> <?= $model->NextOfKinAddrLine4 ?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['NextOfKinPhone'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin phone</dt>
                                <dd> <?= $model->NextOfKinPhone ?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['NextOfKinWorkPhone'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin work phone</dt>
                                <dd> <?= $model->NextOfKinWorkPhone ?> </dd>
                            </dl>
                            <?php //endif;if(isset($modelSP['CoordinationCategoryCode'])):?>
                             <dl class="dl-horizontal">
                                <dt>CoordinationCategoryCode</dt>
                                <dd> <?= $model->coordination_code ?> </dd>
                            </dl>
                             <?php //endif; ?>
                        </div>
                       <div class="col-sm-6 col-xs-12">
                            <?php //if(isset($modelSP['ChartNum'])):?>
                            <!--<dl class="dl-horizontal">
                                <dt>Chart Num</dt>
                                <dd> <?php // $modelSP['ChartNum'] ?> </dd>
                            </dl>-->
                            <?php // endif; if(isset($modelSP['MobileNum']) && $modelSP['MobileNum']):?>
                            <dl class="dl-horizontal">
                                <dt>Mobile Number</dt>
                                <dd> <?= $model->Mobile ?> </dd>
                            </dl>
                            <?php //endif; if(isset($modelSP['PresentComplaint'])):?>
                            <dl class="dl-horizontal">
                                <dt>Present complaint</dt>
                                <dd> <?= $model->PresentComplaint ?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['IncidentDetailsScrnNotesLine1'])):?>
                            <dl class="dl-horizontal">
                                <dt>Incident details 1</dt>
                                <dd> <?= $model->IncidentDetailsScrnNotesLine1 ?> </dd>
                            </dl>
                             <?php //endif; if(isset($modelSP['IncidentDetailsScrnNotesLine2'])):?>
                            <dl class="dl-horizontal">
                                <dt>Incident details 2</dt>
                                <dd> <?= $model->IncidentDetailsScrnNotesLine2 ?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['ModeOfArrivalCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>Mode of arrival</dt>
                                <dd> <?= $model->AEModeOfArrivalCode ?> </dd>
                            </dl>
                             <?php //endif; if(isset($modelSP['CoordinatedDate'])):?>
                            <dl class="dl-horizontal">
                                <dt>Coordinated Date</dt>
                                <dd> <?= $model->PASCoordinatedDate?> </dd>
                            </dl>
                            <?php //endif; if(isset($modelSP['GPCode'])):?>
<!--                            <dl class="dl-horizontal">
                                <dt>GP code</dt>
                                <dd> <?php // $model->ReferringGPCode ?> </dd>
                            </dl>-->
                             <?php // endif; if(isset($modelSP['ReferringGPCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>Referring GP code</dt>
                                <dd> <?= $model->ReferringGPCode ?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['ReferringGPName'])):?>
                            <dl class="dl-horizontal">
                                <dt>Referring GP name</dt>
                                <dd> <?= $model->ReferringGPName ?> </dd>
                            </dl>
                            <?php //endif; if(isset($modelSP['GPPracticeAddrLine1'])):?>
                            <dl class="dl-horizontal">
                                <dt>Referring GP add 1</dt>
                                <dd> <?= $model->GPPracticeAddrLine1 ?> </dd>
                            </dl>
                            <?php //endif; if(isset($modelSP['GPPracticeAddrLine2'])):?>
                            <dl class="dl-horizontal">
                                <dt>Address 2</dt>
                                <dd> <?= $model->GPPracticeAddrLine2 ?> </dd>
                            </dl>
                            <?php //endif; if(isset($modelSP['GPPracticeAddrLine3'])):?>
                            <dl class="dl-horizontal">
                                <dt>Address 3</dt>
                                <dd> <?= $model->GPPracticeAddrLine3 ?> </dd>
                            </dl>
                            <?php //endif; if(isset($modelSP['GPPracticeAddrLine4'])):?>
                            <dl class="dl-horizontal">
                                <dt>Address 4</dt>
                                <dd> <?= $model->GPPracticeAddrLine4 ?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['GPPracticePhoneNum'])):?>
<!--                            <dl class="dl-horizontal">
                                <dt>Referring GP phone</dt>
                                <dd> <?php // $modelSP['GPPracticePhoneNum'] ?> </dd>
                            </dl>-->
                            <?php //endif;?>
                        </div>
                        

                    </div>
                <div class="clearfix"></div>
                    <div class="row m-b-30">
                        <?php // if(isset($showLog)): ?>
                        <h3 class="">Log Data </h3>
                         <?php
                         echo $this->render('/log/view', [
                                'model' => $model,
                            ]);
                        ?>
                        <?php // endif;?>
                    </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="pmb-block">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <div class="row m-b-30">
                        <h3 class="">This Attendence : </h3>
<!--                        <div class="col-sm-6 col-xs-12">
                            <?php //if(isset( $modelSP['AEReferralSourceCode'] )){?>
                            <dl class="dl-horizontal">
                                <dt>Referred to ED by:</dt>
                                <dd> <?php // $modelSP['AEReferralSourceCode'] ?> </dd>
                            </dl>
                            <?php //} ?>
                            <dl class="dl-horizontal">
                                <dt>Registration:</dt>
                                <dd> <?php // $modelSP['AEAttendanceDate'].' with '.$modelSP['PresentComplaint'] ?> </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Triage:</dt>
                                <dd> <?php  //$modelSP['CoordinatedDate'].' with '.$modelSP['IncidentDetailsScrnNotesLine1'].' '. $modelSP['IncidentDetailsScrnNotesLine2']?> </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Triage Category:</dt>
                                <dd> <?php // $modelSP['CoordinationCategoryCode']?> </dd>
                            </dl>
                            <?php //if(isset($modelSP['AEReferralSourceCode'] )){?>
                                <dl class="dl-horizontal">
                                    <dt>Triage Vitals:</dt>
                                    <dd> <?php //'BP: '.$modelSP['BloodPressure'].'HR: '.$modelSP['Pulse'].'RR: '.$modelSP['Respiration'].'SPO<sub>2</sub>: '.$modelSP['Respiration'].'% (FiO2 not given) T<sup>0</sup>:'.$modelSP['Temperature'].' BM :'. $modelSP['BM'] ?> </dd>
                                </dl>  
                            <?php //} ?>
                            <?php // if(isset($modelSP['InfectionStatus'] )){?>
                            <dl class="dl-horizontal">
                                <dt>Infection Alert :</dt>
                                <dd> <?php // $modelSP['InfectionStatus'] ?> </dd>
                            </dl>
                            
                            <?php //} ?>
                        </div> -->
                        <div class="col-sm-6 col-xs-12">
                            <dl class="dl-horizontal">
                                <dt>First Clinician :</dt>
<!--                                <dd> Taken From Ed Tracker <?php // $data['first']['name']?> at <?php // $data['first']['time']?></dd>-->
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Current Clinician:</dt>
<!--                                <dd> Taken From Ed Tracker <?php // $data['last']['name']?> at <?php // $data['first']['time']?> </dd>-->
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Referred To:</dt>
                                <dd>  </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Discharge To:</dt>
                                <dd> </dd>
                            </dl>
                        </div>
                        <div class="clearfix"></div>
                        <h3 class="">Patient Demographics : </h3>
<!--                        <div class="col-sm-6 col-xs-12">
                            <?php //if($modelSP['BoardNum']):?>
                            <dl class="dl-horizontal">
                                <dt>Board Number:</dt>
                                <dd> <?php // $modelSP['BoardNum'] ?> </dd>
                            </dl>
                            <?php // endif; if($modelSP['AEAttendanceDate']):?>
                            <dl class="dl-horizontal">
                                <dt>Date of Birth:</dt>
                                <dd> <?php //$modelSP['AEAttendanceDate'].' with '.$modelSP['PresentComplaint'] ?> </dd>
                            </dl>
                            <?php //endif;?>
                        </div>-->
<!--                        <div class="col-sm-6 col-xs-12">
                            <?php //if(isset($modelSP['ChartNum'])):?>
                            <dl class="dl-horizontal">
                                <dt>Chart Number:</dt>
                                <dd> <?php // $modelSP['ChartNum']?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['CoordinationCategoryCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>Address:</dt>
                                <dd> <?php // $modelSP['AddrLine1'].', '.$modelSP['AddrLine2'].', '. $modelSP['AddrLine3'].', '. $modelSP['AddrLine4']?> </dd>
                            </dl>
                            <?php //endif; if(isset($modelSP['CoordinationCategoryCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>Telephone:</dt>
                                <dd> M: H: W: </dd>
                            </dl>
                            <?php //endif;?>
                        </div>-->
                        
                        <div class="clearfix"></div>
                        <h3 class="">Next-of-Kin: </h3>
<!--                        <div class="col-sm-6 col-xs-12">
                            <?php //if(isset($modelSP['NextOfKinName']) && isset($modelSP['NextOfKinRelationToPatientCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next Of Kin:</dt>
                                <dd> <?php // $modelSP['NextOfKinName'].' ('.$modelSP['NextOfKinRelationToPatientCode'].')' ?> </dd>
                            </dl>
                            <?php //endif; if(isset($modelSP['AEAttendanceDate']) && isset($modelSP['NextOfKinAddrLine1'])&& isset($modelSP['NextOfKinAddrLine2']) && isset($modelSP['NextOfKinAddrLine3'])):?>
                            <dl class="dl-horizontal">
                                <dt>NoK Address:</dt>
                                <dd> <?php // $modelSP['NextOfKinAddrLine1'].', '.$modelSP['NextOfKinAddrLine2'].', '. $modelSP['NextOfKinAddrLine3']?> </dd>
                            </dl>
                            <?php //endif;?>
                        </div>-->
<!--                        <div class="col-sm-6 col-xs-12">
                            <?php //if(isset($modelSP['ChartNum'])):?>
                            <dl class="dl-horizontal">
                                <dt>Nok Telephones:</dt>
                                <dd> H:<?php // $modelSP['NextOfKinPhone'].' W:'. $modelSP['NextOfKinWorkPhone']?> </dd>
                            </dl>
                            <?php //endif;?>
                        </div>-->
                        
                        <div class="clearfix"></div>
                        <h3 class="">General Practitioner: </h3>
                        <div class="col-sm-6 col-xs-12">
                            <?php // if(isset($modelSP['GPCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>Patient of:</dt>
                                <dd> <?= $model->ReferringGPCode.' '.$model->ReferringGPName?> </dd>
                            </dl>
                            <?php // endif; if(isset($modelSP['GPPracticeAddrLine1'])):?>
                            <dl class="dl-horizontal">
                                <dt>Practice Address:</dt>
                                <dd> <?=  $model->GPPracticeAddrLine1.', '.$model->GPPracticeAddrLine2.', '. $model->GPPracticeAddrLine3.', '. $model->GPPracticeAddrLine4?> </dd>
                            </dl>
                            <?php //endif;?>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <?php // if(isset($modelSP['GPPracticePhoneNum'])):?>
                            <dl class="dl-horizontal">
                                <dt>Practice Phone:</dt>
                                <dd> <?= $model->GPPracticePhoneNum ?></dd>
                            </dl>
                            <?php //endif;?>
<!--                            <dl class="dl-horizontal">
                                <dt>Practice e-mail:</dt>
                                <dd>  </dd>
                            </dl>-->
                        </div>
                        
                        <div class="clearfix"></div>
                        <h3 class="">Last ED Attendances (Look at Therfore Navigator for full details): </h3>
                        <div class="col-sm-12 col-xs-12">
                            <?php //if(isset($modelSP['AEAttendanceDate'])){?>
                            <?php //$modelSP['AEAttendanceDate'].' as '.$modelSP['CoordinationCategoryCode'].' from '.$modelSP['PresentComplaint']
//                                    .', leaving at '.$modelSP['LogoutDate'].' or '.$modelSP['AEDischargeDate'].' or '.$modelSP['AEDischargeDate']
                                    ?> 
                            <?php //} ?>
                        </div>
                    </div>
            </div>
        </div>
    </div>
        
   
</div>
