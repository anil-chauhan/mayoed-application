<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Emergency */

//$model = json_decode($model->sp_object,true);

?> 
<script> 
    var completedViews = [];
</script>
<div class="card hospital-groups-view" id="profile-main">
    <div class="pull-right col-xs-12 col-sm-2">
        
    </div>
    <div class="clearfix"></div>
    <div class="card-body card-padding" style="color:black!important">
        <div class="pmb-block">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <div class="row">
                                        
                        <div class="col-sm-6 col-xs-12">
                            <?php if($model['dob']):?>
                            <dl class="dl-horizontal">
                                <dt>Date of Birth</dt>
                                <dd> <?= $model['dob'] ?> </dd>
                            </dl>
                            <?php endif; if($model['address']):?>
                            <dl class="dl-horizontal">
                                <dt>Address</dt>
                                <dd> <?= $model['address'] ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['NextOfKinName'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of Kin</dt>
                                <dd> <?= $model['NextOfKinName'] ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['CoordinationCategoryCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>CoordinationCategoryCode</dt>
                                <dd> <?= $model['coordination_code']; ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['NextOfKinAddrLine1'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin address 1</dt>
                                <dd> <?= $model['NextOfKinAddrLine1']; ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['NextOfKinAddrLine2'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin address 2</dt>
                                <dd> <?= $model['NextOfKinAddrLine2']; ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['NextOfKinAddrLine3'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin address 3</dt>
                                <dd> <?= $model['NextOfKinAddrLine3']; ?> </dd>
                            </dl>
							 
                            <?php endif; if(isset($model['NextOfKinAddrLine4'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin address 4</dt>
                                <dd> <?= $model['NextOfKinAddrLine4'] ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['NextOfKinPhone'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin phone</dt>
                                <dd> <?= $model['NextOfKinPhone'] ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['NextOfKinWorkPhone'])):?>
                            <dl class="dl-horizontal">
                                <dt>Next of kin work phone</dt>
                                <dd> <?= $model['NextOfKinWorkPhone'] ?> </dd>
                            </dl>
                            <?php endif;?>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <?php if(isset($model['ChartNum'])):?>
                            <dl class="dl-horizontal">
                                <dt>Chart Num</dt>
                                <dd> <?= $model['ChartNum'] ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['PresentComplaint'])):?>
                            <dl class="dl-horizontal">
                                <dt>Present complaint</dt>
                                <dd> <?= $model['PresentComplaint'] ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['IncidentDetailsScrnNotesLine1'])):?>
                            <dl class="dl-horizontal">
                                <dt>Incident details 1</dt>
                                <dd> <?= $model['IncidentDetailsScrnNotesLine1'] ?> </dd>
                            </dl>
                             <?php endif; if(isset($model['IncidentDetailsScrnNotesLine2'])):?>
                            <dl class="dl-horizontal">
                                <dt>Incident details 2</dt>
                                <dd> <?= $model['IncidentDetailsScrnNotesLine2'] ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['AEModeOfArrivalCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>Mode of arrival</dt>
                                <dd> <?= $model['AEModeOfArrivalCode'] ?> </dd>
                            </dl>
                             <?php endif; if(isset($model['PASCoordinatedDate'])):?>
                            <dl class="dl-horizontal">
                                <dt>Coordinated Date</dt>
                                <dd> <?= $model['PASCoordinatedDate'] ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['GPCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>GP code</dt>
                                <dd> <?= $model['GPCode'] ?> </dd>
                            </dl>
                             <?php endif; if(isset($model['ReferringGPCode'])):?>
                            <dl class="dl-horizontal">
                                <dt>Referring GP code</dt>
                                <dd> <?= $model['ReferringGPCode'] ?> </dd>
                            </dl>
                            <?php endif; if(isset($model['ReferringGPName'])):?>
                            <dl class="dl-horizontal">
                                <dt>Referring GP name</dt>
                                <dd> <?= $model['ReferringGPName'] ?> </dd>
                            </dl>
                            <?php endif; if($model['GPPracticeAddrLine1']):?>
                            <dl class="dl-horizontal">
                                <dt>Referring GP add 1</dt>
                                <dd> <?= $model['GPPracticeAddrLine1'] ?> </dd>
                            </dl>
                            <?php endif; if($model['GPPracticeAddrLine2']):?>
                            <dl class="dl-horizontal">
                                <dt>Address 2</dt>
                                <dd> <?= $model['GPPracticeAddrLine2'] ?> </dd>
                            </dl>
                            <?php endif; if($model['GPPracticeAddrLine3']):?>
                            <dl class="dl-horizontal">
                                <dt>Address 3</dt>
                                <dd> <?= $model['GPPracticeAddrLine3'] ?> </dd>
                            </dl>
                            <?php endif; if($model['GPPracticeAddrLine4']):?>
                            <dl class="dl-horizontal">
                                <dt>Address 4</dt>
                                <dd> <?= $model['GPPracticeAddrLine4'] ?> </dd>
                            </dl>
                            <?php endif; if($model['GPPracticePhoneNum']):?>
                            <dl class="dl-horizontal">
                                <dt>Referring GP phone</dt>
                                <dd> <?= $model['GPPracticePhoneNum'] ?> </dd>
                            </dl>
                            <?php endif;?>
                        </div>

                    </div>
                   
                </div>
            </div>
        </div>
    </div>
   
</div>

