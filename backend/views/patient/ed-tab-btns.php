<?php
use yii\helpers\Url; 
use yii\helpers\Html;


$paramStr = '&'.$_SERVER['QUERY_STRING'];
$currentUrl = Yii::$app->urlManager->createUrl(['patient']);

?>

<div class="row m-l-20">
        <?php 
            echo Html::a(
                    'By Category',
                    [$currentUrl.'?sort=coordination_code_order'],
                    ['class' => 'create-consultant-button btn btn-primary m-b-10 btn-loading']
                );
            echo Html::a(
                    'Sort by Location',
                    [$currentUrl.'?location=true'],
                    ['class' => 'create-consultant-button btn btn-primary m-b-10 btn-loading']
                );
            echo Html::a(
                    'Sort by Time',
                    [(Yii::$app->request->getQueryParam('sort') == '-timer') ? $currentUrl.'?sort=timer' : $currentUrl.'?sort=-timer' ],
                    ['class' => 'create-consultant-button btn btn-primary m-b-10 btn-loading']
                );
            echo Html::a(
                    'By Infection',
                    [(Yii::$app->request->getQueryParam('infection_status') == 'true') ? $currentUrl.'?infection_status=false' : $currentUrl.'?infection_status=true' ],
                    ['class' => 'create-consultant-button btn btn-primary m-b-10 btn-loading']
                );
            echo Html::a(
                    'Reset Sort',
                    [$currentUrl],
                    ['class' => 'create-consultant-button btn btn-primary m-b-10 btn-loading']
                );
            echo Html::a(
                    'Patient Handover',
                    'javascript:void(0)',
                    [
                       'data-toggle' => 'modal', 
                       'class' => 'handover create-consultant-button btn btn-primary m-b-10', 
                       'data-target-color' => "bluegray",
                       'data-toggle' => "modal",
                    ]
                );
        ?>
        
    <div class="pull-right m-r-20">
        <?php
            if(isset($tab) ){
                echo Html::a('<span></span> Print View', $currentUrl.'?pdf=true&tab='.$tab.$paramStr , ['class' => 'create-consultant-button btn btn-primary m-r-25 pull-right']);
            } else {
                echo Html::a('<span></span> Print View', $currentUrl.'?pdf=true&tab=allpatient'.$paramStr , ['class' => 'create-consultant-button btn btn-primary m-r-25 pull-right']);
            }
        ?>
    </div>
</div>