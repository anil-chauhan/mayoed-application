<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Patient;

/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */
?>


<?php
$form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['patient/update-rpat']),
            'options' => [
                'data' => [
                    'on-done' => 'saved-consultant'
                ],
            ],
            'id' => 'save-rpat-location-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
        ]);
//print_r($ward_list);die;
?>
<div class="modal-body bgm-white  p-t-5">            


    <div class=" m-t-10 " id="part_1">

        <div class="clearfix"></div>
        <div class=" m-t-10 c-gray " >
            <div class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown">Setting RPAT to Active</div>
            <h4 class="m-b-20">Enter Verification Code</h4>
            
                <?php 
                    echo $form->field($model, 'Patient_id')->hiddenInput()->label(false);
                    echo $form->field($model, 'is_rpat')->hiddenInput(['value'=> 1])->label(false);
                    echo $form->field($model, 'board_number')->hiddenInput()->label(false);

                    echo $form->field($model, 'code', Yii::$app->params['htmlTemplate']['inputPassword'])
                            ->textInput(['autocomplete' => 'off', 'maxlength' => true, 
                                'onkeyup' => 'fncheckpassCommon( this.value, "' . \yii\helpers\Url::to(['accesspass/checkpass']) . '" , "checkCodeRpat")'])->label(false); ?>
            <div style='color:red;' id='checkCodeRpat_error'>Please Fill the correct code</div>
            <div class="center col-sm-12 m-b-20">
                <?= Html::submitButton('Create', ['id'=>'checkCodeRpat-btn','class' => 'btn btn-primary btn-lg pull-right', 'disabled' => 'disabled']) ?>
            </div>
        </div>
    </div> 
    <div class="clearfix"></div>
</div>
<input type='hidden' value='0' id='checkCodeRpat' />
<?php ActiveForm::end(); ?>
<div class="clearfix"></div>


