<div id="form-handover-select" class="">
    <input type="hidden" name="select" value="true" />

    <?php
foreach ($models as $key => $model) {
    
?>
    <div  class="col-sm-12 c-gray center m-b-20">
                            
        <div>
            <label class="checkbox checkbox-inline m-r-20">
                <input data-all="true" name="selectPatient[]" id="<?=$model->Patient_id?>" class="select-on-check-all-2" data-type="check2" type="checkbox" value="<?=$model->Patient_id?>" checked="checked" >
                <i class="input-helper selectPatient"></i> <?= $model->patient_name ?>
            </label>
       </div>                        
       <span id="current_error" class="m-t-20 c-red"></span>
    </div>
    
    <div class="c-red hide" id="list_error"> Please select at least one.</div>
<?php    
}
?>
</div>
<script type="text/javascript">
     $('.select-on-check-all-2').change(function() {
        var id = $(this).attr('id');
        
        if($(this).is(":checked")) {
           $('#review-'+id).removeClass('hide');
        } else {
           $('#review-'+id).addClass('hide');            
        }   
    });
</script>