<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EdDoctor;
use common\models\EdLocation;
use common\models\EdNurse;

/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php
    $form = ActiveForm::begin([
        'action' => Yii::$app->urlManager->createUrl(['patient/update-comment']),
        'options' => [
            'data' => [
                'on-done' => 'saved-consultant'
            ],
        ],
        'id' => 'save-comment-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ]);
    //print_r($ward_list);die;
    ?>
    <div class="modal-header">
            <h4 id="referral-form-heading" class="modal-title">Comment</h4>
    </div>
    <div class="modal-body bgm-white  p-t-5">            
        
        <div class="toggle m-t-20 c-gray " id="part_11" >
            
            <h4 class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown">Previous Comment</h4>
            <div id="previous_comment">   
            </div>
            <input type="hidden" id="comment-id" name="Patient[Patient_id]" value="" >
            <h4 class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown m-t-30">Add New Comment</h4>
            <div  class="col-sm-12 c-gray center ">
                <?php
                   echo $form->field($model, 'commentUpdate', Yii::$app->params['htmlTemplate']['input']);
               ?>            
            </div>

            <div class="clearfix"></div>
            <div class=" m-t-10 c-gray " >
                <h4 class="m-b-20">Enter Verification Code</h4>
                <?php echo $form->field($model, 'comment_code', Yii::$app->params['htmlTemplate']['input'])
                        ->textInput([
                            'autocomplete' => 'off',
                            'maxlength' => true,
                            'onkeyup' => 'fncheckpass( this.value, "'.\yii\helpers\Url::to(['accesspass/checkpass']).'" , "checkcode")'
                        ])
                        ->label(false);?>
                <div style='color:red;' id='fill_comment_error' class="fill_error">Please enter correct code</div>
                <div class="center col-sm-12 m-b-20">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary btn-sm pull-right accesspass-btn', 'disabled' => 'disabled']) ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        
    </div>
    <div class="clearfix"></div>
   </div>
    <div class="clearfix"></div>

    <div id="dismiss" class="modal-footer bgm-gray">
        <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    </div>
<?php ActiveForm::end(); ?>
<div class="clearfix"></div>



