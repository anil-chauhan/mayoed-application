<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EdDoctor;
use common\models\EdLocation;
use common\models\EdNurse;

/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php
    $form = ActiveForm::begin([
        'action' => Yii::$app->urlManager->createUrl(['consultant/create?admission=true']),
        'options' => [
            'data' => [
                'on-done' => 'saved-consultant'
            ],
        ],
        'id' => 'save-consultant-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ]);
    //print_r($ward_list);die;
    ?>
    <div class="modal-header">
            <h4 id="admission-form-heading" class="modal-title">Awaiting Triage</h4>
    </div>
    <div class="modal-body bgm-white  p-t-5">            
            

    <div class="toggle m-t-10 stage stage-first" id="part_1">
        <div class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown">Identification</div>
        <div class ="p-l-0">
                <input type="hidden" name="popup-description" id="popup-desc">
                <input type="hidden" id="popup-x_cord" name="popup-x_cord" value="" >
                <input type="hidden" id="popup-y_cord" name="popup-y_cord" value="" >
                <input type="hidden" id="consultant-sp_object" name="Consultant[sp_object]" value="" >
                <?= $form->field($model, 'board_number', Yii::$app->params['htmlTemplate']['hiddenInput'])->hiddenInput()->label(false); ?>
            <div class="row">
                <div  class="col-sm-4 c-gray center ">
                    <?= $form->field($model, 'patient_name', Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true]) ?>
                </div>

                <div  class="col-sm-4 c-gray center ">

                    <?= $form->field($model, 'dob',  Yii::$app->params['htmlTemplate']['pdob'])->textInput(['maxlength' => true]); ?>
                </div>

                <div  class="col-sm-4 c-gray center ">
                   <?= $form->field($model, 'age',  Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true]); ?>
                </div>
            </div>
            <div class="row m-t-20">
                <div  class="col-sm-6 c-gray center ">

                    <?= $form->field($model, 'address',  Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true]); ?>
                </div>
                <div  class="col-sm-6 c-gray center">

                    <?= $form->field($model, 'Tracker_admission_ed_time',  Yii::$app->params['htmlTemplate']['pdob'])->textInput(['maxlength' => true]); ?>
                </div>
            </div>
            <div class="row m-t-20">
                <div class="col-sm-6">
                    <!--<span class="m-t-5 c-gray">Time</span>-->
                    <?php
                        echo $form->field($model, 'ed_stream', Yii::$app->params['htmlTemplate']['select'])
                            ->dropDownList(Yii::$app->params['edStream'], ['prompt' => 'None', 'style' => 'margin-top:18px;']);
                    ?>
                </div>
                <div  class="col-sm-6 c-gray center ">
                    <?php
    //                                'filter' => Html::activeDropDownList($searchModel, 'user_id', ArrayHelper::map(User::getAllUserList(), 'id', 'name'), ['class'=>'form-control','prompt' => 'Select Staff']),

                        echo $form->field($model, 'ed_location', Yii::$app->params['htmlTemplate']['select'])
                            ->dropDownList(ArrayHelper::map(EdLocation::find()->asArray()->all(), 'id', 'name'), ['prompt' => 'None', 'style' => 'margin-top:18px;']);
                    ?>            
                </div>
            </div>
            <div class="row m-t-20">
                <div  class="col-sm-6 c-gray center ">
                    <?php
                        echo $form->field($model, 'ed_doctor', Yii::$app->params['htmlTemplate']['select'])
                            ->dropDownList(ArrayHelper::map(EdDoctor::find()->asArray()->all(), 'id', 'name'), ['prompt' => 'None', 'style' => 'margin-top:18px;']);
                    ?>            
                </div>
                <div  class="col-sm-6 c-gray center ">
                    <?php
                        echo $form->field($model, 'ed_nurse', Yii::$app->params['htmlTemplate']['select'])
                            ->dropDownList(ArrayHelper::map(EdNurse::find()->asArray()->all(), 'id', 'name'), ['prompt' => 'None', 'style' => 'margin-top:18px;']);
                    ?>            
                </div>
            </div>
            <div class="row m-b-20">
                <!--<span class="m-t-5 c-gray">Time</span>-->
                <?php
                    echo Html::a('<span class="glyphicon glyphicon-map-marker"></span> Location', '#a', ['data-toggle' => 'modal', 'class' => 'mapClass-popup', 'data-target-color' => "bluegray", 'data-toggle' => "modal", "id" => "map-index"]);
                    ?>
            </div>
            
            <div class="center col-sm-12 m-b-20 m-t-10">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg pull-right' : 'btn btn-primary btn-lg pull-right']) ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div> 
    
    <div class="clearfix"></div>
    <div class="toggle m-t-10 hide c-gray " id="part_3" >
        <div >
            <h3 class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown">Preview</h3>
            <?php if(isset($model->board_number)):?>
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>Board Number</strong>
                </div>
                <div class="col-sm-7" id="board_number">
                    <?php echo $model->board_number; ?>
                </div>
            </div>
            <?php endif;?>
            
            
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>Patient Name</strong>
                    
                </div>
                <div class="col-sm-7" id="patient_name">

                </div>
            </div>
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>Date of Birth</strong>
                </div>
                <div class="col-sm-7" id="dob">

                </div>
            </div>
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>Age</strong>
                </div>
                <div class="col-sm-7" id="age">

                </div>
            </div>
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>Address</strong>
                </div>
                <div class="col-sm-7" id="address">

                </div>
            </div>
            
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>Date of Admission</strong>
                </div>
                <div class="col-sm-7" id="doa">

                </div>
            </div>
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>ED Stream</strong>
                </div>
                <div class="col-sm-7" id="ed_stream">

                </div>
            </div>
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>ED Location</strong>
                </div>
                <div class="col-sm-7" id="ed_location">

                </div>
            </div>
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>ED Doctor</strong>
                </div>
                <div class="col-sm-7" id="ed_doctor">

                </div>
            </div>
            <div class="row m-t-10">
                <div class="col-sm-4 col-sm-offset-1">
                    <strong>ED Nurse</strong>
                </div>
                <div class="col-sm-7" id="ed_nurse">

                </div>
            </div>
        

            <div class="center col-sm-12 m-t-10">

                <a class="btn btn-lg bgm-red pull-left" data-show_id="part_2" onclick="customValidator(this)">Prev</a>
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg pull-right' : 'btn btn-primary btn-lg pull-right']) ?>
        
            </div>
        <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
   </div>
    <div class="clearfix"></div>

    <div id="dismiss" class="modal-footer bgm-gray">
        <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    </div>
<?php ActiveForm::end(); ?>
<div class="clearfix"></div>
<?php $searchUrl = Yii::$app->urlManager->createUrl(['consultant/consultant-search']); ?>

<?php $this->registerJs("initSelect2('#consultant-cons_name_current','$searchUrl');", \yii\web\View::POS_READY); ?>




