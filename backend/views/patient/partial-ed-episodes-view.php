<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div class="row m-b-20 p-l-20 p-r-20" id="episode-list">
    <h2>Episode List  <small>(Total Count : <?= (count($episodes)) ? count($episodes) : 0 ?>)</small></h2>
        <div id="episode_list">
            <div class="row c-gray ">
                <table class="table table-borderd focused">
                    <tr>
                       
                        <th>
                            Episode Number#
                        </th>
                        <th>
                            Hospital Code
                        </th>

                        <th>
                            Clinic Speciality Code
                        </th>
                        <th>
                            Clinic
                        </th>
                        <th>
                            Outpatient Registration Date
                        </th>
                        <th>
                            Cons Specialty
                        </th>
                    </tr>
                    <?php if(!count($episodes)){?>
                        <tr>
                            <td colspan="3">
                                Records are not available.
                            </td>
                        </tr>
                    <?php } else{?>
                    <?php foreach($episodes as $episode):?>
                        <tr class="focused-row" data-enum="<?php echo $episode['EpisodeNum'];?>" data-edate="" data-ecode="<?php echo $episode['ConsCode'];?>">
                            
                            
                            <td>
                                <?php echo $episode['EpisodeNum'];?>
                            </td>
                            <td>
                                <?php echo isset($episode['HospCode']) ? $episode['HospCode']: '';?>
                            </td>

                            <td>
                                <?php echo isset($episode['ClinicSpecCode']) ? $episode['ClinicSpecCode']: '';?>
                            </td>
                            <td>
                                <?php echo isset($episode['Clinic']) ? $episode['Clinic']: '';?>
                            </td>
                            <td>
                                <?php echo isset($episode['OutPatientRegistrationDate']) ? $episode['OutPatientRegistrationDate']: '';?>
                            </td>
                            <td>
                                <?php echo $episode['ConsSpecialty'];?>
                            </td>
                        </tr>
                    <?php endforeach;?>
                    <?php } ?>
                </table>
            </div>
            <?php if(count($episodes)):?>
            <span id="episode_number_error" class="m-t-10 c-red"></span>

            <?php endif;?>
        </div>
    </div>