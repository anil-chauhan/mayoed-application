<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php
    $form = ActiveForm::begin([
        'action' => Yii::$app->urlManager->createUrl(['patient-referral/create?referral=true']),
        'options' => [
            'data' => [
                'on-done' => 'saved-consultant'
            ],
        ],
        'id' => 'save-referral-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ]);
    //print_r($ward_list);die;
    ?>
    
    <div class="modal-header">
            <h4 id="referral-form-heading" class="modal-title">Referral </h4>
    </div>
    <div class="modal-body bgm-white  p-t-5">            
            

        <div class="m-t-10" id="part_21">
            <div class="hide">
                <?= $form->field($model, 'episode_number')->hiddenInput()->label(false); ?>
                <?= $form->field($model, 'patient_id')->hiddenInput()->label(false); ?>
            </div>
            <div class="col-sm-12 p-0 c-black h3 m-0 p-b-30 c-brown">Identification</div>
            <div class ="p-l-0">
                <div  class="col-sm-4 c-gray center ">
                    <?= $form->field($model, 'board_number', Yii::$app->params['htmlTemplate']['input'])->textInput(['readOnly'=>true]); ?>
                </div>
                <div  class="col-sm-4 c-gray center ">
                    <?= $form->field($model, 'patient_name', Yii::$app->params['htmlTemplate']['input'])->textInput(['readOnly' => true]) ?>
                </div>

                <div  class="col-sm-4 c-gray center ">
                   <?= $form->field($model, 'age',  Yii::$app->params['htmlTemplate']['input'])->textInput(['readOnly' => true,'disabled'=>'disabled']); ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div> 
        <div class="m-t-10" id="part_22">
            <div class="col-sm-12 p-0  c-black h3 m-0 p-b-30 c-brown">Referral Type</div>
             <div  class="col-sm-12 c-gray center ">
                <?= $form->field($model, 'referral_type', Yii::$app->params['htmlTemplate']['select'])
                    ->dropDownList(Yii::$app->params['referralType'], ['prompt' => '', 'style' => 'margin-top:18px;']);
                ?>
            </div> 

            <div class="clearfix"></div>
        </div>
        <div class="toggle m-t-10" id="part_23" >
            <div class="col-sm-12 p-0 c-black h3 m-0 p-b-30 c-brown">Comment</div>
            <div  class="col-sm-12 c-gray center ">
                <?= $form->field($model, 'comment', Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true]) ?>
            </div>
            
            <div class="clearfix" ></div>
            <div class=" m-t-10 c-gray " >
                <h4 class="m-b-20">Enter Verification Code</h4>
                <?php echo $form->field($model, 'code', Yii::$app->params['htmlTemplate']['inputPassword'])->passwordInput([ 'autocomplete' => 'off', 'maxlength' => true, 'onkeyup'=>'fncheckPass(this.value)'])->label(false);?>
                <div style='display:none;color:red;' id='fill_referral_error'>Please enter correct code</div>
                
                <div class="clearfix"></div>
            </div>
            
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-sm pull-right' : 'btn btn-primary btn-lg pull-right']) ?>
            
            
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>

    </div>
    <div class="clearfix"></div>

    <div id="dismiss" class="modal-footer ">
        <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    </div>
<?php ActiveForm::end(); ?>
<div class="clearfix"></div>
<?php $searchUrl = Yii::$app->urlManager->createUrl(['consultant/consultant-search']); ?>

<?php $this->registerJs(
        "initSelect2('#consultant-cons_name_current','$searchUrl','Search for a Consultant');", \yii\web\View::POS_READY
); ?>
<script>
    $('#add-readonly').on('click',function(){
        if(!$('#patientreferral-referral_type option:selected').val()){
            $('.field-patientreferral-referral_type').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
        } else {
            showId = $(this).data('show_id');
            $('.field-patientreferral-referral_type').removeClass('has-error').find('.help-block').html('');
            $('.toggle').addClass('hide');
            
            $('#'+showId).removeClass('hide');
            
        }
    });
    
    $('#save-referral-form').on('submit', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();
        $('#fill_referral_error').hide();
        flag = true;
        if ($('#checkcode').val() == '1') {
            $('#fill_referral_error').hide();
        } else {
            flag = false;
            $('#fill_referral_error').show();
        }


        if (!$('#patientreferral-referral_type').val()) {
            $('.field-patientreferral-referral_type').addClass('has-error').find('.help-block').html('Field cannot be blank.');
            flag = false;
        } else {
            $('.field-patientreferral-referral_type').removeClass('has-error').find('.help-block').html('');
        }
        if (flag) {
            $("#patientreferral-referral_type").prop('disabled', false);
            $('#loader').show();
            submitFormHandle(e);   
        }
    });
</script>

