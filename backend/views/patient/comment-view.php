<?php 
use common\models\Emergency;
use common\models\Comments;
use common\models\Logs;
use common\models\CommentsSearch;
?>

<table class="table table-striped bootgrid-table" aria-busy="false">
               
    <tbody>
        <?php
        $flag = false;
        if(!empty($model->logs)){
            foreach($model->logs as $key => $value):
                $replace_old;
                $replace_new;
                $user;
                $replace_type;
                $logMsg = '';
                $locationFlag = false;
                $alterUser =$value->alterUser['firstname'].' '.$value->alterUser['lastname'];

                if($value->log_type == Logs::getType('Comment')){
                ?>
                    <tr data-row-id="<?php echo $key; ?>">
                        <td class="text-left">

                <?php 
                    $replace_old = $value->replace_old;
                    $replace_new = $value->replace_new;
                    $msg = '<b>'.$replace_new.'</b> at ' . date('H:i, dS F Y',$value->created_at). ' - commented by '.$alterUser;
                    echo $msg;
                    $flag = true;
                ?>
                        </td>

                    </tr>
                <?php
                } 

            endforeach;
        }
            if(!$flag){
                ?>
                <tr >
                    <td class="text-left">
                        No Data found.
                    </td>

                </tr>
                <?php 
            }
            ?>

    </tbody>
</table>