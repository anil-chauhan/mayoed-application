<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\widgets\MyTabs; 

/* @var $this yii\web\View */
/* @var $searchModel common\models\PatientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ED Patient List';
$this->params['breadcrumbs'][] = $this->title;
$params = Yii::$app->getRequest()->getQueryParams();
$tab = isset($params['tab']) ? $params['tab'] : '';
$line_color = isset(Yii::$app->params['edStreamColor'][$tab]) ? Yii::$app->params['edStreamColor'][$tab] : '#f123w';

echo $this->render('../partials/partial-ed_stats', [
    'data' => $data,
]);

?>

<style>
    .tab-nav:not([data-tab-color]) > li > a:after {
        background: <?= $line_color; ?>;
    }
    .mini-charts-item{
        height: 80px;
    }
    
    @media (min-width: 1500px){
        .modal-elg {
            width: 1300px;
        }
    }
</style>

<div class="card" style="padding-bottom: 150px;">
    <?php
        $tab = Yii::$app->getRequest()->getQueryParam('tab');
        
        echo MyTabs::widget([
            'items' => [
                [
                    'label' => 'All Patients',
                    'content' => (!$tab || $tab == 'allpatient') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'allpatient',
    //                            'color'  => $color,
                                'tab' => $tab, 
                                'episodeData' => $episodeData,
                            ]) : (($tab) ? '' : $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'allpatient',
    //                            'color'  => $color,
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ])),
                    'headerOptions' => [],
                    'active' => $tab && $tab == 'allpatient' ? true : (($tab) ? false : true),
                    'linkOptions' => [
                        'id' => 'allpatient',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'Resus/Major',
                    'content' => ($tab == 'resus') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'resus', 
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '',
                    'headerOptions' => [],
                    'active' => $tab && $tab == 'resus' ? true : false,
                    'linkOptions' => [
                        'id' => 'resus',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'RAN/RAT',
                    'content' => ($tab == 'triage-assist') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'triage-assist',
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '',
                    'headerOptions' => [],
                    'active' => $tab && $tab == 'triage-assist' ? true : false,
                    'linkOptions' => [
                        'id' => 'triage-assist',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'Ambulatory Care',
                    'content' => ($tab == 'yellow') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'yellow',
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '',
                    'headerOptions' => [],
                    'active' => $tab && $tab == 'yellow' ? true : false,
                    'linkOptions' => [
                        'id' => 'yellow',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'Minor Injury',
                    'content' => ($tab == 'minor') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'minor',
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '',
                    'headerOptions' => [],
                    'active' => $tab && $tab == 'minor' ? true : false,
                    'linkOptions' => [ 
                        'id' => 'minor',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'Paeds ED',
                    'content' => ($tab == 'paediatrics') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'paediatrics',
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '',
                    'headerOptions' => [],
                    'active' => $tab && $tab == 'paediatrics' ? true : false,
                    'linkOptions' => [
                        'id' => 'paediatrics',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'Patients Under 16',
                    'content' => ($tab == 'patients_16') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'patients_16',
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '', 
                    'headerOptions' => ['class' => 'hide'],
                    'active' => $tab && $tab == 'patients_16' ? true : false,
                    'linkOptions' => [
                        'id' => 'patients_16',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'Awaiting Discharge',
                    'content' => ($tab == 'discharge') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'patients_16',
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '',
                    'headerOptions' => ['class' => 'hide'],
                    'active' => $tab && $tab == 'discharge' ? true : false,
                    'linkOptions' => [
                        'id' => 'discharge',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'Referrals',
                    'content' => ($tab == 'awaiting') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'awaiting',
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '',
                    'headerOptions' => [],
                    'active' => $tab && $tab == 'awaiting' ? true : false,
                    'linkOptions' => [
                        'id' => 'awaiting',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'PAB',
                    'content' => ($tab == 'awaiting_admission') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'awaiting_admission',
    //                            'color'  => $color,
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '',
                    'headerOptions' => ['class' => 'hide'],
                    'active' => $tab && $tab == 'awaiting_admission' ? true : false,
                    'linkOptions' => [ 
                        'id' => 'awaiting_admission',
                        'class' => 'tabs btn-loading'
                    ]
                ],
                [
                    'label' => 'To Be Seen',
                    'content' => ($tab == 'to_be_seen') ? $this->render('partial-ed-tab', [
                                'dataProvider' => $dataProvider,
                                'searchModel' => $searchModel,
                                'id_value' => 'to_be_seen',
    //                            'color'  => $color,
                                'tab' => $tab,
                                'episodeData' => $episodeData,
                            ]) : '',
                    'headerOptions' => [],
                    'active' => $tab && $tab == 'to_be_seen' ? true : false,
                    'linkOptions' => [
                        'id' => 'to_be_seen',
                        'class' => 'tabs btn-loading'
                    ]
                ]
            ],
            'options' => [
                'id' => 'stroke-tab',
                'class' => 'tab-nav tn-justified'
            ]
        ]);
    ?>
</div>

<div class="modal fade" data-modal-color="bluegray" id="replace-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-lg">
        <div id="replaceData" class="modal-content">
            <?= $this->render('../forms/replace-form', ['model' => $patient]) ?>

        </div>
    </div>
</div>
<div class="modal fade" data-modal-color="bluegray" id="handover-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="handoverData" class="modal-content">
            <?= $this->render('handover-form', ['model' => $patient]) ?>

        </div>
    </div>
</div>
<div class="modal fade" data-modal-color="bluegray" id="pincode-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="pincodeData" class="modal-content">
            
            <div class="modal-header">
                <h4 id="referral-form-heading" class="modal-title">Handover Form</h4>
            </div>
            <div class="modal-body bgm-white  p-t-5">    
                <?= Html::beginForm(
                        Yii::$app->urlManager->createUrl(['accesspass/get-pincode']),
                        'post',
                        [
                            'id' => 'get-pincode-form',
                            'data' => [
                                'on-done' => 'saved-consultant'
                            ],
                        ]
                    );?>
                    <div class="row c-gray m-t-20 center ">
                        <div class="form-group fg-float field-user-list has-success">
                            <div class="fg-line fg-toggled">
                                <div class="select">
                                    <?php echo Html::dropDownList('users', 'null', ArrayHelper::map($userList, 'id', 'name'),['id' => 'user-list', 'class' => "input-small form-control"]);?>

                                </div>
                            </div>
                            <label class="fg-label" for="user-list">Ed Location</label>

                            <div class="help-block"></div>
                        </div>            
                    </div>
                    <div class="clearfix"></div>
                    <?php
                        echo Html::submitButton('Send Pincode',['class' => 'btn btn-primary pull-right m-10']);
                    ?>
                    <div class="clearfix"></div>
                     <?=  Html::endForm(); ?>
            </div>

            <div id="dismiss" class="modal-footer bgm-gray">
                <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" data-modal-color="bluegray" id="log-view" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div class="modal-header bgm-gray">
            <h4 id="" class="modal-title">Patient Log View</h4>
        </div>
        <div id="logData" class="modal-body bgm-white p-t-10">

        </div>
        <div id="dismiss" class="modal-footer bgm-gray">
            <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        </div>

    </div>
</div>

<!-- Episodes modal -->
<div class="modal fade" data-modal-color="bluegray" id="episodes-view" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog  modal-lg">
        <div class="modal-header bgm-gray">
            <h4 id="" class="modal-title">Episodes View</h4>
        </div>
        <div id="episodesData" class="modal-body bgm-white p-t-10">

        </div>
        <div id="dismiss" class="modal-footer bgm-gray">
            <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        </div>

    </div>
</div>

<!-- RPAT modal -->
<div class="modal fade" data-modal-color="bluegray" id="rpat-view" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog  modal-md">
        <div class="modal-header bgm-gray">
            <h4 id="" class="modal-title"><?= date("d-m-Y h:i A") ?>: <span id="rpat-patient"></span></h4>
        </div>
        <div id="rpatData" class="modal-body bgm-white p-t-10">

        </div>
        <div id="dismiss" class="modal-footer bgm-gray">
            <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        </div>

    </div>
</div>

<div class="modal fade" data-modal-color="bluegray" id="dropdown-view" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-lg">
        <div class="modal-header row bgm-gray">
            <div id="" class=" col-sm-6 modal-title h4">Patient View</div>
            <div class="col-sm-6">
                <button type="button" class="btn btn-link pull-right c-white" data-dismiss="modal"><span class="md-close md-2x"></span></button>
            </div>
        </div>
        <div id="dropdownData" class="modal-body bgm-white p-t-10">

        </div>
        <div id="dismiss" class="modal-footer bgm-gray">
            <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
        </div>

    </div>
</div>
<div class="modal fade" data-modal-color="bluegray" id="comment-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="replaceData" class="modal-content">
            <?= $this->render('comment-form', ['model' => $patient]) ?>

        </div>
    </div>
</div>
<div class="modal fade" data-modal-color="bluegray" id="discharge-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="dischargeData" class="modal-content">
            <?= $this->render('../forms/discharge-form', ['model' => $patient]) ?>

        </div>
    </div>
</div>
<div class="modal fade" data-modal-color="bluegray" id="admission-model" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="formData" class="modal-content">
            <?= $this->render('admission-form', ['model' => $patient]) ?>

        </div>
    </div>
</div>
<div class="modal fade" data-modal-color="bluegray" id="referrals-model" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div id="formData" class="modal-content">
            <?php echo $this->render('referrals-form', ['model' => $referral]) ?>

        </div>
    </div>
</div>
<div class="modal fade scroll-bar" data-modal-color="bluegray" id="alertError" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div id="alertPatient" class="modal-body bgm-white p-t-10" style="color: black;line-height:1;">
            <div class="danger">
                <h4 id="discharge-msga"></h4>
            </div></br></br >
            <a class='btn btn-sm bgm-red waves-effect waves-button waves-float' id="deleteRecord" href="" >Yes</a>
            <button type="submit" class="pull-right btn btn-primary waves-effect waves-button waves-float waves-effect waves-button waves-float" data-dismiss="modal">No</button>
            <!--<button  type="button" class="btn btn-link" >Close</button>-->
        </div>

    </div>
</div> 

<div class="modal fade " data-modal-color="bluegray" id="select-field" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-elg">
        <div id="" class="modal-content">
            
            <?= $this->render('../forms/select-field', ['model' => $patient]) ?>

        </div>
    </div>
</div>

<?php $this->registerJs("$('#content').on('click', '.tabs', sum);", \yii\web\View::POS_READY); ?>
<?php $this->registerJS("  
    $(document).ready(function(){

        $(document).on('click', '.floor_plan_image', function(e) {
            $('#x_cord').val('');
            $('#y_cord').val('');
            $('#board_number').val('');
            $('#desc').val('');
            var offset = $(this).offset();
            board_number = String($(this).data('board_number'));

            $('#x_cord').val(e.pageX - offset.left);
            $('#y_cord').val(e.pageY - offset.top);
            $('#board_number').val(board_number);
            $('#myModal').modal({
                show: 'true'
            });

        });

   });  "); ?>
<?php $this->registerJsFile(Yii::$app->request->baseUrl . '/www/js/form.js', ['depends' => [\backend\assets\AppAsset::className()]]); ?>

<?php
$this->registerJs("
    $('.second').each(function(e){
        var id = $(this).attr('id');
        var second = $(this).data('to_diff');
        if(second) {
          var d = new Date();
          d.setSeconds(d.getSeconds() - second);
          $('#'+id).tinyTimer({ from: d, format: '%0H{ : } %0m' }); 
        }

    });
    $('.replaceClass').click(function(){
        var element = $(this);
        name = element.data('name');
        boardNumber   = element.data('boardnumber');
        episodeNumber = element.data('episodenumber');
        dob = element.data('dob');
        address = element.data('address');
        age = element.data('age');
        id = element.data('id');
        edStream = element.data('ed_stream');
        edLocation = element.data('ed_location');
        edDoctor = element.data('ed_doctor');
        edNurse = element.data('ed_nurse');
     
        replaceType = element.data('type');
        heading = replaceType.charAt(0).toUpperCase() + replaceType.slice(1);
        $('#referral-form-heading').html(heading);
        
        $('.dischargeClass').data('boardnumber', boardNumber)
        $('.dischargeClass').data('id',id);
        $('#dropdownAnchor').data('id',id);
        $('.dischargeClass').data('name',name);
        $('.dischargeClass').data('age',age);
         
        $('#dietetianAnchor').data('boardnumber', boardNumber)
        $('#dietetianAnchor').data('episodeNumber', episodeNumber)
        $('#dietetianAnchor').data('name', name)


        admission_date = element.data('admission_date');
        $('#patient-patient_name').val(name);
        $('#patient-dob').val(dob);
        $('#patient-age').val(age);
        $('#patient-address').val(address);
        $('#patient-admission_date').val(admission_date);
        $('#patient-board_number').val(boardNumber);
        $('#replace-id').val(id);
        $('#patient-ed_location').val(edLocation);
        $('#patient-ed_stream').val(edStream);
        $('#patient-ed_doctor').val(edDoctor);
        $('#patient-ed_nurse').val(edNurse);
        $('.toggle').addClass('hide');
        $('#part_1').removeClass('hide');
        focusInputs();
        $('#replace-form').modal({
            show: 'true',
            keyboard: true
        });
        
    });
    

  $('.dietetianClass').click(function(e){
        $('#nursereferral-board_number').val($('#dietetianAnchor').data('boardnumber'));
        $('#nursereferral-episode_number').val($('#dietetianAnchor').data('episodeNumber'));
        $('#nursereferral-patient_name').val($('#dietetianAnchor').data('name'));
                       
        $('#loader').hide();
        $('#dietetian-form').modal({
              show: 'true',
              keyboard: true 
         }); 

   });
   
    $('.handover').click(function(){
       
        var element = $(this);
        name = element.data('name');
        $('#handover-form').modal({
            show: 'true',
            keyboard: true
        });
        
    });
    $('div.handover-nurse #patient-ed_nurse_from').change(function(){
        var id = $('div.handover-nurse #patient-ed_nurse_from option:selected').val();
        var url = '" . Yii::$app->urlManager->createUrl(['patient/get-nurse-patient?id=']) . "' + id;
        $('#handover-list').html('');
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            success: function(data) {
                if(data.success) {
                    $('#handover-list').html(data.patientList);
                    $('#review-list').html(data.patientReview);
                    $('#review-list').append('<div id=" . 'a' . "></div>');
                } 
            }
        });
               
    });    


    $('.logClass').click(function(){
        id = $(this).data('id');
        var url = '" . Yii::$app->urlManager->createUrl(['patient/get-log?id=']) . "' + id;
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            success: function(data) {
                if(data.success) {
                    $('#logData').html(data.logView);
                    $('#log-view').modal({
                        show: 'true',
                        keyboard: true
                    });
                } 
            }
        });
    });
    
    

//    $('.episodesClass').click(function(){
//        board_number = $(this).data('board_number');
//        var url = '" . Yii::$app->urlManager->createUrl(['patient/get-episodes-data?id=']) . "' + board_number;
//        $.ajax({
//            type: 'post',
//            dataType: 'json',
//            url: url,
//            success: function(data) {
//                if(data.success) {
//                    $('#episodesData').html(data.episodesView);
//                    $('#episodes-view').modal({
//                        show: 'true',
//                        keyboard: true
//                    });
//                    $('#loader').hide();
//                } 
//            }
//        });
//    });
    

    $('.rpatClass').click(function(){
        board_number = $(this).data('board_number');
        patient_name = $(this).data('patient_name');
        var id = $(this).data('id');
        var url = '" . Yii::$app->urlManager->createUrl(['patient/rpat?id=']) . "'+id ;
            
        $('#rpat-patient').html(patient_name+' ('+ board_number+' )');

        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            success: function(data) {
                if(data.success) {
                    $('#rpatData').html(data.rpatView);
                    $('#rpat-view').modal({
                        show: 'true',
                        keyboard: true
                    });
                    $('#loader').hide();
                } 
            }
        });
    });


    $('.dropdownClass').click(function(){
        id = $(this).data('id');
        var url = '" . Yii::$app->urlManager->createUrl(['patient/get-dropdowndata?id=']) . "' + id;
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            success: function(data) {
                if(data.success) {
                    $('#dropdownData').html(data.dropdownData);
                    $('#loader').hide();
                    $('#dropdown-view').modal({
                        show: 'true',
                        keyboard: true
                    });
                } 
            }
        });
        
        
    });
    $('.commentClass').click(function(){
       
        var element = $(this);
        $('#part_11').removeClass('hide');
        id = element.data('id');
        model = element.data('model');
        $('#previous_comment').html('');
        number = 1;
        var url = '" . Yii::$app->urlManager->createUrl(['patient/comment-log?id=']) . "' + id;
        $('#comment-id').val(id);
        $('#comment-form').modal({
            show: 'true'
        });
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            success: function(data) {
                if(data.success) {
                       $('#previous_comment').append(data.list);
                } 
            }
         });
    });
    $('.action-referral').click(function(e){
        data = $(this).closest('td').closest('tr');
        $('#patientreferral-patient_id').val(data.data('id'));
        $('#patientreferral-patient_name').val(data.data('patient_name'));
        $('#patientreferral-dob').val(data.data('dob'));
        $('#patientreferral-age').val(data.data('age'));
        $('#patientreferral-address').val(data.data('address'));
        $('#patientreferral-admission_date').val(data.data('admission_date'));
        $('#patientreferral-board_number').val(data.data('board_number'));
        $('#patientreferral-episode_number').val(data.data('episode_number'));
        focusInputs();
        $('#referrals-model').modal({
                show: 'true'
            });
    });
    

    ");
?>  
<script>
    
    $(document).ready(function () {
        
        $('.pincode').click(function(){
            var element = $(this);
            $('#pincode-form').modal({
                show: 'true',
                keyboard: true
            });

        });
        
        $('#get-pincode-form').on('submit',function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            submitFormHandle(e);   
        });    
        

        $('.kv-expand-header-cell').removeClass();
        $('.kv-expand-header-icon').addClass('hide');

        // Screen reloading function
        var timeout = 60000;
        var idleTimeout = 59000;
        
        <?php
            if(Yii::$app->user->identity->email == "edscreen@hse.ie"){
        ?> 
        
         timeout = 15000;
         idleTimeout = 15000;
        
        <?php 
            }
        ?>
        
        setTimeout(function () {
            loadHandler();
        }, timeout);

        function loadHandler() {console.log(timeout,idleTimeout);
            if ($('.modal').is(':visible')) {
                setTimeout(function () {
                    loadHandler();
                }, 5000);
            } else {console.log("In else");
                $(document).idle({
                    onIdle: function () {console.log("onIdle");
                        $('#loader').show();
                        
                        <?php 
                            if(Yii::$app->user->identity->email == "edscreen@hse.ie"){
                        ?>
                        window.location.href = "<?= Yii::$app->urlManager->createUrl(['/site/dashboard-index']) ?>";
                        
                        <?php } else { ?>
                            window.location.reload(1);
                        <?php } ?>
                    },
                    onActive: function () {console.log("onActive");
                        setTimeout(function () {
                            loadHandler();
                        }, timeout);
                    },
                    idle: idleTimeout
                });
            }
        }
        $('.readonly-btn').click(function () {
            var id = $(this).attr('id');
            console.log(id);
            if (id == "add-readonly") {
                if (!$('#patientreferral-referral_type option:selected').val()) {
                    $('.field-patientreferral-referral_type').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                    flag = false;
                } else {
                    $('.field-patientreferral-referral_type').removeClass('has-error').find('.help-block').html('');
                    $('#referral_type').html($('#patientreferral-referral_type option:selected').text());
                    $('#add-readonly').addClass('hide');
                    $("#patientreferral-referral_type").prop('disabled', 'disabled');

                    $('.toggle').addClass('hide');

                    $('#part_23').removeClass('hide');
                }
            } else if (id == "remove-readonly") {
                $("#patientreferral-referral_type").prop('disabled', false);
                $('#add-readonly').removeClass('hide');
            }
        });

    });
    function showAlert(element) {
        var $deleteUrl = $(element).data('delete');
        var $discharge = $(element).data('discharge');
        var $id = $(element).data('id');
        var name = $(element).data('name');
        var boardNumber = $(element).data('boardnumber');
        var age = $(element).data('age');
        
        $('#deleteRecord').attr('href', $deleteUrl);
        $('#discharge_id').val($id);

        $('#discharge-patient_name').val(name);
        $('#discharge-board_number').val(boardNumber);
        $('#discharge-age').val(age);
        focusInputs();

        if ($discharge) {
            $('#discharge_type').val($discharge);
        }

        $('#discharge-form').modal({
            show: 'true'
        });
        if ($discharge == '1') {
//            $('#alertError').data('value')
            $('#discharge-msg').html('Do you wish to cancel the discharge of this patient?');
        } else {
            $('#discharge-msg').html('Are you sure you want to mark this patient as discharged?');
        }
    }


    $('#save-replace-form').on('submit', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        $('#fill_error').hide();
        currentElement = e;
        var showId = $(e).data('show_id');
        var requiredFiels = $(e).data('required');
        flag = true;
        if ($('#checkcode').val() == '1') {

            $('#fill_error').hide();
        } else {
            flag = false;
            $('#fill_error').show();
        }
        if (flag) {
            
            $('#loader').show();
            $('#patient-ed_doctor').attr("disabled", false);             
            $('#patient-ed_location').attr("disabled", false);             
            $('#patient-ed_nurse').attr("disabled", false);             
            $('#patient-referral_type').attr("disabled", false);    
            submitFormHandle(e);
        }
    });
    
    $('#save-comment-form').on('submit', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        flag = true;
        if (!$('#patient-commentupdate').val()) {
            $('.field-patient-commentupdate').addClass('has-error').find('.help-block').html('Field cannot be blank.');
            flag = false;
        } else {
            $('.field-patient-commentupdate').removeClass('has-error').find('.help-block').html('');
            $('#comment').html($('#consultant-comment').val());
        }
        if ($('#checkcode').val() == '1') {

            $('#fill_comment_error').hide();
        } else {
            flag = false;
            $('#fill_comment_error').show();
        }
        if (flag) {
            $('#loader').show();
            submitFormHandle(e);
        }
    });
    $('#save-handover-form').on('submit', function (e) {

        e.preventDefault();
        e.stopImmediatePropagation();
        $('.fill_error').hide();
        flag = true;
        if ($('#checkcode-handover').val() == '1') {
            $('.fill_error').hide();
        } else {
            flag = false;
            $('.fill_error').show();
        }
        if (flag) {
            $('.loader').show();
            submitFormHandle(e);            
        }
    });
    
    function replaceValidator(element) {

    }

    function sum(e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var $link = $(e.target);
        window.location.href = "?tab=" + $link.attr('id');
    }
 
    $('#save-rpat-location-form').on('submit', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        flag = true;
        if ($('#checkCodeRpat').val() == '1') {

            $('#fill_error_rpat').hide();
            $('#createRpatBtn').prop('disabled', false);
        } else {
            flag = false;
            $('#fill_error_rpat').show();
        }
        if (flag) {
            $('#save-rpat-location-form').submit();
        }
    });
    function checkHandle(e) {
        //e.preventDefault();
        //e.stopImmediatePropagation();
        var $link = $(e.target);
        var checkedAll = ($link.data('all')) ? $link.data('all') : false;

        if (checkedAll) {
            $link.prop('checked') ? $link.prop('checked', true) : $link.prop('checked', false);
            var param = ($link.prop('checked')) ? 1 : 0;
            var url = '<?= Yii::$app->urlManager->createUrl(['patient/mutlicheckpatient?check=']) ?>' + param;
        } else {
            var url = '<?= Yii::$app->urlManager->createUrl(['patient/checkpatient?id=']) ?>' + $link.attr('value');
        }
        $.ajax({
            type: "post",
            dataType: 'json',
            url: url,
            success: function (data) {
                if (data.success) {
                    if (checkedAll) {
                        ($link.prop('checked')) ? $('.action-checkbox').prop('checked', true) : $('.action-checkbox').prop('checked', false);
                    } else {
                        $link.prop('checked') ? $link.prop('checked', true) : $link.prop('checked', false);

                    }
                    notify('Successfully Saved', 'success');
                } else {
                    
                    $('.loader').hide();
                    notify('There is some issues right now. Please try later.', 'warning');
                }
              }
        });


    }
        
        
$('.referralSelectClear').click(function(e){
   $('#patient-referral_type').val('');
   $('#select-field').modal('toggle');
});

</script>
<?php $this->registerJs("$('#content').on('click', '.action-checkbox', checkHandle);", \yii\web\View::POS_READY); ?>
<?php $this->registerJs("$('#content').on('click', '.select-on-check-all', checkHandle);", \yii\web\View::POS_READY); ?>
    