<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Review Patients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= $this->render( '/partials/list-header', ['hideCreate'=>true]); ?>
    <?= GridView::widget(array_merge([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'patient_name',
            'episode_number',  
            'board_number',
            'age',
            'sex',
            'address',
            [
            'attribute'=>'dob',
            'filter'=>false, 
            'format'=>['date'],    
            ],    
            'PhoneNumber',
            'Mobile',
            [
            'attribute'=>'ed_location',
            'filter'=>Html::activeDropDownList($searchModel, 'ed_location', ArrayHelper::map(common\models\EdLocation::find()->all(), 'id', 'name'), ['class'=>'form-control','prompt' => 'Select']),    
            'value'=>function($model){
                return $model->edLocation['name'];    
               }
            ],
            [
             'attribute'=>'ed_nurse',   
             'filter' => Html::activeDropDownList($searchModel, 'ed_nurse', ArrayHelper::map(common\models\EdNurse::getNurseList(true), 'id', 'name'), ['class'=>'form-control','prompt' => 'Select']),
              'value'=>function($model){
                return $model->edNurse['name'];    
               }
             ],  
            [
             'attribute'=>'ed_doctor',   
             'filter' => Html::activeDropDownList($searchModel, 'ed_doctor', ArrayHelper::map(common\models\EdDoctor::getDoctorList(true), 'id', 'name'), ['class'=>'form-control','prompt' => 'Select']),
              'value'=>function($model){
                return $model->edDoctor['name'];    
               }
           ],
            //['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
        ],
    ],Yii::$app->params['widgetOptions']['gridView'])); ?>

</div>

