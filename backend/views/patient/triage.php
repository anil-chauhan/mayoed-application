<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PatientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Awaiting Triage';
$this->params['breadcrumbs'][] = $this->title;

?>
<style>
    .modal-dialog {
        width : 900px !important
    }
    
    .mini-charts-item{
        height: 80px;
    }
</style> 
<?php
    echo $this->render('../partials/partial-ed_stats', [
                            'data' => $data,
                        ]);
    ?>
<div class="card" style="padding-bottom: 150px;">

    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
   
    <?= $this->render('/partials/list-header', ['hidden' => true]); ?> 
    
    <?=
    
    GridView::widget(array_merge([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export' => false,
       
        'responsive' => true,
        'striped' => true,  
        'bootstrap'=>true,
        'hover'=>true,
        'toggleData'=>true,
        'pjax' => true, // pjax is set to always true for this demo
        'pjaxSettings'=>['neverTimeout'=>true,],
        'columns' => [
            [
                 'class' => 'kartik\grid\ExpandRowColumn',
                 'value' => function ($model, $key, $index, $column) {
                     return GridView::ROW_COLLAPSED;
                 },
                 //'enableRowClick' => true,
                 'allowBatchToggle'=>true,
                 'detail'=>function ($model, $key, $index, $column) {
                        return Yii::$app->controller->renderPartial('partial_view', ['model'=> $model]);
                 },
                 'detailOptions'=>[ 'class'=> 'kv-state-enable',],
                 'expandOneOnly' => true,
                 'expandTitle' => 'Click to view detail',
                 'collapseTitle' => 'Click to hide detail',    
                 'detailAnimationDuration' => 150 ,
                 'width' =>'70px',

             ],
            ['class' => 'yii\grid\SerialColumn'],
            
            'patient_name',
            'board_number',
            [
                'label' => 'Age/Sex',
                'value' => function ($model) {
                   return $model->age.'/'.$model->sex;
                },
            ], 
                        
           [
                'header' => 'Present <br> Complaint',
                'value' => function($model){
                    return $model['PresentComplaint'];
                }
           ],
         
            
//            [
//                'attribute' => 'register_with',
//                'header' => 'Present <br> Complaint',
//            ],
               
            [
                'format' => 'raw',
                'attribute' => 'PASAEAttendanceDate', 
                'header' => 'Time Since<br>Registration',
                'value' => function ($data){
                        $modeArrival = ($data->AEModeOfArrivalCode == 'A') ? 'A' : '';
                    
                        //return '<div id="'.$data['id'].'" class="second" data-second="'.$data['Tracker_timer'].'" data-now="'. strtotime('now').'" data-to_diff="'.(strtotime("now")-$data['Tracker_timer']).'"></div>'.$modeArrival;
                        return '<div id="'.$data['Patient_id'].'" class="second" data-second="'.strtotime($data['PASAEAttendanceDate']).'" data-now="'. strtotime('now').'" data-to_diff="'.(strtotime("now")-strtotime($data['PASAEAttendanceDate'])).'"></div>'.$modeArrival;
                        
                },
                'contentOptions' => function ($model, $key, $index, $grid) {
                    $styleOpacity = '';
                    if ($model->is_discharge){
                         $styleOpacity = 'opacity :0.5!important;';
                    }
                    return [
                        'style' => $styleOpacity,
                    ];
                 },
              ],
              [
                'vAlign'=>'middle',
                 'header' => '',
                 'format' => 'raw',
                 'value' => function ($data) {
                    
//                    if(($data->is_triage)){
//                       return Html::a('Discharge', 'javascript:void(0)', [ 'class' => 'btn btn-primary btn-xs' , 'style' => 'opacity:0.5']);
//                    }
                    return Html::a('Triage Code', 'javascript:void(0)', [
                      'data-toggle' => "modal",
                      'data-patient_id' => $data->Patient_id,   
                      'style' =>'background-color: #2196f3 !important;',
                      'class' => 'addCoordCode btn btn-xs btn-primary m-t-15', 
                   ]);
                },
             ],
           ]
       ], Yii::$app->params['widgetOptions']['gridView'])) ;
    ?>
</div>  
<div class="modal fade " data-modal-color="bluegray" id="coordination-code" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog modal-elg">
        <div id="" class="modal-content">
            
            <?= $this->render('../forms/coordination-code', ['model' => $model]) ?>

        </div>
    </div>
</div>
  <?php $this->registerJS("  
   $('.second').each(function(e){
        var id = $(this).attr('id');
        var second = $(this).data('to_diff');
        if(second) {
          var d = new Date();
          d.setSeconds(d.getSeconds() - second);
          $('#'+id).tinyTimer({ from: d, format: '%0H{ : } %0m' }); 
        }
   });
"); 
?>

<?php // $this->registerJs("$('body').on('beforeSubmit', '#save-consultant-form', submitHandle(this));", \yii\web\View::POS_READY);?>

<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/www/js/form.js',['depends' => [\backend\assets\AppAsset::className()]]); ?>

<script type="text/javascript">
    $(document).ready(function(){
        
        setTimeout(function(){loadHandler();}, 300000);
        
        function loadHandler(){
            if($('.modal').is(':visible')){
                setTimeout(function(){loadHandler();}, 10000);
            } else {
                window.location.reload(1);
            }
         }
         
            $('.addCoordCode').click(function(){
                
                var element = $(this);
                var patient_id = element.data('patient_id');
                  $('#patient-patient_id').val(patient_id);
//                $('.selectBox').addClass('hide');
//                $('.'+type).removeClass('hide');
                 $('#coordination-code').modal({
                  show: true,
                  keyboard: true
                 });
             });
             
             $('.codeSelect').click(function(){
                
                var element = $(this);
                var codeId = element.data('code');
                $('#patient-coordination_code').val(codeId);
                console.log(codeId);
                //$('#select-field').modal('toggle');
       });
    });
</script> 