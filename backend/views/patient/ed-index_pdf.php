<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use common\components\ArrayHelpers;
use common\widgets\MyTabs;

$params =  Yii::$app->getRequest()->getQueryParams();
$tab = isset($params['tab']) ? $params['tab']: 'allpatient';

if($tab == 'awaiting_admission' || $tab == 'allpatient'){  
?>  
    <table style="width: 100%" >
        <tr>
            <td class="text-center" colspan="2">
                <div>Total Patients in ED: <?=$data['edPatients']?></div>
                <div>> 9 Hours: <?=$data['>9']?></div>
                <div>> 24 Hours: <?=$data['>24']?></div>
            </td>
            <td class="text-center" colspan="2">
                <div>Total Patients in ED over 75: <?=$data['edPatients']?></div>
                <div>> 9 Hours: <?=$data['75+p>9']?></div>
                <div>> 24 Hours: <?=$data['75+p>24']?></div>
            </td>
            <td class="text-center" colspan="2">
                Patient under 16 Years: <?=$data['patients_under_16']?>(<?php echo '('. round((100*$data['patients_under_16']/$data['edPatients']),2).'%)';?>)
            </td>
        </tr>
        <tr>
            <td class="text-center" colspan="3">
                <div class="col-sm-12">
                    Number of patients 0-4 hours = <?=$data['0-4']?>
                </div>

                <div class="col-sm-12">
                    Number of Patients  4-8 Hours = <?=$data['4-8']?> 
                </div>
                <div class="col-sm-12 text-center">
                   Number of Patients >8 Hours = <?=$data['>8']?>
                </div>
            </td>
            <td class="text-center" colspan="3">
                <div class="row">Patients by Co-ord category - Not Co-ord - <?=$data['not_coord']?></div>
                <div class="row">
                    C1 - <?=$data['C1']?>, M1 - <?=$data['M1']?>,
                </div>
                <div class="row">
                    C2 - <?=$data['C2']?>, M2 - <?=$data['M2']?>,
                </div>
                <div class="row">
                    C3 - <?=$data['C3']?>, M3 - <?=$data['M3']?>,
                </div>
                <div class="row">
                    C3 - <?=$data['C4']?>, M3 - <?=$data['M4']?>,
                </div>
                <div class="row">
                    C3 - <?=$data['C5']?>, M3 - <?=$data['M5']?>,
                </div>
                             
            </td>
        </tr>
        
    </table>
<div style="border: 1px solid #000000; background-color: #EEEEFF;">
<?php
}

echo  GridView::widget([
        'dataProvider' => $dataProvider,
        'export' => false,
        
        'resizableColumns' =>false,
        'rowOptions' => function ($model, $key, $index, $grid){
            return [
                'style' => 'border: 1px solid #ddd; ',
            ];
        },
        'columns' => [
            
            [
                'vAlign'=>'middle',
                'label' => '',
                'format' => 'raw',
                'value' => function($model) use ($tab){
                    if($tab == 'awaiting_admission' || $tab == 'allpatient'){
                        return '';
                    }
                    $modelSP = json_decode($model->sp_object,true);
                    $color = '';
                    $opacity = false;
                    if ($model->is_discharge){
                        $color = '#D3D3D3';
                        $opacity = .5;
                    } else {
                        $color = isset(Yii::$app->params['edTriageColor'][$modelSP['CoordinationCategoryCode']]) ? Yii::$app->params['edTriageColor'][$modelSP['CoordinationCategoryCode']] : '';
                    }
                    $class = (isset($model->infection_status) && $model->infection_status == '$') ? 'c-purple' :'c-red';
                    return ' <div style="border: 1px solid #dcdcdc;padding: 7px;background-color:'.$color.';"><span class="text-center '.$class.'">'.$model->infection_status.'</span>       </div> ';
                },
            ],
            
            [
                'format'    =>  'raw',
                'value' => function($model){
                    if($model->is_awaiting_admission) {
                        return '<i class="md-hotel" aria-hidden="true"> </i> <br>'.$model->current_ward_code.' '.$model->current_bed_code;
                    }
                    return '';
                },
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],     
            ],
                        
            [
                'attribute' => 'patient_name',
                'header' => 'Patient Name <br>(Board Number)',
                'value' => function($model){
                    return  $model->patient_name.' <span class="c-red">'.$model->infection_status.'</span><br>('.$model->board_number.') ';
                },
                'format' => 'raw',
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ],
                       
            [
                'label' => 'Sex/Age',
                'header' => 'Sex / Age',
                'headerOptions' => ['style' => 'min-width:48px;'],
                'value' => function ($model) {
                  $age = $model->getAge(strtotime($model->dob),$model->age);
                  return $model->sex.'/'.$age;
                },
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ], 
                        
            
            [
                'format' => 'raw',
                'attribute' => 'coordination_code',
                'label' => 'Co. Ca. Code',
                'header' => 'Co. Ca.<br> Code',
                'visible' => ($tab == 'awaiting_admission' || $tab == 'allpatient') ? true :false,
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ],  
            [
                'attribute' => 'ed_location',
                'header' => 'ED Location',
                'format' => 'raw',
                'value' => function ($model,$kew,$index){
                       return ($model->edLocation) ? $model->edLocation['name'] : 'None';
                   },
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ],
            [
                'format' => 'raw',
                'attribute' => 'timer',
                'header' => 'Time in ED',
                'value' => function ($data)  {
                    $second = strtotime("now") - $data['Tracker_timer'];
                    $hours = floor($second / 3600);
                    $minutes = floor(($second / 60) % 60);

                    return $hours.' : '.$minutes;
                },
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ], 
                    
            [
                'header' => 'Bed Location',
                'format'    =>  'raw',
                'value' => function($model){
//                    if($model->bed_status) {
//                        $bed_time = '';
//                        $bed_awaiting_time = '';
//                        if( $model->bed_awaiting_time ){
//                            $bed_awaiting_time = ArrayHelpers::getHourMin($model->bed_awaiting_time);
//                        }
//                        if($model->bed_time){                            
//                            $bed_time = ArrayHelpers::getHourMin($model->bed_time);
//                        } 
//                        if($bed_time && $bed_awaiting_time){
//                            return $model->bed_status.' '.$bed_time.' ('.$bed_awaiting_time.')';
//                        } else if($bed_time){
//                            return $model->bed_status.' '.$bed_time;
//                        } else if($bed_awaiting_time){
//                            return $model->bed_status.' '.$bed_awaiting_time;
//                        }
//                        
//                    }
                    return '';
                },
                'visible' => ((isset($tab) && ($tab == 'awaiting_admission' || $tab='allpatient')) || !isset($tab)) ? true :false,
            ],

            [
                'attribute' => 'cons_name_current',
                'label' => 'CONS',
                'visible' => ($tab == 'awaiting_admission' || $tab == 'allpatient') ? true :false,
                
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ],
            
            [
                'attribute' => 'ed_doctor',
                'header' => 'Clinician',
                'format' => 'raw',
                'value' => function ($model,$kew,$index){
                       return ($model->edDoctor) ? $model->edDoctor['name'] : 'None';
                 },
                
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ],
           
            [
                'attribute' => 'ed_nurse',
                'header' => 'ED Nurse',
                'format' => 'raw',
                'value' => function ($model,$kew,$index){
                   return ($model->edNurse) ? $model->edNurse['name'] : 'None';
                    
                 },
                         
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ],
            [
                'attribute' => 'ed_stream',
                'header' => 'Stream',
                'format' => 'raw',
                'value' => function ($model,$key,$index){
                    return isset(Yii::$app->params['edStream'][$model->ed_stream]) ? Yii::$app->params['edStream'][$model->ed_stream] : 'None';
                },
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ],
            [
                'attribute' => 'comment',
                'header' => 'Comment(s)',
                'headerOptions' => ['style' => 'min-width:180px;'],
                'format' => 'raw',
                'value' => function ($model,$key,$index){
                    return !empty($model->comment) ? substr($model->comment, 0, 30) : 'None';
                },
                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
            ],
                    
//            [
//                'vAlign'=>'middle',
//                'header' => 'Referrals',
//                'format' => 'raw',
//                'value' => function ($model,$key,$index){
//                    $count = count($model->patientReferrals);
//                    $countValue = ($count == '0' ) ? '0': $count;
//                    return 'Referral('.$countValue.')';
//                },
//                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
//            ],
//            [ 
//                'header' => 'Registered With',
//                'format' => 'raw',
//                'value' => function ($data)  {
//                        $modelSP = json_decode($data->sp_object,true);
//                        return $modelSP['PresentComplaint'];
//               },
//                'contentOptions' => ['style' => 'border-left: 1px solid #ddd;'],
//            ],
           
        ]
            ]);
 ?>
</div>