<?php
 use yii\helpers\Html;
?>
<div class="card-header bgm-lightgreen">
    <h2><?= Html::encode($this->title) ?></h2>
    <?if(!isset($hideCreate)):?>
    <?= Html::a('<i class="md md-add"></i>', ['create'], ['class' => 'btn bgm-teal btn-float waves-effect waves-effect waves-button waves-float']) ?>
    <?endif?>
</div> 
