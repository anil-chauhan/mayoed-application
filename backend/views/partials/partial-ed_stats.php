<?php
    $currentUrl = Yii::$app->urlManager->createUrl(['patient']);
?>
<style>
    .equal-size{
        width : 10.4%;
    }
    .ed-stats-color{
        background-color: #607d8b!important;    
    }
</style>
<div class="block-header">
    <!--<h2>ED Patient Administration</h2>-->
</div>
<div class="mini-charts">
    <div class="row">    
        <div class="col-sm-3">
            <a href="<?= $currentUrl.'?tab=allpatient'?>" class="btn-loading"">
                <div class="mini-charts-item ed-stats-color">
                    <div class="clearfix">
                        <div class="chart">
                            <small class="c-white">Total (Inc Triage)</small><br>
                            <div id="edPatientDiv" style="display: inline-block; width: 83px; height: 15px; vertical-align: top;">
                                <h3 class="c-white m-t-0 m-l-30"><span><?=$data['edPatients']?></span><h3>
                            </div>
                        </div>
                        <div class="count">
                            <small>Patients in ED</small>
                            <h2>
                                <?php
//                                    echo '<span id="ed" data-id="ed" class="second-info" data-second="'.$data['longestInEd'].'"></span>'.
                                        echo '<span class="c-red"> '.$data['red'].'</span>'.
                                         '<span class="c-orange"> '.$data['orange'].'</span>'.
                                         '<span class="c-yellow"> '.$data['yellow'].'</span>'.
                                         '<span class="c-green"> '.$data['green'].'</span>'.
                                         '<span style="color:#7eb4df!important;"> '.$data['blue'].'</span>';
                                ?> 
                            </h2>
                        </div>
                    </div>
                </div>
            </a>
        </div>
         <div class="col-sm-3">
            <a href="<?= $currentUrl.'/triage'?>" class="btn-loading">
                <div class="mini-charts-item ed-stats-color">
                    <div class="clearfix">
                        <div class="chart stats-bar-2"></div>
                        <div class="count">
                            <small>Number Awaiting Triage</small>
                            <h2><?php echo $data['awaitingTriage'].' ('.$data['awaitingTriageAmbulance'].' Ambulance)';?></h2>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        
       <div class="col-sm-3">
            <a href="<?= $currentUrl.'?tab=awaiting_admission'?>" class="btn-loading">
                <div class="mini-charts-item ed-stats-color">
                        <div class="clearfix">
                            <div class="chart stats-bar"></div>
                            <div class="count">
                                <small>PAB</small>
                                <h2> <?=$data['awaitingBed']?> (Longest: <span id="ed" data-id="ed" class="second-info" data-second="<?=$data['longestInEd']?>"></span>)
                                       </h2>
                            </div>
                        </div>
                </div>
            </a>
        </div>
        <div class="col-sm-3">
            <a href="<?= $currentUrl.'?tab=patients_16'?>" class="btn-loading">
                <div class="mini-charts-item ed-stats-color">
                        <div class="clearfix">
                            <div class="chart stats-bar"></div>
                            <div class="count">
                                <small>Patients under 16 </small>
                                <h2><?=$data['patients_under_16']?></h2>
                                <!--<small>Paeds ED </small>-->
                                <!--<h2><?php // echo $data['paediatrics']?></h2>-->
                            </div>
                        </div>
                </div>
            </a> 
        </div>
   </div>
<!--    <div class="row m-l-10 m-r-10 m-b-30 ed-stats-color c-white" style="border-bottom: 1px solid #fff;">
        <div class="col-sm-4">
            Number of patients 0-4 hours = <?php // echo $data['0-4']?>
        </div>
                     
        <div class="col-sm-4">
            Number of Patients  4-8 Hours = <?php // echo $data['4-8']?> 
        </div>
        <div class="col-sm-4">
           Number of Patients >8 Hours = <?php // echo $data['>8']?>
        </div>
    </div>-->
<!--     <div class="row m-b-10">
            <button class="m-l-10 btn bgm-red waves-effect waves-button waves-float equal-size"><small>Red: <?php // echo $data['red']?></small></button>
            <button class="btn bgm-orange waves-effect waves-button waves-float equal-size"><small>Orange: <?php // echo $data['orange']?></small></button>
            <button class="btn bgm-yellow waves-effect waves-button waves-float equal-size"><small>Yellow: <?php // echo ['yellow']?></small></button>
            <button class="btn bgm-green waves-effect waves-button waves-float equal-size"><small>Green: <?php // echo $data['green']?></small></button>
            <button class="btn bgm-blue waves-effect waves-button waves-float equal-size"><small>Blue: <?php // echo $data['blue']?></small></button>
            <button class="btn bgm-pink waves-effect waves-button waves-float equal-size"><small>Ambulatory Care: <?php // echo $data['ambulatory']?></small></button>
            <button class="btn bgm-deeppurple waves-effect waves-button waves-float equal-size"><small>Resus/Major: <?php // echo $data['resus-major']?></small></button>
            <button class="btn bgm-cyan waves-effect waves-button waves-float equal-size"><small>RAN/RAT: <?php // echo $data['ran-rat']?></small></button>
            <button class="btn bgm-bluegray waves-effect waves-button waves-float equal-size"><small>AMU Waiting: <?php // echo $data['mau_waiting']?></small></button>
        </div>-->
</div>
<script>
//    var canvas = document.getElementById("edPatientDiv");
//    var ctx = canvas.getContext("2d");
//    ctx.font="20px sans-serif";
//    ctx.fillStyle = "#ffffff";
//    ctx.fillText('Total: '+"<?=$data['edPatients']?>", canvas.width - 85, canvas.height - 14);
//    
//    ctx.font="12px sans-serif";
//    ctx.fillText('(Inc. Triage)', canvas.width - 85, canvas.height - 2);
    $(document).ready(function(){
         $('.second-info').each(function(e){
            var second = $(this).data('second');
            var id = $(this).data('id');
            if(second) {
              var d = new Date(second*1000);
              $('#'+id).tinyTimer({ from: d, format: '%0H{:}%0m' }); 
            }
        });
    });
    
</script>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/www/js/jquery.tinytimer.min.js',['depends' => [\backend\assets\AppAsset::className()]]); ?>
