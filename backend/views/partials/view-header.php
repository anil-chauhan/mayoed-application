<?php
use yii\helpers\Html;

$id = isset($id) ? $id : 'id';

?>
<div class="card-header bgm-lime">
    <h2><?= Html::encode($this->title) ?></h2>
    <ul class="actions <?= (isset($hidden) && $hidden) ? 'hidden': ''?>">
        <li class="dropdown">
            <a href="" data-toggle="dropdown">
                <i class="md md-more-vert"></i>
            </a>
            
            <ul class="dropdown-menu dropdown-menu-right">
                <li>
                   <?= Html::a('Update', ['update', 'id' => $model->$id]) ?>
                    <?=
                    Html::a('Delete', ['delete', 'id' => $model->$id], [
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ])
                    ?>
                 </li>
            </ul>
        </li>
    </ul>

</div>