<?php
use yii\helpers\Html;
?>
<div class="card-header bgm-bluegray">
    <h2><?= Html::encode($this->title) ?></h2>
</div>