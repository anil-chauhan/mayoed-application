<?php
    $currentUrl = Yii::$app->urlManager->createUrl(['patient']);
?>
<style>

    .ed-stats-color{
        background-color: #607d8b!important;    
    }
</style>
<div class="mini-charts">
    <div class="row">
        <div class="col-sm-3">
            <a href="<?= $currentUrl.'?tab=allpatient'?>" class="btn-loading"">
                <div class="mini-charts-item ed-stats-color">
                    <div class="clearfix">
                        <div class="chart">
                            <small class="c-white">Total (Inc Triage) :</small><br>
                            <div id="edPatientDiv" style="display: inline-block; width: 83px; height: 15px; vertical-align: top;">
                                <h3 class="c-white m-t-0 m-l-30"><span><?=$data['edPatients']?></span><h3>
                            </div>
                        </div>
                        <div class="count">
                            <small>Patients in ED</small>
                            <h2>
                                <?php
//                                    echo '<span id="ed" class="second-info" data-id="ed" data-second="'.$data['longestInEd'].'"></span>'.
                                      echo   '<span class="c-red"> '.$data['red'].'</span>'.
                                         '<span class="c-orange"> '.$data['orange'].'</span>'.
                                         '<span class="c-yellow"> '.$data['yellow'].'</span>'.
                                         '<span class="c-green"> '.$data['green'].'</span>'.
                                         '<span style="color:#7eb4df!important;"> '.$data['blue'].'</span>';
                                ?>
                            </h2>
                        </div>
                    </div>
                </div>
            </a>
        </div>

        <div class="col-sm-3">
            <a href="<?= $currentUrl.'/triage'?>" class="btn-loading">
                <div class="mini-charts-item ed-stats-color">
                    <div class="clearfix">
                        <div class="chart stats-bar-2"></div>
                        <div class="count">
                            <small>Number Awaiting Triage</small>
                            <h2><?php echo $data['awaitingTriage'].' ('.$data['awaitingTriageAmbulance'].' Ambulance)';?></h2>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        

         <div class="col-sm-3">
            <a href="<?= $currentUrl.'?tab=awaiting_admission'?>" class="btn-loading">
                <div class="mini-charts-item ed-stats-color">
                        <div class="clearfix">
                            <div class="chart stats-bar"></div>
                            <div class="count">
                                <small>PAB</small>
                                   <h2> <?=$data['awaitingBed']?> (Longest: <span id="ed" data-id="ed" class="second-info" data-second="<?=$data['longestInEd']?>"></span>)
                                       </h2>
                            </div>
                        </div>
                </div>
            </a>
        </div>
        <div class="col-sm-3">
            <a href="<?= $currentUrl.'?tab=patients_16'?>" class="btn-loading">
                <div class="mini-charts-item ed-stats-color">
                        <div class="clearfix">
                            <div class="chart stats-bar"></div>
                            <div class="count">
                                <small>Patients under 16 </small>
                                <h2><?=$data['patients_under_16']?></h2>
                                <!--<small>Paeds ED </small>-->
                                <!--<h2><?php // echo $data['paediatrics']?></h2>-->
                            </div>
                        </div>
                </div>
            </a>
        </div>
        
    </div>
</div>
<div class="card c-white ed-stats-color"  >
    <div class="row">
        <div class="col-sm-2 text-center"><?=$data['75+p']?> Patients age 75+<br> <?=$data['75+p>9']+$data['75+p>24']?> > 9H, <?=$data['75+p>24']?> >24H </div>
        <div class="col-sm-2 text-center">Total <?=$data['edPatients']?> Patients<br> <?=$data['>9']+$data['>24']?> > 9H, <?=$data['>24']?> >24H </div>
        <div class="col-sm-2 text-center">
            <div class="col-sm-12">
                Number of patients 0-4 hours = <?=$data['0-4']?>
            </div>

            <div class="col-sm-12">
                Number of Patients  4-8 Hours = <?=$data['4-8']?> 
            </div>
            <div class="col-sm-12 text-center">
               Number of Patients >8 Hours = <?=$data['>8']?>
            </div>
        </div>
<!--        <table border="1">
            <tr><td colspan="2">Number of patients</td></tr>
            <tr><td>Hours</td><td>Count</td></tr>
            <tr><td>0-4</td><td><?php // echo $data['0-4']?></td></tr>
            <tr><td>4-8</td><td><?php // echo $data['0-4']?></td></tr>
            <tr><td>>8</td><td><?php // echo $data['0-4']?></td></tr>
        </table>-->
        <div class="col-sm-2 text-center">
            Patient under 16 Yrs <br>
            <?=$data['patients_under_16']?><br>
            <?php echo ($data['edPatients']) ? '('. round((100*$data['patients_under_16']/$data['edPatients']),2).'%)' : 0 ;?>
        </div>
        <div class="col-sm-4 pull-right text-center">
            <div class="row">Patients by Co-ord category - Not Co-ord - <?=$data['not_coord']?></div>
            <div class="row">
                <div class="col-sm-2">C1 - <?=$data['C1']?>,</div>
                <div class="col-sm-2">C2 - <?=$data['C2']?>,</div>
                <div class="col-sm-2">C3 - <?=$data['C3']?>,</div>
                <div class="col-sm-2">C4 - <?=$data['C4']?>,</div>
                <div class="col-sm-2">C5 - <?=$data['C5']?>,</div>
            </div>
            <div class="row">
                <div class="col-sm-2">M1 - <?=$data['M1']?>,</div>
                <div class="col-sm-2">M2 - <?=$data['M2']?>,</div>
                <div class="col-sm-2">M3 - <?=$data['M3']?>,</div>
                <div class="col-sm-2">M4 - <?=$data['M4']?>,</div>
                <div class="col-sm-2">M5 - <?=$data['M5']?>,</div>
            </div>
            
        </div>
    </div>
</div>
<script>
//    var canvas = document.getElementById("edPatientDiv");
//    var ctx = canvas.getContext("2d");
//     ctx.font="20px sans-serif";
//    ctx.fillStyle = "#ffffff";
//    ctx.fillText('Total: '+"<?=$data['edPatients']?>", canvas.width - 85, canvas.height - 14);
//    
//    ctx.font="12px sans-serif";
//    ctx.fillText('(Inc. Triage)', canvas.width - 85, canvas.height - 2);
    $(document).ready(function(){
         $('.second-info').each(function(e){
            var second = $(this).data('second');
            var id = $(this).data('id');
            if(second) {
              var d = new Date(second*1000);
              console.log(d);
              $('#'+id).tinyTimer({ from: d, format: '%0H{:}%0m' }); 
            }
        });
    });
</script>
<?php $this->registerJsFile(Yii::$app->request->baseUrl.'/www/js/jquery.tinytimer.min.js',['depends' => [\backend\assets\AppAsset::className()]]); ?>
