<?php

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$requestedRoute = Yii::$app->controller->id;
?>

<?php $this->beginPage() ?> 
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class=""> <!--<![endif]-->
<html lang="<?= Yii::$app->language ?>"> 
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
        <link href="<?php echo $this->theme->baseUrl; ?>/vendors/animate-css/animate.min.css" rel="stylesheet">
        <link href="<?php echo $this->theme->baseUrl; ?>/vendors/sweet-alert/sweetalert2.css" rel="stylesheet">
        <link href="<?php echo $this->theme->baseUrl; ?>/vendors/material-icons/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="<?php echo $this->theme->baseUrl; ?>/vendors/socicon/socicon.min.css" rel="stylesheet">
        <link href="<?php echo $this->theme->baseUrl; ?>/vendors/noUiSlider/jquery.nouislider.min.css" rel="stylesheet">

        <!-- CSS -->
        <link href="<?php echo $this->theme->baseUrl; ?>/css/app.min.1.css" rel="stylesheet">
        <link href="<?php echo $this->theme->baseUrl; ?>/css/app.min.2.css" rel="stylesheet">

        <script src="<?php echo $this->theme->baseUrl; ?>/js/jquery-2.1.1.min.js"></script>

	    <link href="<?php echo $this->theme->baseUrl; ?>/vendors/select2/css/select2.material.css" rel="stylesheet">

    </head>
    <body onload="$('#loader').hide();">
		<?php $this->beginBody() ?>
        <div class="loader" id="loader"></div>
        <div class="wrap">
            <header id="header" class="bgm-cyan">
                <ul class="header-inner">
                    <li id="menu-trigger" data-trigger="#sidebar">
                        <div class="line-wrap">
                            <div class="line top"></div>
                            <div class="line center"></div>
                            <div class="line bottom"></div>
                        </div>
                    </li>
                     <li>
                        <a href="<?= Yii::$app->urlManager->createUrl('site/dashboard-index')?>" class="btn btn-sm btn-warning">Dashboard</a>
                        <a href="<?= Yii::$app->urlManager->createUrl('site/ward-dashboard')?>" class="btn btn-sm btn-warning">Ward Round</a>
                        <a href="<?= Yii::$app->urlManager->createUrl('patient?tab=minor')?>" class="btn btn-sm btn-warning">ANP/Minor</a>
                    </li>
                    <li style="text-align: center">
                        <div id="top-search-wrap" class="m-l-60" style="top:0; opacity:1;  margin-left: 370px!important; background: none!important; width: 50%" >
                            <form id="patient-search-form" method="GET" action="/patient">

                            <input type="text" id="searchPatient" placeholder="Search Patient" name="searchPatient" >
                            

                            </form>
                        </div>
                    </li>

                    <li class="pull-right">
                        <ul class="top-menu">
                            <li id="toggle-width">
                                <div class="toggle-switch">
                                    <input id="tw-switch" type="checkbox" hidden="hidden" checked="checked" >
                                    <label for="tw-switch" class="ts-helper"></label>
                                </div>
                            </li>
 <!--                            <li id="top-search">
                                <a class="tm-search" href=""></a>
                            </li>-->


                            <li class="dropdown">
                                <a data-toggle="dropdown" class="tm-settings" href=""></a>
                                <ul class="dropdown-menu dm-icon pull-right">
                                    <li>
                                        <a data-action="fullscreen" href=""><i class="md md-fullscreen"></i> Toggle Fullscreen</a>
                                    </li>
                                    <li>
                                        <a data-action="clear-localstorage" href=""><i class="md md-delete"></i> Clear Local Storage</a>
                                    </li>
                                    <li>
                                        <a href=""><i class="md md-person"></i> Privacy Settings</a>
                                    </li>
                                    <li>
                                        <a href=""><i class="md md-settings"></i> Other Settings</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>

                <!-- Top Search Content -->
                <div id="top-search-wrap">
                    <input type="text">
                    <i id="top-search-close">&times;</i>
                </div>
            </header>
            <?php if(isset($this->params['breadcrumbs'])):?>
            <?=
            Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                'options' => [
                    'class' => 'breadcrumb',
                    'style' => 'margin-bottom: 5px'
                ]
            ])
            ?>
            <?php endif ?>
          
            <section id="main">
                <aside id="sidebar">
                    <div class="sidebar-inner">
                        <div class="si-inner">
                            <div class="profile-menu">
                                <a href="">
                                    <div class="profile-pic">
                                        <img src="<?php echo $this->theme->baseUrl; ?>/img/profile-pics/profile-pic-3.jpg" alt="">
                                    </div>

                                    <div class="profile-info">
                                        <?= Yii::$app->user->identity->getUserName() ?>
                                        <i class="md md-arrow-drop-down"></i>
                                    </div>
                                </a>

                                <ul class="main-menu">
                                    <li>
                                        <a href="profile-about.html"><i class="md md-person"></i> View Profile</a>
                                    </li>
                                    <li>
                                        <a href=""><i class="md md-settings-input-antenna"></i> Privacy Settings</a>
                                    </li>
                                    <li>
                                        <a href=""><i class="md md-settings"></i> Settings</a>
                                    </li>
                                    <li>
                                    <li><?= Html::a('<i class="md md-history"></i>Logout', ['/site/logout']) ?></li>
                                    </li>
                                </ul>
                            </div>

                            <ul class="main-menu">
                                <li class="<?if(in_array($requestedRoute, ['site'])):?>active<?endif?>" >
                                    <?= Html::a('<i class="md md-home"></i>Dashboard', ['/']) ?>
                                </li>
                                <li class="<?if(in_array($requestedRoute, ['patient'])):?>active<?endif?>" >
                                    <?= Html::a('<i class="md md-person"></i>Patients', ['/patient']) ?>
                                </li>
                                <li class="<?if(in_array($requestedRoute, ['patient/triage'])):?>active<?endif?>" >
                                    <?= Html::a('<i class="md md-person"></i>Triage', ['/patient/triage']) ?>
                                </li>
                                 <li class="<?if(in_array($requestedRoute, ['patient/triage'])):?>active<?endif?>" >
                                    <?= Html::a('<i class="md md-person"></i>All Patients', ['/patient/all-patients']) ?>
                                </li>
                                <li class="<?if(in_array($requestedRoute, ['patient/review-patients'])):?>active<?endif?>" >
                                    <?= Html::a('<i class="md md-person"></i>Review Patients', ['/patient/review-patients']) ?>
                                </li>
                                <li class="<?if(in_array($requestedRoute, ['patient-referral'])):?>active<?endif?>" >
                                    <?= Html::a('<i class="md md-list"></i>Patient Referral', ['/patient-referral']) ?>
                                </li>
                                
                                <li class="btn-loading" ><?= Html::a('<i class="md md-list"></i>Awaiting Discharge', ['/patient?tab=discharge']) ?></li>
                                <li class="sub-menu">
                                    <?= Html::a('<i class="md md-dvr"></i>Settings', ['']) ?>
                                    <ul class="main-menu m-t-0">        
                                         <?php if(!Yii::$app->user->identity->isBasicUser()){ ?>
                                                 
                                            <li class="btn-loading" ><?= Html::a('<i class="md md-list"></i>Manage lists of named doctors', ['/eddoctor/']) ?></li>
                                            <li class="btn-loading" ><?= Html::a('<i class="md md-list"></i>Manage lists of named nurses', ['/ednurse/']) ?></li>
                                            <li class="btn-loading" ><?= Html::a('<i class="md md-list"></i>Manage lists of named physical spaces', ['/ed-location/']) ?></li>
                                            <li class="btn-loading" ><?= Html::a('<i class="md md-list"></i>Manage lists & access rights of users', ['/accesspass']) ?></li>                                           
                                            <li class="btn-loading" ><?= Html::a('<i class="md md-list"></i>Manage lists of referral types', ['/referral-type']) ?></li>
                                            <li class="btn-loading" ><?= Html::a('<i class="md md-list"></i>Mail recipients', ['/mail-recipient']) ?></li>
                                            <li class="btn-loading" ><?= Html::a('<i class="md md-list"></i>Coordination Code', ['/coordination-code']) ?></li>
                                           <?php } ?> 
                                    </ul> 
                                </li>
                            </ul>
                        </div>
                    </div>
                </aside>
                <section id="content">
                    <div class="container" style="width : 100%">
                        <?= $content ?>
                    </div>
                </section>
           </section>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
