<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html class="ie6" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie7" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie8" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie9" lang="<?= Yii::$app->language ?>"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class=""> <!--<![endif]-->
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
        
        <!-- Vendor CSS -->
        <link href="<?php echo $this->theme->baseUrl; ?>/vendors/animate-css/animate.min.css" rel="stylesheet">
        <link href="<?php echo $this->theme->baseUrl; ?>/vendors/sweet-alert/sweet-alert.min.css" rel="stylesheet">
        <link href="<?php echo $this->theme->baseUrl; ?>/vendors/material-icons/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="<?php echo $this->theme->baseUrl; ?>/vendors/socicon/socicon.min.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="<?php echo $this->theme->baseUrl; ?>/css/app.min.1.css" rel="stylesheet">
        <link href="<?php echo $this->theme->baseUrl; ?>/css/app.min.2.css" rel="stylesheet">
        <style>
            a, a:hover {
                color: #fff;
            }
        </style>
        <!--[if lte IE 8]>
            <style>
                .ie9-only {
                    display: block !important;
                }
                body {
                    margin-top: 50px;
                }
            </style>
        <![endif]-->
    </head>
    
    <body class="login-content">
        <div class="ie9-only">Please upgrade your browser to IE 9 or above.</div>
        <?php $this->beginBody() ?>
        <!-- Login -->
        <?= $content ?>
        <script src="<?php echo $this->theme->baseUrl; ?>/js/jquery-2.1.1.min.js"></script>
        <script src="<?php echo $this->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo $this->theme->baseUrl; ?>/vendors/waves/waves.min.js"></script>
        <script src="<?php echo $this->theme->baseUrl; ?>/js/functions.js"></script>
        
        <script src="<?php echo $this->theme->baseUrl; ?>/vendors/moment/moment.min.js"></script>
        <script src="<?php echo $this->theme->baseUrl; ?>/vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
        
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl();?>/www/js/placeholder.js"></script>
        <script src="<?php echo Yii::$app->getUrlManager()->getBaseUrl();?>/www/js/library/fileinput.min.js"></script>
    </body>
</html>
</body>
</html>
<?php $this->endPage() ?>