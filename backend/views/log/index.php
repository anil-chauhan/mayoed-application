<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\LogsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <?= $this->render( '/partials/list-header', ['hidden' => true] ); ?>
    
    <div class="clearfix"></div>
    
    <?= GridView::widget(array_merge([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'date',
                'label' => 'Created At',
                'filter' => false
            ],
            'total',
//            'id',
//            'user_id',
//            'patient_name',
//            'replace_old',
//            'replace_new',
            // 'updated_at',
            // 'log_type',
//             'board_number',
            // 'comment',
            // 'code_name',
            // 'patient_id',

//            ['class' => 'yii\grid\ActionColumn'],
        ], 
      ], Yii::$app->params['widgetOptions']['gridView'])); ?>

</div>