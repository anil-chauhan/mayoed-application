<?php

use common\models\Logs;
use common\models\LogType;

?>
<style>
    .table > tbody > tr {
        color: #000 !important; 
    }
</style>
<table id="data-table-basic" class="table table-striped bootgrid-table" aria-busy="false">

    <tbody>
        <?php
        if (!empty($model->logs)) {
            foreach ($model->logs as $key => $value):
                $replace_type = LogType::getName($value->log_type);
                $replace_old = '';
                $replace_new = '';
                $user;
                $logMsg = '';
                $locationFlag = false;
                $alterUser = $value->alterUser['firstname'] . ' ' . $value->alterUser['lastname'];
                ?>
                <tr data-row-id="<?php echo $key; ?>">
                    <td class="text-left">

                        <?php
                        if ($replace_type == 'Location') {
                            $replace_old = Logs::location($value->replace_old);
                            $replace_new = Logs::location($value->replace_new);
                            $locationFlag = true;
                        } elseif ($replace_type == 'Doctor') {
                            $replace_old = Logs::doctor($value->replace_old);
                            $replace_new = Logs::doctor($value->replace_new);
                        } elseif ($replace_type == 'Nurse') {
                            $replace_old = Logs::nurse($value->replace_old);
                            $replace_new = Logs::nurse($value->replace_new);
                        } elseif ( $replace_type == 'Stream') {
                            $replace_old = isset(Yii::$app->params['edStream'][$value->replace_old]) ? Yii::$app->params['edStream'][$value->replace_old] : '';
                            $replace_new = isset(Yii::$app->params['edStream'][$value->replace_new]) ? Yii::$app->params['edStream'][$value->replace_new] : '';
                        } elseif ( $replace_type == 'Comment') {
                            $replace_old = $value->replace_old;
                            $replace_new = $value->replace_new;
                        } elseif ( $replace_type == 'Discharge') {
                            $replace_old = ($value->replace_old) ? 'Active' : 'Inactive';
                            $replace_new = ($value->replace_new) ? 'Active' : 'Inactive';
                        }
                        if ($replace_type == "Comment" || $replace_type == "Discharge" || $replace_type == "Stream") {
                            $msg = $replace_type . ',  has been changed from <b>' . $replace_old . '(old)</b>  to <b>' . $replace_new . '(new)</b> at ';
                        } else {
                            if ($locationFlag) {
                                $msg = $replace_type . ', <b>' . $replace_new . '</b> has been changed from <b>' . $replace_old . '</b> at ';
                            } else {
                                $msg = $replace_type . ', <b>' . $replace_new . '</b> has taken over from <b>' . $replace_old . '</b> at ';
                            }
                            if ( $replace_type == 'Admission') {
                                $msg = 'A Request for Admission was made at ';
                            } elseif ( $replace_type == 'Ward') {
                                $msg = $value->replace_new . ' ward is allocated at';
                            }
                        }
                        $addedBy = 'Generic User';
                        $code = (($value->code_name) ? $value->code_name : '');
                        if($code && strpos($code, '--') !== false){
                            $array = explode('--', $code);
                            $addedBy = $array[0];
                        }
                        echo $msg . date('H:i, dS F Y', strtotime($value->created_at)) . '. Added by ' . $addedBy;
                        ?>
                    </td>

                </tr>
            <?php
            endforeach;
        } else {
            ?>
            <tr>No result found.</tr>
<?php } ?>    
    </tbody>
</table>
