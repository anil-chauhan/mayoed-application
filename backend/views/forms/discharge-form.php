<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EdDoctor;
use common\models\EdLocation;
use common\models\EdNurse;

/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php
    $form = ActiveForm::begin([
        'action' => Yii::$app->urlManager->createUrl(['patient/ed-discharge']),
        'options' => [
            'data' => [
                'on-done' => 'saved-consultant'
            ],
        ],
        'id' => 'save-discharge-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ]);
    //print_r($ward_list);die;
    ?>
    <div class="modal-header">
            <h4 id="referral-form-heading" class="modal-title">Discharge</h4>
    </div>
    <div class="modal-body bgm-white  p-t-5">            
         
            <input type="hidden" id="discharge_id" name="dischargeId" value="" >
            <input type="hidden" id="discharge_type" name="discharge" value="0" >
            <div class="danger">
                <h4 id="discharge-msg"></h4>
            </div></br></br >
            
            <div class="clearfix"></div>
            <div class="row">
                <div class="form-group fg-float col-sm-4">
                    <div class="fg-line">
                        <input type="text" id="discharge-board_number" class="input-small form-control fg-input" readonly="">
                    </div>
                    <label class="fg-label" for="discharge-board_number">Board Number</label>
                </div>

                <div class="form-group fg-float col-sm-4">
                    <div class="fg-line">
                        <input type="text" id="discharge-patient_name" class="input-small form-control fg-input" readonly="">
                    </div>
                    <label class="fg-label" for="discharge-patient_name">Patient Name</label>
                </div>

                <div class="form-group fg-float col-sm-4">
                    <div class="fg-line">
                        <input type="text" id="discharge-age" class="input-small form-control fg-input" readonly="">
                    </div>
                    <label class="fg-label" for="discharge-age">Age</label>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class=" m-t-10 c-gray " >
                <h4 class="m-b-20">Enter Verification Code</h4>
                <?php echo $form->field($model, 'discharge_code', Yii::$app->params['htmlTemplate']['input'])->passwordInput(['autocomplete' => 'off', 'maxlength' => true,'onkeyup'=>'fncheckPass(this.value)'])->label(false);?>
                <div style='display:none;color:red;' id='fill_discharge_error'>Please enter correct code</div>
                <div class="center col-sm-12 m-b-20">
                    <?= Html::submitButton('Update', ['class' => 'btn btn-primary btn-sm pull-right']) ?>
                </div>
                <div class="clearfix"></div>
            </div>
        
    </div>
    <div class="clearfix"></div>
    <div id="dismiss" class="modal-footer">
        <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    </div>
<?php ActiveForm::end(); ?>
<script>
    function fncheckPass(cval){
     
	
	if(cval !=''){
			
		$.ajax({
			method: "POST",
			url: "<?php echo \yii\helpers\Url::to(['accesspass/checkpass']);?>",
			data: { cval: cval},
                async:"false"
		})
		.done(function( msg ) {
			if(msg=='1'){
				$('#checkcode').val('1');
			}else{
				$('#checkcode').val('0');
			}
		});
	}
}
    $('#save-discharge-form').on('submit', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();

        flag = true;
        if ($('#checkcode').val() == '1') {

            $('#fill_discharge_error').hide();
        } else {
            flag = false;
            $('#fill_discharge_error').show();
        }
        if (flag) {
            $('#save-discharge-form').submit();
        }
    });
</script>



