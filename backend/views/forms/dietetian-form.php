<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\NurseReferral */
?>  
 <div class="nurse-referral-form">
 <?php $form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['patient/createdietetian']),
         ]); ?>
    <div class="hide">
        <?= $form->field($model, 'board_number')->hiddenInput()?>
        <?= $form->field($model, 'episode_number')->hiddenInput()?>
        <?= $form->field($model, 'patient_name')->hiddenInput() ?>
        <?= $form->field($model, 'consultant_name')->hiddenInput() ?> 
        <?= $form->field($model, 'ward')->hiddenInput(['value'=>'ED'])?>
    </div>
    <div class="modal-header">
            <h4 id="referral-form-heading" class="modal-title">Create Dietetian Alert</h4>
    </div>
     
   <div class="modal-body bgm-white  p-t-5">
         </br></br>
   <div class="row">
        <?php
            echo $form->field($model, 'patient_weight', Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true])->label('Please enter the Patient Weight (kg)');
        ?>
  
  </div>
      
    <div class="row c-black"> 
        <?= $form->field($model, 'commence_two_day_food_chart', Yii::$app->params['htmlTemplate']['checkbox'])->checkbox(array(), false)->label('Commence a 2 day food chart') ?>
    </div>
 
    <div class="row c-black">
        <?= $form->field($model, 'compact_protein_125ml_bd', Yii::$app->params['htmlTemplate']['checkbox'])->checkbox(array(), false)->label('Offer Fortisip Compact Protein 125ml bd') ?>
    </div>
  <div class="row">
        <?php
            echo $form->field($model, 'nurse_name', Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true]);
        ?>
    </div>
    <div class="row">
        <?php
            echo $form->field($model, 'contact_details', Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true]);
        ?>
    </div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
   </div>
 <div class="clearfix"></div>
    <div id="dismiss" class="modal-footer">
        <button  type="button" class="btn btn-link" data-number="2">Close</button>
    </div>
    <?php ActiveForm::end(); ?>
 </div>

<script>
    $("button[data-number=2]").click(function(){
    $('#dietetian-form').modal('hide');
});
  </script>