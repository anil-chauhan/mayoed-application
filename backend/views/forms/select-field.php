<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EdDoctor;
use common\models\EdLocation;
use common\models\EdNurse;

/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */


$locArray = EdLocation::getLocationList();
$locUpdateArr = [];
$paedsWaitKey = '';
$flag = false;
foreach($locArray as $key => $val){
    if($val['name'] == "Paeds P1"){
        $flag = true;
        $locUpdateArr[$key+1] = $locArray[$key];
        $paedsWaitKey = $key;
    } elseif ($val['name'] == "Paeds Waiting Room") {
        $locUpdateArr[$paedsWaitKey] = $val;
    } elseif ($flag) {
        $locUpdateArr[$key+1] = $val;
    } else {
        $locUpdateArr[$key] = $val;
    }
}

?>
<style>
/*    .bgm-bluegray{
        background-color: #607d8b!important;
    }*/
    .m-2{
        margin: 10px 0px 1px 1px !important;
    }
    .column-div{
        float: left !important;
    }
    .cell-height{
        min-height: 35px;
    }
    .btn-select{
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 3px 4px 0px 0 rgba(0, 0, 0, .20);
        border-radius: 7px;
        min-height: 25px;
        text-align: center;
    }
    .doctorSelect{
        min-height: 25px;
    }
    .locationSelect{
        min-height: 25px;
    }
    .nurseSelect{
        min-height: 25px;
    }
    .referralSelect{
        min-height: 25px;
    }
</style>
<div class="modal-header">
            <h3 id="referral-form-heading" class="modal-title">Select Field</h3>
    </div>
    <div class="modal-body bgm-white  p-t-5 c-black">            
        <div class ="selectBox doctor">
            <div class="row">
                <?php
                $doctors = EdDoctor::getDoctorList();
                $count = count($doctors);
                $count += 5 - $count%5;
                $ind = 1;
                $newCol = 1;
                foreach ($doctors as $key => $doctor){
                    
                    if($ind ==1){
                        echo '<div class="column-div m-r-5" style="width:19%">';
                    }
                    echo '<div class="cell-height"><a class="c-white doctorSelect" data-id="'.$doctor['id'].'" style="cursor: pointer;"><div class="bgm-blue text-center btn-select m-2">'.$doctor['name'].'</div></a></div>';
                    
                    if($ind == count($doctors)){
                        echo '</div>';
                    }else if( $newCol == $count/5){
                        $newCol = 0;
                        echo '</div><div class="column-div m-r-5" style="width:19%">';
                    }
                    $ind++;
                    $newCol++;
                    
                }
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class ="selectBox location">
            <div class="row">
                <?php
                $count = count($locUpdateArr);
                $count += 5 - $count%5;
                $ind = 1;
                $newCol = 1;
                foreach ($locUpdateArr as $key => $location){
                    if($ind ==1){
                        echo '<div class="column-div m-r-5" style="width:19%">';
                    }
                    echo '<div class="cell-height"><a class="c-white locationSelect" data-id="'.$location['id'].'" style="cursor: pointer;"><div class="bgm-blue text-center btn-select  m-2">'.$location['name'].'</div></a></div>';
                    
                    if($ind == count($locUpdateArr)){
                        echo '</div>';
                    }else if($newCol == $count/5){
                        $newCol =0;
                        echo '</div><div class="column-div m-r-5" style="width:19%">';
                    }
                    $ind++;
                    $newCol++;
                    
                }
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class ="selectBox nurse">
            <div class="row">
                <?php
                $nurses = EdNurse::getNurseList();
                $count = count($nurses);
                $count += 5 - $count%5;
                $ind = 1;
                $newCol = 1;
                foreach ($nurses as $key => $nurse){
                    
                    if($ind ==1){
                        echo '<div class="column-div m-r-5" style="width:19%">';
                    }
                    echo '<div class="cell-height"><a class="c-white nurseSelect" data-id="'.$nurse['id'].'" style="cursor: pointer;"><div class="bgm-blue text-center btn-select m-2">'.$nurse['name'].'</div></a></div>';
                    
                    if($ind == count($nurses)){
                        echo '</div>';
                    }else if($newCol == $count/5){
                        $newCol = 0;
                        echo '</div><div class="column-div m-r-5" style="width:19%">';
                    }
                    $ind++;
                    $newCol++;
                }
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class ="selectBox referral">
            <div class="row">
                <?php
                $referrals = Yii::$app->params['referralType'];
                $count = count($referrals);
                $count += 5 - $count%5;
                $ind = 1;
                $newCol = 1;
                asort($referrals);
                foreach ($referrals as $key => $referral){
                    
                    if($ind ==1){
                        echo '<div class="column-div m-r-5" style="width:19%">';
                    }
                    echo '<div class="cell-height"><a class="c-white referralSelect" data-id="'.$key.'" style="cursor: pointer;"><div class="bgm-blue text-center btn-select m-2">'.$referral.'</div></a></div>';
                    
                    if($ind == count($referrals)){
                        echo '<div class="cell-height"><a class="c-white referralSelectClear"  style="cursor: pointer;"><div class="bgm-Red text-center btn-select m-2" style="background-color: #f44336!important;
">Reset</div></a></div>';

                        echo '</div>';
                    }else if($newCol == $count/5){
                        $newCol = 0;
                        echo '</div><div class="column-div m-r-5" style="width:19%">';
                    }
                    $ind++;
                    $newCol++;
                }
                ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div id="dismiss" class="modal-footer">
        <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    </div>
<div class="clearfix"></div>

