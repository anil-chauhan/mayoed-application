<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EdDoctor;
use common\models\EdLocation;
use common\models\EdNurse;
use common\models\NurseReferral;

/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php
    $form = ActiveForm::begin([
        'action' => Yii::$app->urlManager->createUrl(['patient/update-data']),
        'options' => [
            'data' => [
                'on-done' => 'saved-consultant',
                'autocomplete' => "Off"
            ],
        ],
        'id' => 'save-replace-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ]);
    //print_r($ward_list);die;
   ?>
<!--<input style="display:none" type="text" name="fakeUsername"/>
<input style="display:none" type="password" name="fakePassword"/>-->
    <div class="modal-header">
            <h3 id="referral-form-heading" class="modal-title">Add Patient Details</h3>
    </div>
    <div class="modal-body bgm-white  p-t-5">            
            

     <div class="toggle m-t-10 " id="part_1">
        <div class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown">Identification</div>
        <div class ="p-l-0"> 
            <input type="hidden" id="replace-id" name="Patient[Patient_id]" value="" >
            <!--<input type="hidden" id="replace-board_number" name="Consultant[board_number]" value="" >-->
            <div  class="col-sm-4 c-gray center ">
                <?= $form->field($model, 'board_number', Yii::$app->params['htmlTemplate']['input'])->textInput(['readOnly'=>true]); ?>
            </div>
            <div  class="col-sm-4 c-gray center ">
                <?= $form->field($model, 'patient_name', Yii::$app->params['htmlTemplate']['input'])->textInput(['readOnly' => true]) ?>
            </div>
           
            <div  class="col-sm-4 c-gray center ">
               <?= $form->field($model, 'age',  Yii::$app->params['htmlTemplate']['input'])->textInput(['readOnly' => true, 'disabled' => 'disabled']); ?>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="col-sm-12 p-0 text-center c-black h3 m-0 p-b-30 c-brown">Patient Options</div>
            
            <div  class="col-sm-6 c-gray center ">
                <div class="clearfix"></div>
                <div  class="col-sm-4 ">
                <?php
    //                                'filter' => Html::activeDropDownList($searchModel, 'user_id', ArrayHelper::map(User::getAllUserList(), 'id', 'name'), ['class'=>'form-control','prompt' => 'Select Staff']),
                $locArray = EdLocation::getLocationList();
                $locUpdateArr = [];
                $paedsWaitKey = '';
                $flag = false;
                foreach($locArray as $key => $val){
                    if($val['name'] == "Paeds P1"){
                        $flag = true;
                        $locUpdateArr[$key+1] = $locArray[$key];
                        $paedsWaitKey = $key;
                    } elseif ($val['name'] == "Paeds Waiting Room") {
                        $locUpdateArr[$paedsWaitKey] = $val;
                    } elseif ($flag) {
                        $locUpdateArr[$key+1] = $val;
                    } else {
                        $locUpdateArr[$key] = $val;
                    }
                }
                ksort($locUpdateArr);
                echo Html::a(
                        'Ed Location',
                        'javascript:void(0)',
                        [
                           'data-toggle' => 'modal', 
                           'data-select' => 'location', 
                           'class' => 'selectField btn btn-xs btn-primary m-t-15', 
                           'data-target-color' => "bluegray",
                           'data-toggle' => "modal",
                           'style' => "width:100%!important",
                        ]
                    );
                    echo '</div><div class="col-sm-8">';
                echo $form->field($model, 'ed_location', Yii::$app->params['htmlTemplate']['input'])
                        ->dropDownList(ArrayHelper::map($locUpdateArr, 'id', 'name'), ['prompt' => '','disabled' => true])->label(false);
                ?>            
                </div>
            </div>
            <div  class="col-sm-6 c-gray center ">
                <div  class="col-sm-4">
                <?php
                    echo Html::a(
                    'Clinician',
                    'javascript:void(0)',
                    [
                       'data-toggle' => 'modal', 
                       'data-select' => 'doctor', 
                       'class' => 'selectField btn btn-xs btn-primary m-t-15', 
                       'data-target-color' => "bluegray",
                       'data-toggle' => "modal",
                        'style' => "width:100%!important",
                    ]
                );
                echo '</div><div class="col-sm-8">';
                    echo $form->field($model, 'ed_doctor', Yii::$app->params['htmlTemplate']['input'])
                        ->dropDownList(ArrayHelper::map(EdDoctor::getDoctorList(), 'id', 'name'), ['prompt' => '','disabled' => true])->label(false);
                ?>            
                </div>
            </div>
            <!--<div  class="col-sm-12 c-gray center ">-->
                 <?php
//                            echo $form->field($model, 'commentDoctor', Yii::$app->params['htmlTemplate']['input']);
                ?>            
            <!--</div>-->
            <div  class="col-sm-6 c-gray center ">
                <div  class="col-sm-4">
                 <?php
                 echo Html::a(
                    'Ed Nurse',
                    'javascript:void(0)',
                    [
                       'data-toggle' => 'modal', 
                       'data-select' => 'nurse', 
                       'class' => 'selectField btn btn-xs btn-primary m-t-15', 
                       'data-target-color' => "bluegray",
                       'data-toggle' => "modal",
                        'style' => "width:100%!important",
                    ]
                );
                echo '</div><div class="col-sm-8">';
                    echo $form->field($model, 'ed_nurse', Yii::$app->params['htmlTemplate']['input'])
                        ->dropDownList(ArrayHelper::map(EdNurse::getNurseList(), 'id', 'name'), ['prompt' => '','disabled' => true])->label(false);
                ?>            
                </div>
            </div>


            <div  class="col-sm-6 c-gray center  ">
                <div  class="col-sm-4 ">
                    <?php
                    echo Html::a(
                        'Referral Type',
                        'javascript:void(0)',
                        [
                           'data-toggle' => 'modal', 
                           'data-select' => 'referral', 
                           'class' => 'selectField btn btn-xs btn-primary m-t-15', 
                           'data-target-color' => "bluegray",
                           'data-toggle' => "modal",
                           'style' => "width:100%!important",
                        ]
                    );
                echo '</div><div class="col-sm-8">';
                    echo $form->field($model, 'referral_type', Yii::$app->params['htmlTemplate']['input'])
                    ->dropDownList(Yii::$app->params['referralType'], ['prompt' => '','disabled' => true])->label(false);
                ?>
                 <?php
//                    echo $form->field($model, 'ed_stream', Yii::$app->params['htmlTemplate']['select'])
//                        ->dropDownList(Yii::$app->params['edStream'], ['prompt' => '', 'style' => 'margin-top:10px;']);
                ?>           
                </div> 
            </div>
            
            <div  class="col-sm-12 c-gray center ">
                 <?php
                   echo $form->field($model, 'comment', Yii::$app->params['htmlTemplate']['input']);
                ?>            
            </div>
            <div  class="col-sm-12 c-gray center ">
                 <?php
                    echo '<div class="clear-fix"></div><div class ="row"><div class="col-sm-3">';
                     echo Html::a('Dietetian Alert', 'javascript:void(0)', 
                            [
                                'data-toggle' => 'modal', 
                                'class' => 'float-left dietetianClass btn btn-xs btn-primary btn-loading', 
                                'data-target-color' => "bluegray",
                                'data-toggle' => "modal",
                                'id' => "dietetianAnchor",
                                'style' => "width:100%!important",
                            ]); 
                             echo '</div><div class="col-sm-3">';
                        echo Html::a('Episodes', 'javascript:void(0)', 
                            [
                                'data-toggle' => 'modal', 
                                'class' => 'float-left episodesClass btn btn-xs btn-primary btn-loading', 
                                'data-target-color' => "bluegray",
                                'data-toggle' => "modal",
                                'id' => "episodeAnchor",
                                'style' => "width:100%!important",
                            ]); 
                            echo '</div><div class="col-sm-3">';
                        echo Html::a('Patient Info', 'javascript:void(0)', 
                            [
                                'data-toggle' => 'modal', 
                                'class' => 'float-left dropdownClass btn btn-xs btn-primary btn-loading', 
                                'data-target-color' => "bluegray",
                                'data-toggle' => "modal",
                                'id' => "dropdownAnchor",
                                'style' => "width:100%!important",
                            ]); 
                            echo '</div><div class="col-sm-3">';
                        echo Html::a('Discharge', 'javascript:void(0)', [
                            'data-toggle' => 'modal', 
                            'data-target-color' => "bluegray",
                            'class' => 'create-consultant-button btn btn-primary btn-xs c-white discharge dischargeClass',
                            'style' =>'background-color:#2196f3!important;',
                            'data-discharge' => 0,
                            'id' => "dischargeAnchor",
                           'style' => "width:100%!important",
                        ]);    
                        ?>            
                    </div>
                </div>
            </div>
            
            <div class="clearfix"></div>
        </div>

        <div class="clearfix"></div>
        <div class=" m-t-10 c-gray " >
            <h4 class="m-b-20">Enter Verification Code</h4>
            <?php echo $form->field($model, 'code', Yii::$app->params['htmlTemplate']['inputPassword'])->textInput([
                'autocomplete' => 'asdeda',
                'maxlength' => true,
                'onkeyup'=>'fncheckpass( this.value, "'.\yii\helpers\Url::to(['accesspass/checkpass']).'" , "checkcode")'])->label(false);?>
            <div style='color:red;' class='fill_error'>Please Fill the correct code</div>
            <div class="center col-sm-12 m-b-20">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg pull-right accesspass-btn' : 'btn btn-primary pull-right', 'disabled' => 'disabled']) ?>
            </div>
        </div>
    </div> 
    <div class="clearfix"></div>
   </div>
    <div class="clearfix"></div>

    <div id="dismiss" class="modal-footer">
        <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    </div>
<input type='hidden' value='0' id='checkcode' />
<?php ActiveForm::end(); ?>
<div class="clearfix"></div>

<!--Dietetian-alert-form-->
<div class="modal fade" data-modal-color="bluegray" id="dietetian-form" data-backdrop="static" data-keyboard="true" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;z-index: 1050;">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php
            $referralModel = new common\models\NurseReferral();
            ?> 
            <?= $this->render('../forms/dietetian-form', ['model' => $referralModel]) ?>

        </div>
    </div> 
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.selectField').click(function(){
        var element = $(this);
        var type = element.data('select');
        $('.selectBox').addClass('hide');
        $('.'+type).removeClass('hide');
         $('#select-field').modal({
            show: true,
            keyboard: true
        });
    });
    
    $('.doctorSelect').click(function(){
        var element = $(this);
        var doctorId = element.data('id');
        $('#patient-ed_doctor').val(doctorId);
         console.log(doctorId);
         $('#select-field').modal('toggle');

    });
    $('.locationSelect').click(function(){
        var element = $(this);
        var locationId = element.data('id');
        $('#patient-ed_location').val(locationId);
        console.log(locationId);
         $('#select-field').modal('toggle');
    });
    $('.nurseSelect').click(function(){
        var element = $(this);
        var nurseId = element.data('id');
        $('#patient-ed_nurse').val(nurseId);
        console.log(nurseId);
         $('#select-field').modal('toggle');
    });
    $('.referralSelect').click(function(){
        var element = $(this);
        var referralId = element.data('id');
        $('#patient-referral_type').val(referralId);
        console.log(referralId);
         $('#select-field').modal('toggle');
    });
    
    $('.episodesClass').click(function(e){
        e.stopPropagation();
        board_number = $(this).data('board_number');
        var url = "<?=Yii::$app->urlManager->createUrl(['patient/get-episodes-data?id='])?>" + board_number;
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            success: function(data) {
                if(data.success) {
                    $('#episodesData').html(data.episodesView);
                    $('#episodes-view').modal({
                        show: 'true',
                        keyboard: true
                    });
                    $('#loader').hide();
                } 
            }
        });
        
    });
    
});

    
$('.dischargeClass').click(function(e){
   e.stopPropagation();
   var element = this;
   var $deleteUrl = $(element).data('delete');
   var $discharge = $(element).data('discharge');
   var $id = $(element).data('id');
   var name = $(element).data('name');
   var boardNumber = $(element).data('boardnumber');
   var age = $(element).data('age');
   console.log(boardNumber)

   $('#deleteRecord').attr('href', $deleteUrl);
   $('#discharge_id').val($id);
   $('#discharge-patient_name').val(name);
   $('#discharge-board_number').val(boardNumber);
   $('#discharge-age').val(age);
   focusInputs();

   if ($discharge) {
       $('#discharge_type').val($discharge);
   }

   $('#discharge-form').modal({
       show: 'true'
   });
   if ($discharge == '1') {
//            $('#alertError').data('value')
       $('#discharge-msg').html('Do you wish to cancel the discharge of this patient?');
   } else {
       $('#discharge-msg').html('Are you sure you want to mark this patient as discharged?');
   }
});

</script>