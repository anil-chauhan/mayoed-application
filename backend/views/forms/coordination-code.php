<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\CoordinationCode;


/* @var $this yii\web\View */
/* @var $model common\models\Cleaning */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
/*    .bgm-bluegray{
        background-color: #607d8b!important;
    }*/
    .m-2{
        margin: 10px 0px 1px 1px !important;
    }
    .column-div{
        float: left !important;
    }
    .cell-height{
        min-height: 35px;
    }
    .btn-select{
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 3px 4px 0px 0 rgba(0, 0, 0, .20);
        border-radius: 7px;
        min-height: 25px;
        text-align: center;
    }
    .doctorSelect{
        min-height: 25px;
    }
    .locationSelect{
        min-height: 25px;
    }
    .nurseSelect{
        min-height: 25px;
    }
    .referralSelect{
        min-height: 25px;
    }
    .codeSelect 
    {
        min-height:25px;
    }
</style>
<?php
    $form = ActiveForm::begin([
        'action' => Yii::$app->urlManager->createUrl(['patient/update-coordination-code']),
//        'options' => [
//            'data' => [
//                'on-done' => 'saved-coo',
//                'autocomplete' => "Off"
//            ],
//        ],
        'id' => 'save-coordination-form',
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ]);
    //print_r($ward_list);die;
   ?>
<div class="modal-header">
            <h3 id="referral-form-heading" class="modal-title">Manchester Triage Code</h3>
    </div>
    <div class="modal-body bgm-white  p-t-5 c-black">            
        <div class ="selectBox codeSelectt">
<!--            <div class="row">-->
                <?php
                $code = CoordinationCode::getCodeList();
//                $count = count($code);
//                $count += 5 - $count%5;
//                $ind = 1;
//                $newCol = 1;
                foreach ($code as $key => $code){
                     echo '<div class="column-div m-r-5" style="width:19%">';
                     echo '<div class="cell-height"><a class="c-white codeSelect" data-id="'.$code['id'].'"  data-code="'.$code['code'].'"  style="cursor: pointer;"><div class="bgm-blue text-center btn-select m-2">'.$code['code'].'</div></a></div></div>';
                 
                }
                ?>
<!--            </div>-->
            <div class="clearfix"></div>
        </div>
        <div class="col-sm-12 c-gray center ">
                 <?php
                   echo  $form->field($model, 'Patient_id')->hiddenInput()->label(false);
                   echo $form->field($model, 'coordination_code')->textInput(['readonly'=> true]);
                ?>            
        </div>
            <div class="clearfix"></div>

            <div class="center col-sm-12 m-b-20">
                 <a class="btn btn-sm bgm-green pull-right" data-required="coordinationCode" onclick="tgForm(this)">Finish</a>
                <?php //Html::submitButton('Finish' , ['class' => 'btn btn-success btn-lg pull-right accesspass-btn', 'disabled' => 'disabled']) ?>
            </div>
   
    <div class="clearfix"></div>

    </div>
   
    <div id="dismiss" class="modal-footer">
        <button  type="button" class="btn btn-link" data-dismiss="modal">Close</button>
    </div>
<div class="clearfix"></div>
<?php ActiveForm::end(); ?>


 
