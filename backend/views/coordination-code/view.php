<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CoordinationCodeOrder */

$this->title = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Coordination Code Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card hospital-groups-view" id="profile-main">
    <?= $this->render('/partials/view-header', ['model' => $model]); ?>

    <div class="card-body card-padding">
        <div class="pmb-block">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <?= DetailView::widget(array_merge([
                            'model' => $model,'attributes' => [
                                'id',
                                'code',
                                'color', 
                            ],
                        ], Yii::$app->params['widgetOptions']['detailView']));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
