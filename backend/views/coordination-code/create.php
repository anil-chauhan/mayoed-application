<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\CoordinationCodeOrder */

$this->title = 'Create Coordination Code Order';
$this->params['breadcrumbs'][] = ['label' => 'Coordination Code Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">

    <?= $this->render('/partials/create-header', []); ?>

    <div class="card-body card-padding">


        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>

    </div>
</div>
