<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CoordinationCodeOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coordination Code Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= $this->render( '/partials/list-header', [] ); ?>

    <?= GridView::widget(array_merge([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'code',
            'color',
            'code_order',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ], Yii::$app->params['widgetOptions']['gridView'])); ?>
</div>
