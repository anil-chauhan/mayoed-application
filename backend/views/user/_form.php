<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

use app\models\HospitalGroups;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <?php
                echo $form->field($model, 'firstname', Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true]);
            ?>
        </div>
        <div class="col-sm-6">
        <?php
            echo $form->field($model, 'lastname', Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true])
        ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php 
                echo $form->field($model, 'status', Yii::$app->params['htmlTemplate']['select'])->dropDownList(Yii::$app->params['userStatus'], ['prompt' => '']);
            ?>
        </div> 
        <div class="col-sm-6">
            <?php
                echo $form->field($model, 'type', Yii::$app->params['htmlTemplate']['select'])->dropDownList(Yii::$app->params['userType'], ['prompt' => '']);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php
                echo $form->field($model, 'email', Yii::$app->params['htmlTemplate']['input'])->textInput(['maxlength' => true]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php
                echo $form->field($model, 'password', Yii::$app->params['htmlTemplate']['input'])->passwordInput(['maxlength' => true]);
            ?>
        </div>
        <div class="col-sm-6">
            <?php
                echo $form->field($model, 'confirm_password', Yii::$app->params['htmlTemplate']['input'])->passwordInput(['maxlength' => true]);
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::a('Back', ['/user'], ['class' => 'btn btn-danger']) ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>   
