<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= $this->render( '/partials/list-header', []); ?>
    <?= GridView::widget(array_merge([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'firstname',
            'lastname',  
            'email:email',
            'updated_at:datetime',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ],Yii::$app->params['widgetOptions']['gridView'])); ?>

</div>
