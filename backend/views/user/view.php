<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->getUserName();
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card hospital-groups-view" id="profile-main">
    <?= $this->render('/partials/view-header', ['model' => $model]); ?>

    <div class="card-body card-padding">
        <div class="pmb-block">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <?= DetailView::widget(array_merge([
                        'model' => $model,
                        'attributes' => [
                            [
                                'label' => 'Name',
                                'value' => $model->getUserName()
                            ],
                            'email:email',
                            'address1',
                            'address2',
                            'county',
                            'phone',
                            'dateofbirth:date',
                            [
                                'attribute' => 'type',
                                'value'    => $model->getType(),
                            ],
                            [
                                'attribute' => 'status',
                                'value'     => $model->getStatus(),
                            ],
                            'created_at:datetime',
                            'updated_at:datetime',
                            ]
                        ], Yii::$app->params['widgetOptions']['detailView']));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
