<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\EdDoctor */

$this->title = 'Create Accesspass';
$this->params['breadcrumbs'][] = ['label' => 'Accesspass ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">

    <?= $this->render('/partials/create-header', []); ?>

    <div class="card-body card-padding">


        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>

    </div>
</div>
