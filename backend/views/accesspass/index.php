<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Accesspasses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= $this->render( '/partials/list-header', [] ); ?>

    <?= GridView::widget(array_merge([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'password',
            'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ], Yii::$app->params['widgetOptions']['gridView'])); ?>
</div>

