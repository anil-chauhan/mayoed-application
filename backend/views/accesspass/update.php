<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\EdDoctor */

$this->title = 'Update Accesspass: ' . $model->Accesspass_id;
$this->params['breadcrumbs'][] = ['label' => 'Accesspass', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->Accesspass_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">
    <div class="row">
        <div class="card">
            <?= $this->render('/partials/create-header', []); ?>

            <div class="card-body card-padding">
                <?=
                $this->render('_form', [
                    'model' => $model,
                ])
                ?>
            </div>
        </div>
    </div>
</div>
