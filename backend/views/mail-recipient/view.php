<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MailRecipient */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Mail Recipients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card hospital-groups-view" id="profile-main">
    <?= $this->render('/partials/view-header', ['model' => $model]); ?>

    <div class="card-body card-padding">
        <div class="pmb-block">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <?= DetailView::widget(array_merge([
                            'model' => $model,'attributes' => [
                                'id',
                                'name',
                                'email',
//                                'alter_user_id',
                            ],
                        ], Yii::$app->params['widgetOptions']['detailView']));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
