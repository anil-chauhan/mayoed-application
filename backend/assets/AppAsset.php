<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'theme/material/js/bootstrap.min.js',
        'theme/material/vendors/select2/js/select2.js',
        'theme/material/js/general.js',
        'theme/material/vendors/moment/moment.min.js',
        'theme/material/vendors/nicescroll/jquery.nicescroll.min.js',
        'theme/material/vendors/waves/waves.js',
        'theme/material/vendors/bootstrap-growl/bootstrap-growl.min.js',
        'theme/material/vendors/sweet-alert/sweetalert2.min.js',
        'theme/material/vendors/noUiSlider/jquery.nouislider.all.min.js',
        'theme/material/vendors/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',
        'www/js/jquery.mobile.custom.min.js',
        'theme/material/vendors/chosen/chosen.jquery.min.js',
        'theme/material/js/functions.js',
        'www/js/library/fileinput.min.js',
        'www/js/library/jquery.tinytimer.min.js',
        'www/js/jquery.idle.js',
        'www/js/ajax.js',
        'theme/material/js/charts.js',
        'theme/material/js/charts.js',
        'theme/material/vendors/sparklines/jquery.sparkline.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
