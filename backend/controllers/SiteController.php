<?php

namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use common\models\SignupForm;
use common\models\Patient;
use common\models\EdLocation;
use common\models\MailRecipient;
use yii\filters\VerbFilter;
/**
 * Site controller
 */ 

class SiteController extends Controller {
     
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => [
                            'login', 'error', 'register', 'patient-sp', 'bed-sp','test-mail',
                            'dashboard-index','ward-dashboard'
                            ],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
     public function actionIndex() {
           
        $data = Patient::countInfoEd(); 
        
        // Ordering NULL values to the last by placing a minus(-) sign in front of the
        // column name
             
        $locations = EdLocation::find()->where(['=', 'location_type', 1])
                                       ->orderBy('-[location_order] DESC, name')->all(); 
        
        return $this->render('index',[
                'data' => $data,
                'locations' => $locations,
                'model' => new Patient()
            ]);
        
    }
     
    public function actionDashboardIndex() {
        $data = Patient::countInfoEd(); 
        
        // Ordering NULL values to the last by placing a minus(-) sign in front of the
        // column name
        $locations = EdLocation::find()
//                ->select(['*',"TRIM(TRAILING '}' FROM TRIM(LEADING '{' FROM NAME)) as trim_name"])
                ->select(['*',"name as trim_name"])
                ->where(['=', 'location_type', 1])
                ->orderBy('-[location_column_order] DESC, location_column_suborder')->all(); 


        return $this->render('dashboard-index',[
            'data' => $data,
            'locations' => $locations,
            'patients' => Patient::getUnassignedPatientModel(),
            'model' => new Patient()
        ]);
        
    }
    public function actionWardDashboard() {
        $data = Patient::countInfoEd(); 
        
        // Ordering NULL values to the last by placing a minus(-) sign in front of the
        // column name
        $locations = EdLocation::find()
//                ->select(['*',"TRIM(TRAILING '}' FROM TRIM(LEADING '{' FROM NAME)) as trim_name"])
                ->select(['*',"name as trim_name"])
                ->where(['=', 'location_type', 1])
                ->orderBy('-[location_column_order] DESC, location_column_suborder')->all()
                ; 
        return $this->render('ward-dashboard',[
            'data' => $data,
            'locations' => $locations,
            'patients' => Patient::getUnassignedPatientModel(),
            'model' => new Patient()
        ]);
        
    }

    public function actionLogin() {
        $this->layout = 'register';
            
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                    'model' => $model,
            ]);
        }
    }   
    
    public function actionRegister() {
       $this->layout = 'register';
       
       if (!\Yii::$app->user->isGuest) { 
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }
        
        return $this->render('register', [
                'model' => $model,
        ]);
    }  

    public function actionLogout() {
        Yii::$app->user->logout();
         
        return $this->goHome();
    } 
    
    //Extra Test Code

    public function actionPatientSp() {
        echo __LINE__ . "--" . __FILE__;
        echo "<pre>";
            print_r(\common\components\SPHelper::getAllEdPatients());
        echo "</pre>";
        exit;
        return $this->goHome();
    }
    
      public function actionTestMail() {
        try{
            $data = Patient::countMailInfoEd(); 
            $html = $this->renderPartial('mail-template2',['data' => $data]);
            Yii::$app->mailer->compose()
              ->setFrom('edtracker@hse.ie')
              ->setTo('enda.furey@twdc.ie')
              ->setSubject("Current ED Statistics")
              ->setHtmlBody($html)
              ->send();
            echo 'Mail Send';exit;
        } catch (InvalidParamException $e) {
                echo 'Error';exit();
            throw new BadRequestHttpException($e->getMessage());
        }        
   } 
        

}
