<?php

namespace backend\controllers;

use Yii;
use common\models\PatientReferral;
use common\models\Accesspass;
use common\models\PatientReferralSearch;
use common\models\Patient;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Logs;
use common\models\LogType;

/**
 * PatientReferralController implements the CRUD actions for PatientReferral model.
 */
class PatientReferralController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PatientReferral models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PatientReferralSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PatientReferral model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PatientReferral model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   
        $model = new PatientReferral();
        $params = Yii::$app->request->post();
        $logModel = new Logs();
        $session = Yii::$app->session;
        $code_pass = $session->get('code_pass');
        if(empty($code_pass)){
             return json_encode(array('success' => false));
        } 
        
         $Accesspass = Accesspass::find()
                    ->where(['password' => $params['PatientReferral']['code']])
                    ->one();
            
           
                
            $logModel->accesspass_id=$Accesspass->Accesspass_id;
            $logModel->code_name = $code_pass;
        
         $patientModel = Patient::find()->where(['Patient_id' => $params['PatientReferral']['patient_id']])->one();
//        $logModel->last_value = $patientModel->referral_type;
//        $logModel->log_type = LogType::getId('Referral');
//        $logModel->code_name = $code_pass;
                
           if($params['PatientReferral']['comment']){
            
            $logModel->replace_old = NULL;
            $logModel->replace_new = $params['PatientReferral']['comment'];
            
             $log = LogType::find()->where(['LogTypeDescription' => 'Comment'])->one();
             $logModel->log_type = $log->LogTypeID;
             $patientModel->comment   = $params['PatientReferral']['comment'];
             
                Logs::addDischarge($logModel,$patientModel);
               
              }   
             
                    $logModelRef = new Logs(); 
                    $logModelRef->setAttributes($logModel->getAttributes());
                    $log = LogType::find()->where(['LogTypeDescription' => 'Referral'])->one();
                    $replace_old =  $patientModel->referral_type;
                    $replace_new =  $params['PatientReferral']['referral_type'];
                    if($replace_old !== $replace_new){
                    $logModelRef->replace_old    = NULL;
                    $logModelRef->replace_new    = $replace_new;
                    $logModelRef->log_type       = $log->LogTypeID;
                    $logModelRef->accesspass_id  =$Accesspass->Accesspass_id;
                    if($logModelRef->replace_old != $logModelRef->replace_new){
                     $comment = $Accesspass->name.' added referral '.$replace_new;
                     Logs::addLog($logModelRef,$patientModel,$comment); 
                      
                   } 
               }
               
              
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //$patientModel->referral_type = $logModel->updated_value = $model->referral_type;
              $patientModel->referral_type = $model->referral_type;
            
            if($patientModel){ 
                $model->patient_id = $patientModel->Patient_id;
                $model->code       = $code_pass;
            }
            if($patientModel->save() && $model->save()){
                //Logs::add($logModel,$patientModel);
            }
            return $this->redirect(Yii::$app->request->referrer); 
        } else {
            return $this->redirect(Yii::$app->request->referrer); 
        }
    } 

    /**
     * Updates an existing PatientReferral model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PatientReferral model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PatientReferral model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PatientReferral the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PatientReferral::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
