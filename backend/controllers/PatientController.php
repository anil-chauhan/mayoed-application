<?php

namespace backend\controllers;

use Yii;
use common\models\Patient;
use common\models\LogType;
use common\models\Logs;
use common\models\PatientSearch;
use common\models\Accesspass;
use common\models\User;
use common\models\PatientReferral;
use common\models\NurseReferral;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\components\SPHelper; 

/**
 * PatientController implements the CRUD actions for Patient model.
 */
class PatientController extends BaseController
{
    /**
     * Lists all Patient models.
     * @return mixed
     */
    
    public function actionIndex() { 
        $params = Yii::$app->request->queryParams;
        $tab = isset($params['tab']) ? $params['tab'] : 'allpatient';
        $searchModel = new PatientSearch();
        $searchTriageModel = [];
        $triageDataProvider = [];

        $episodes = array();
        switch ($tab) :
            case 'allpatient':
                $dataProvider = $searchModel->search($params, 0);
                break;
            case 'resus':
                $params['ed_stream'] = 'resus';
                $dataProvider = $searchModel->search($params, 0);
                break;
            case 'triage-assist':
                $params['ed_stream'] = 'triage';
                $dataProvider = $searchModel->search($params, 0);
                break;
            case 'yellow':
                $params['ed_stream'] = 'y_stream';
                $dataProvider = $searchModel->search($params, 0);
                break;
            case 'minor':
                $params['ed_stream'] = 'minor_injury';
                $dataProvider = $searchModel->search($params, 0);
                break;
            case 'paediatrics':
                $params['ed_stream'] = 'paediatrics';
                $dataProvider = $searchModel->search($params, 0);
                break;
            case 'patients_16':
                $params['age_under_16'] = true;
                $dataProvider = $searchModel->search($params, 0);
                break;
            case 'awaiting':
                $params['ed_stream'] = false;
                $params['is_mau'] = '1'; 
                $dataProvider = $searchModel->search($params, 0);
                break;
            case 'awaiting_admission':
                $params['is_mau'] = false;
                $params['is_awaiting_admission'] = 1;
                $params['is_triage'] = 0;  
                $dataProvider = $searchModel->search($params, 0);
                break;
            case 'to_be_seen':
                $params['not_ed_doctor'] = true;
                $dataProvider = $searchModel->search($params);
                break;
            case 'discharge':
                $params['Patient']['is_discharge'] = 1;
                $dataProvider = $searchModel->search($params);
                break;
           
        endswitch;
        $data = Patient::countInfoEd();
       
        if (Yii::$app->request->getQueryParam('pdf')) {
            $params = Yii::$app->request->queryParams;
            if($tab != 'discharge'){
                $dataProvider->query->andWhere('is_discharge is null or is_discharge <> 1');
            }
            $pdf = Yii::$app->pdfLand;
            $pdf->destination = 'D';
            $pdf->content = $this->renderPartial('ed-index_pdf', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'data' => $data,
                'pdf' => true
            ]);
            $pdf->methods = [
                'SetHeader' => ['<div class="text-center">' . $tab . '<br></div>'],
                'SetFooter' => ['{PAGENO}']
            ];
            return $pdf->render();
        }

        return $this->render('index', [ 
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'episodeData' => [],
                    'data' => $data,
                    'patient' => new Patient(),
                    'referral' => new PatientReferral(),
                    'userList' => User::getUserList(),
                    'pdf' => false,
        ]);
    } 
    
    public function actionAllPatients() {
        
        $params        = Yii::$app->request->queryParams;
        $params['all'] = 1;
        $searchModel   = new PatientSearch();
        $dataProvider  = $searchModel->search($params);
         return $this->render('all-patients', [
               'searchModel' => $searchModel,
               'dataProvider' => $dataProvider,
               'model' => new Patient()
           ]);
     }
    
      public function actionReviewPatients(){
        
        $params = Yii::$app->request->queryParams;
        $params['review'] = 1;
        $searchModel = new PatientSearch();
        $dataProvider=$searchModel->search($params);
         return $this->render('review-patients', [
               'searchModel' => $searchModel,
               'dataProvider' => $dataProvider,
               'model' => new Patient()
           ]);
    }
    
    public function actionTriage() { 

        $params = Yii::$app->request->queryParams;
        $params['PatientSearch']['is_triage'] = 1;
       
        $searchModel = new PatientSearch();
        $dataProvider = $searchModel->search($params);
        $dataProvider->setSort(['defaultOrder' => ['PASAEAttendanceDate' => SORT_DESC]]);

        return $this->render('triage', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'data' => Patient::countInfoEd(),
            'model' => new Patient()
        ]);
    }  

    /**
     * Displays a single Patient model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Patient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Patient();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Patient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
       
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Patient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->is_deleted = 1;
        $model->Save();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Patient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Patient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    
    protected function findModel($id)
    {
        if (($model = Patient::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    //To prepare view for patient episode
    public function actionGetEpisodesData() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $params = Yii::$app->request->queryParams;
        
        $episodes = array();
        if ($params['id']) {
            $episodes = SPHelper::getPatients($params['id']);
        }
        return [
            'episodesView' => $this->renderPartial('partial-ed-episodes-view', [
                'episodes' => $episodes,
            ]), 
            'success' => true
        ];
    }
    
    //To active rpat of a patient 
    
    
    public function actionRpat($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $params = Yii::$app->request->queryParams;
        $model = $this->findModel($id);
        return [
            'rpatView' => $this->renderPartial('partial-ed-rpat-view', [
            'model' => $model,
            ]),
            'success' => true
        ];
    }
    
    
    public function actionUpdateRpat() {
         $params = Yii::$app->request->post();
         $model  = $this->findModel($params['Patient']['Patient_id']);
         if (!empty($model) && $model->load($params)) {
            
            $time = date("Y-m-d H:i:s");//strtotime('now');
             
             $model->Tracker_rpat_time = $time; 
//            $logModel = new Logs();
//            $user = NULL;
//            $logModel->last_value = NULL;
//            $logModel->updated_value = $model->rpat_active;
//            $logModel->log_type = LogType::getId('RPAT');
//            if ($logModel->last_value != $logModel->updated_value) {
//                $logModel->code_name = $code_pass;
//                if ($model->save()) {
//                    Logs::add($logModel, $model, $user);
//                }
//            }

            if($model->save()){
                $logModel = new \common\models\Logs();
                $log = LogType::find()->where(['LogTypeDescription' => 'RPAT'])->one();
                
                $Accesspass = Accesspass::find()
                    ->where(['password' => $params['Patient']['code']])
                    ->one();
            
                $session = Yii::$app->session;
                $code_pass = $session->get('code_pass');
                $logModel->accesspass_id=$Accesspass->Accesspass_id;
                $logModel->code_name = $code_pass;
                $logModel->log_type = $log->LogTypeID;
                $logModel->replace_old    = $model['Tracker_rpat_time'];
                $logModel->replace_new    = $time;
                //$logModel->code_name = $model->code_name;
                \common\models\Logs::addDischarge($logModel, $model);
            } 
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionEdDischarge() { 
        $params = Yii::$app->request->post();
        
        if (isset($params['dischargeId'])) {
            $model = $this->findModel($params['dischargeId']);
            $logModel = new \common\models\Logs();
            if($model->is_discharge == 0 || $model->is_discharge == NULL){
                $log = LogType::find()->where(['LogTypeDescription' => 'Discharge'])->one();
            } else {
                $log = LogType::find()->where(['LogTypeDescription' => 'CancelDischarge'])->one();
            }
            
            $logModel->log_type = $log->LogTypeID;
            
            if ($model && isset($params['discharge'])) { 
                
                 $Accesspass = Accesspass::find()
                    ->where(['password' => $params['Patient']['discharge_code']])
                    ->one();
            
                $session = Yii::$app->session;
                $code_pass = $session->get('code_pass');
                
                $logModel->accesspass_id=$Accesspass->Accesspass_id;
                $logModel->code_name = $code_pass;
//                if($model->Tracker_discharge_time == NULL) {
//                   $logModel->replace_old    = NULL;    
//                } else {
//                   $logModel->replace_old    = ($model->is_discharge == 0 || $model->is_discharge == NULL) ? '0' : date("Y-m-d H:i:s");   
//                }
//                $logModel->replace_new    = ($model->is_discharge == 0 || $model->is_discharge == NULL)?date("Y-m-d H:i:s"):'0';
                
                $olderLog = Logs::find()->where([
                          'board_number' => $model->board_number,'log_type'=>5,'episode_number'=>$model->episode_number])
                          ->orderBy(['id' => SORT_DESC])
                          ->one();
                
                if($olderLog){
                   
                 $logModel->replace_old   = $olderLog->replace_new; 
                 
                } 
                $logModel->replace_new   = date('Y-m-d H:i:s');    
                $distext = " discharge patient at ";
                $reversetext = " reverse discharge patient at "; 
                
                $logModel->comment = ($model->is_discharge == 0 || $model->is_discharge == NULL) ? $Accesspass->name.$distext.date("Y-m-d H:i:s"):$Accesspass->name.$reversetext.date("Y-m-d H:i:s");
                \common\models\Logs::addDischarge($logModel, $model);
                if($model->is_discharge == 0 || $model->is_discharge == NULL){
                 $model->Tracker_discharge_time = date("Y-m-d H:i:s"); 
                }
                $model->is_discharge = ($params['discharge'] == NULL || $params['discharge'] == 0 ) ? 1 : 0;
                $model->save();
                return $this->redirect(Yii::$app->request->referrer);
           
             } else {
             
                 return $this->redirect(Yii::$app->request->referrer);
            }
        } else {  
            return $this->redirect(Yii::$app->request->referrer);
        }
    } 
    
     public function actionUpdateData()
    {
        
        $updateModel = new Patient();
        
        if (Yii::$app->request->isAjax && $updateModel->load(Yii::$app->request->post())) {
            
            $params = Yii::$app->request->post();
            $model = $this->findModel($params['Patient']['Patient_id']);
            $type = false;
            $session = Yii::$app->session;
            $code_pass = $session->get('code_pass');
           
            $fields =   [
                'ed_location'=>'Location',
                'ed_doctor'=>'Doctor',
                'ed_nurse'=>'Nurse',
                'referral_type'=>'Referral',
                'comment'   => 'Comment',
            ];
            
             $Accesspass = Accesspass::find()
                    ->where(['password' => $params['Patient']['code']])
                    ->one();
             
            foreach ($fields as $field => $logType) {
                //if((!empty($updateModel->$field))){
                    //$type = Yii::$app->params['logType'][$logType];
                    $type  = LogType::find()->where(['LogTypeDescription' => $logType])->one();
                    $replace_old = $model->$field;
                    $replace_new = $updateModel->$field;
                    $logModel = new \common\models\Logs();
                   
            
                   $session = Yii::$app->session;
                   $code_pass = $session->get('code_pass');
                
                    $logModel->accesspass_id=$Accesspass->Accesspass_id;
                    $logModel->code_name = $code_pass;
                    if($field == 'referral_type' || $field == 'comment'){
                       $logModel->replace_old    = NULL;
                       $logModel->replace_new    = $replace_new;
                    } else {
                       $logModel->replace_old    = $replace_old;
                       $logModel->replace_new    = $replace_new;
                    }
                    $logModel->log_type       = $type->LogTypeID;
                 
                    if($field == 'comment'){
                      
                          Logs::addDischarge($logModel,$model);
                      
                    } else {
                    
                    if($logModel->replace_old != $logModel->replace_new){
                       
                     // if($field == 'comment'){
                      
                          //Logs::addDischarge($logModel,$model);
                      
                     // } else {
                          
                        if($field == 'referral_type')
                        $comment = $Accesspass->name.' added referral '.$replace_new;
                        else
                        $comment = 'Replace '.$logType.' '.$replace_old.' with '.$logType.' '.$replace_new;
                        Logs::addLog($logModel,$model,$comment); 
                      
                        
                        // }
                        }
                    }
                 
                    if($field == 'referral_type') { 
                        
                        $referralModel = new PatientReferral();
                       
                        if(!empty($model)){
                            $data = $model->getAttributes();
                            unset($data['Patient_id']);
                            if(isset($data['Tracker_admission_ed_time'])){
                                $referralModel->Tracker_admission_ed_time = $data['Tracker_admission_ed_time'];
                            }
                            $referralModel->setAttributes($data);
                            //$present_complaint = json_decode($model->sp_object,true)['PresentComplaint'];
                            if(isset($data['present_complaints'])){
                            $referralModel->present_complaints = $data['present_complaints'];
                            }
                        }

                        $referralModel->patient_name = $data['patient_name'];
                        $referralModel->age = $data['age'];
                        $referralModel->sex = $data['sex'];
                        $referralModel->dob = $data['dob'];
                        $referralModel->code = $code_pass;
                        $referralModel->referral_type = $updateModel->$field;
                        $referralModel->patient_id = $model->Patient_id;
                        $referralModel->save();
                       
                        
                    }
                }
            
            foreach ($updateModel as $key => $value) {
                
                if(isset($updateModel[$key]) && (!empty($value) || $value ==="0")){
                    $model->$key = trim($value); 
                }
            }
            
            $model->save();
            return $this->redirect(Yii::$app->request->referrer,200);
        }        
        return json_encode(array('success' => false));
        
    }
    
    public function actionCommentLog() {
     
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $params = Yii::$app->request->queryParams;
        
        $model = Patient::findOne($params['id']);
        return [
            'list' => $this->renderPartial('/log/comment-view', [
                'model' => $model,
            ]),
            'success' => true
        ];
    } 
    
     /**
     * Creates a new Consultant model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    public function actionUpdateComment()
    {
        $params = Yii::$app->request->post();
        if(isset($params['Patient']['commentUpdate'])){
            $params['Patient']['comment'] = $params['Patient']['commentUpdate'];
        }
        if(isset($params['Patient']['Patient_id'])){
            $logModel = new Logs();
            $model = Patient::findone($params['Patient']['Patient_id']);
             
            $Accesspass = Accesspass::find()
                    ->where(['password' => $params['Patient']['comment_code']])
                    ->one();
            
                $session = Yii::$app->session;
                $code_pass = $session->get('code_pass');
                
            $logModel->accesspass_id=$Accesspass->Accesspass_id;
            $logModel->code_name = $code_pass;
            $logModel->replace_old = NULL;
            $logModel->replace_new = $params['Patient']['commentUpdate'];
            
             $log = LogType::find()->where(['LogTypeDescription' => 'Comment'])->one();
             $logModel->log_type = $log->LogTypeID;
             $model->comment   = $params['Patient']['commentUpdate'];
             if($model->save()){
                 Logs::addDischarge($logModel,$model);
             }
           return $this->redirect(Yii::$app->request->referrer); 
        }
        return $this->redirect(Yii::$app->request->referrer); 
    }
    
    public function actionGetDropdowndata() {
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        $params = Yii::$app->request->queryParams;
        $model = Patient::findOne($params['id']);
        return [
            'dropdownData' => $this->renderPartial('partial-ed_view', [
                'model' => $model,
            ]),
            'success' => true
        ];
    }
     
    
    public function actionCreatedietetian()
    {
        $params = Yii::$app->request->post();
        $model = new NurseReferral();
        if ($model->load($params)&& $model->save()) {
            return $this->redirect(Yii::$app->request->referrer);
        }
    } 
    
     public function actionUpdateLocationData() {
        $params = Yii::$app->request->post();
        //Searching the patient with the given id and updating the location
        $model = Patient::findone($params['Patient']['Patient_id']);
        if (!empty($model)) {
            $model->ed_location = $params['Patient']['ed_location'];
            $logModel = new Logs();
            $session = Yii::$app->session;
            $code_pass = $session->get('code_pass');
             $Accesspass = Accesspass::find()
                    ->where(['password' => $params['Patient']['code']])
                    ->one();
            $type =  LogType::find()->where(['LogTypeDescription' => 'Location'])->one();
            $logModel->accesspass_id=$Accesspass->Accesspass_id;
            $logModel->code_name = $code_pass;
            $logModel->replace_old    = NULL;
            $logModel->replace_new = $params['Patient']['ed_location'];
            $logModel->log_type      = $type->LogTypeID;
            if ($logModel->replace_old != $logModel->replace_new) {
                $logModel->code_name = $code_pass;
                if ($model->save()) {
                    $comment= "Replace Location with Location".$params['Patient']['ed_location'];
                   Logs::addLog($logModel,$model,$comment); 
                }
            }
            //$model->save();
            return json_encode(array('success' => true));
        }
        return json_encode(array('success' => false));
    }
    
    public function actionGetNursePatient()
    { 
        Yii::$app->response->format = Response::FORMAT_JSON; 
        $params = Yii::$app->request->queryParams;
        $id = isset($params['id']) ? $params['id'] : '';
        $html = '';
        if($id){
            $cons = Patient::find()->where(['=','ed_nurse',$id])->all();
            
            foreach ($cons as $model) {
                $html .= '<div  class="col-sm-12 c-gray center m-b-20" id="review-'.$model->Patient_id.'" >

                    <div >
                        <label class="checkbox checkbox-inline m-r-20">
                            <input data-all="true" name="selectPatient[]"  class="select-on-check-all-2" data-type="check2" type="checkbox" value="'.$model->Patient_id.'" checked="checked"  disabled="true">
                            <i class="input-helper"></i>'.$model->patient_name.'
                        </label>
                   </div>                        
                   <span id="current_error" class="m-t-20 c-red"></span>
                </div>';
            }
            
            if(!empty($cons)){
                return [
                    'success' => true,
                    'patientList' => $this->renderPartial('nurse-patient_list',[ 'models' => $cons]),
                    'patientReview' => $html,
                ];
            }
        }
        return [
            'success' => true,
            'patientList' => '<div class="c-red">There is no patient with the selected record.</div>',
            'patientReview' => $html,
        ];
    }
    
    public function actionCreateHandover()
    { 
        $params = Yii::$app->request->post();
        if(isset($params['selectPatient']) && $params['selectPatient']){
            foreach ($params['selectPatient'] as $key => $value) {
                $model = Patient::findOne($value);
//                $logModel = new Logs();
//                
//                $logModel->last_value = $model->ed_nurse;
                
                //$model->ed_nurse = $logModel->updated_value = $params['Patient']['ed_nurse_to'];
                 $model->ed_nurse = $params['Patient']['ed_nurse_to'];
//                $logModel->log_type = LogType::getId('Nurse');
//                $logModel->code_name =  $params['Patient']['code'];
                $model->save();
                
//                $code_pass = Yii::$app->session->get('code_pass');
//                $password = '';
            
//                if(!empty($code_pass)){
//                    $password = explode('--', $code_pass);
//                    $password = $password[1];
//                }else {
//                     return json_encode(array('success' => false));
//                }
//                $model->code_name = $code_pass;
//
//                $accesspass = Accesspass::find()->where(['password' => $password])->one();
//                $user = NULL;
//                if (!Yii::$app->user->isGuest){
//                    $user = Yii::$app->user;
//                }
//                Logs::add($logModel, $model, $user);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    
    public function actionUpdateCoordinationCode() {
        
        $params = Yii::$app->request->post();
        $model = $this->findModel($params['Patient']['Patient_id']);
        if($model){
            $model->coordination_code = trim($params['Patient']['coordination_code']);
            $model->save();
            
        }
        
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        }
       
         return $this->redirect(Yii::$app->request->referrer);

    }
            
}
