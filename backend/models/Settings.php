<?php

namespace app\models;
use yii\db\Query;
//$connection = \Yii::$app->db;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property string $id
 * @property string $type
 * @property string $value
 * @property integer $rank
 *
 * @property Paitents[] $paitents
 * @property Paitents[] $paitents0
 * @property Paitents[] $paitents1
 * @property Paitents[] $paitents2
 * @property Paitents[] $paitents3
 * @property Paitents[] $paitents4
 * @property Paitents[] $paitents5
 * @property Paitents[] $paitents6
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'rank', 'value'], 'required'],
            [['type'], 'string'],
            [['rank'], 'integer'],
            [['value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'value' => 'Value',
            'rank' => 'Rank',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaitents()
    {
        return $this->hasMany(Patients::className(), ['hb_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaitents0()
    {
        return $this->hasMany(Patients::className(), ['bp_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaitents1()
    {
        return $this->hasMany(Patients::className(), ['creatinine_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaitents2()
    {
        return $this->hasMany(Patients::className(), ['platelets_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaitents3()
    {
        return $this->hasMany(Patients::className(), ['pulse_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaitents5()
    {
        return $this->hasMany(Patients::className(), ['urea_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaitents6()
    {
        return $this->hasMany(Patients::className(), ['wcc_id' => 'id']);
    }
    
    /**
     * @return Array
     */
    public static function getSettingsByCode() {
        $models = Settings::find()->orderBy('rank')
            ->asArray()
            ->all();
        $settings = array();
        foreach($models as $setting) {
            if(!isset($settings[$setting['type']])) {
                $settings[$setting['type']] = [];
            }
            $settings[$setting['type']][$setting['id']] = $setting['value'];
        }
        
        return $settings;
    }
}
