ALTER TABLE
	ned.dbo.coordination_code 
	ADD 
	color_code VARCHAR(16) NULL;


INSERT INTO ned.dbo.coordination_code (code,code_order,color,color_code) VALUES 
('C1','1','red','#ff1f01')
,('M1','1','red','#ff1f01')
,('C2','2','orange','#ff7d01')
,('M2','2','orange','#ff7d01')
,('C3','3','yellow','#f9ea00')
,('M3','3','yellow','#f9ea00')
,('C4','4','green','#83ce01')
,('M4','4','green','#83ce01')
,('C5','5','blue','#01b4d5')
,('M5','5','blue','#01b4d5')
, ('NC','6','',NULL);



ALTER TABLE
	ned.dbo.patient_referral 
	ADD 
	dob date NULL,
        address VARCHAR(124) NULL,
        admission_date date NULL,
        subject VARCHAR(64) NULL;

