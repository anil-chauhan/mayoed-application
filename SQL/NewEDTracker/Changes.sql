-- To track the deletetion of a patient
ALTER TABLE
NewEdTracker.dbo.Patients 
ADD is_deleted smallint DEFAULT ('0');

-- To 

ALTER TABLE 
       NewEDTracker.dbo.Patients
       ADD comment varchar(512) DEFAULT (NULL);


CREATE TABLE NewEDTracker.dbo.coordination_code (
	id int NOT NULL IDENTITY(1,1),
	code varchar(32) DEFAULT (NULL),
	code_order varchar(8) DEFAULT (NULL),
	color varchar(32) DEFAULT (NULL),
	color_code varchar(16),
	CONSTRAINT PK__coordina__3213E83F66B23B01 PRIMARY KEY (id)
) go;


INSERT INTO NewEDTracker.dbo.coordination_code (code,code_order,color,color_code) VALUES 
('C1','1','red','#ff1f01')
,('M1','1','red','#ff1f01')
,('C2','2','orange','#ff7d01')
,('M2','2','orange','#ff7d01')
,('C3','3','yellow','#f9ea00')
,('M3','3','yellow','#f9ea00')
,('C4','4','green','#83ce01')
,('M4','4','green','#83ce01')
,('C5','5','blue','#01b4d5')
,('M5','5','blue','#01b4d5')
,('NC','6','',NULL);




-- New added

ALTER TABLE
NewEdTracker.dbo.Patients 
ADD is_triage smallint DEFAULT ('0');


ALTER TABLE
NewEdTracker.dbo.Logs 
ADD PASIncidentDetailsSeqNumber int DEFAULT (NULL); 
