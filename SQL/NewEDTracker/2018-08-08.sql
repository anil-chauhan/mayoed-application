CREATE TABLE [NewEDTracker].[dbo].[Patients](
	[Patient_id] [int] IDENTITY(1,1) NOT NULL,
	[patient_name] [varchar](50) NULL,
	[episode_number] [varchar](50) NULL,
	[board_number] [varchar](50) NULL,
	[chart_number] [varchar](50) NULL,
	[surname] [varchar](50) NULL,
	[forename] [varchar](50) NULL,
	[age] [int] NULL,
	[sex] [varchar](3) NULL,
	[dob] [datetime] NULL,
	[address] [varchar](250) NULL,
	[PhoneNumber] [varchar](30) NULL,
	[Mobile] [varchar](30) NULL,
	[coordination_code] [varchar](5) NULL,
	[AEModeOfArrivalCode] [varchar](5) NULL,
	[ComplaintSiteSide] [varchar](50) NULL,
	[PresentComplaint] [varchar](50) NOT NULL,
	[IncidentDetailsScrnNotesLine1] [varchar](150) NOT NULL,
	[IncidentDetailsScrnNotesLine2] [varchar](150) NOT NULL,
	[ReferringGPCode] [varchar](8) NOT NULL,
	[ReferringGPName] [varchar](70) NOT NULL,
	[GPPracticeAddrLine1] [varchar](50) NOT NULL,
	[GPPracticeAddrLine2] [varchar](50) NOT NULL,
	[GPPracticeAddrLine3] [varchar](50) NOT NULL,
	[GPPracticeAddrLine4] [varchar](50) NOT NULL,
	[GPPracticePhoneNum] [varchar](30) NOT NULL,
	[NextOfKinName] [varchar](50) NOT NULL,
	[NextOfKinAddrLine1] [varchar](50) NOT NULL,
	[NextOfKinAddrLine2] [varchar](50) NOT NULL,
	[NextOfKinAddrLine3] [varchar](50) NOT NULL,
	[NextOfKinAddrLine4] [varchar](50) NOT NULL,
	[NextOfKinPhone] [varchar](30) NOT NULL,
	[NextOfKinWorkPhone] [varchar](30) NOT NULL,
	[NextOfKinRelationToPatientCode] [varchar](5) NOT NULL,
	[TimeSinceRegistration] [varchar](7) NULL,
	[ed_location] [int] NULL,
	[ed_doctor] [int] NULL,
	[ed_nurse] [int] NULL,
	[referral_type] [int] NULL,
	[ed_stream] [int] NULL,
	[is_mau] [smallint] NULL,
	[is_awaiting_admission] [smallint] NULL,
	[is_referral] [smallint] NULL,
	[is_discharge] [smallint] NULL,
	[is_bed_allocated] [smallint] NULL,
	[is_admitted] [smallint] NULL,
	[is_rpat] [smallint] NULL,
	[current_ward_code] [varchar](50) NULL,
	[current_bed_code] [varchar](50) NULL,
	[cons_name_current] [varchar](50) NULL,
	[Pulse] [varchar](5) NOT NULL,
	[Respiration] [varchar](4) NOT NULL,
	[BloodPressure] [varchar](9) NOT NULL,
	[Temperature] [varchar](7) NOT NULL,
	[HeadInjuryObservation] [varchar](5) NOT NULL,
	[BM] [varchar](7) NOT NULL,
	[CoordinationCategory] [varchar](5) NULL,
	[FilterCode] [varchar](50) NULL,
	[infection_status] [varchar](50) NULL,
	[PASAEAttendanceDate] [datetime] NOT NULL,
	[PASCoordinatedDate] [datetime] NOT NULL,
	[PASAEDischargeDate] [datetime] NOT NULL,
	[PASAEDischargeCode] [varchar](5) NULL,
	[PASAEDischargeUserDetails] [varchar](50) NULL,
	[PASAEDischarge] [varchar](30) NULL,
	[Tracker_timer] [datetime] NULL,
	[Tracker_time_of_triage] [datetime] NULL,
	[Tracker_awaiting_bed_time] [datetime] NULL,
	[Tracker_allocated_bed_time] [datetime] NULL,
	[Tracker_admission_ed_time] [datetime] NULL,
	[Tracker_rpat_time] [datetime] NULL,
	[Tracker_discharge_time] [datetime] NULL,
	[created_at] [datetime] NULL,
	[updated_at] [datetime] NULL,
 CONSTRAINT [PK__consulta__3213E83F549606B5] PRIMARY KEY CLUSTERED 
(
	[Patient_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_is_mau]  DEFAULT ((0)) FOR [is_mau]
GO

ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_is_awaiting_admission]  DEFAULT ((0)) FOR [is_awaiting_admission]
GO

ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_is_referral]  DEFAULT ((0)) FOR [is_referral]
GO

ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_is_discharge]  DEFAULT ((0)) FOR [is_discharge]
GO

ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_bed_requested]  DEFAULT ((0)) FOR [is_bed_allocated]
GO

ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_is_admitted]  DEFAULT ((0)) FOR [is_admitted]
GO

ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_is_rpat]  DEFAULT ((0)) FOR [is_rpat]
GO

ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_created_at]  DEFAULT (getdate()) FOR [created_at]
GO

ALTER TABLE [dbo].[Patients] ADD  CONSTRAINT [DF_Patients_updated_at]  DEFAULT (getdate()) FOR [updated_at]
GO












CREATE TABLE NewEDTracker.dbo.logs (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
        board_number varchar(16) DEFAULT (NULL),
	episode_number varchar(16) DEFAULT (NULL),
        patient_id int DEFAULT (NULL),
	user_id int DEFAULT (NULL),
	accesspass_id int DEFAULT (NULL),
        code_name varchar(64) DEFAULT (NULL),
        log_type int DEFAULT (NULL),
	replace_old varchar(200) DEFAULT (NULL),
	replace_new varchar(200) DEFAULT (NULL),
	comment varchar(250) DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	
) go;


CREATE TABLE NewEDTracker.dbo.accesspass (
	Accesspass_id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	name varchar(255) DEFAULT (NULL),
	password varchar(50) DEFAULT (NULL),
	is_active int DEFAULT ('1') NOT NULL,
	user_id int DEFAULT (NULL),
) go;


CREATE TABLE NewEDTracker.dbo.ed_location (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	name varchar(255) DEFAULT (NULL),
	priority int DEFAULT (NULL),
 	location_type smallint DEFAULT ('1'),
	location_order smallint DEFAULT (NULL),
	location_column_order smallint DEFAULT (NULL),
	location_column_suborder smallint DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
        alter_user_id int DEFAULT (NULL),
) go;

CREATE TABLE NewEDTracker.dbo.ed_doctor (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	name varchar(255) DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
        alter_user_id int DEFAULT (NULL),
) go;  


CREATE TABLE NewEDTracker.dbo.ed_nurse (
        id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	name varchar(255) DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
        alter_user_id int DEFAULT (NULL),
) go;

CREATE TABLE NewEDTracker.dbo.stream_type (
        id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
        code varchar(16) DEFAULT (NULL),
	name varchar(255) DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
        alter_user_id int DEFAULT (NULL),
) go;

CREATE TABLE NewEDTracker.dbo.referral_type (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	code varchar(16) NOT NULL,
        name varchar(64) NOT NULL,
        created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
        alter_user_id int DEFAULT (NULL),
) go;


CREATE TABLE NewEDTracker.dbo.LogTypes (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
        LogTypeID varchar(8) DEFAULT (NULL),
        LogTypeDescription varchar(32) DEFAULT (NULL)
) go;

INSERT INTO NewEDTracker.dbo.LogTypes (LogTypeID,LogTypeDescription) VALUES
('0',  'Doctor'),
('1',  'Location'), 
('2',  'Nurse'),
('3',  'Stream'), 
('4',  'Comment'),
('5',  'Discharge'),
('6',  'Admisson'),
('7',  'Ward'),
('8',  'RPAT'),
('9',  'Referral'),
('10', 'Breach'),
('11', 'CoOrdination'),
('12', 'Pulse'), 
('13', 'Respiration'),
('14', 'BloodPressure'),
('15', 'Temperature'),
('16', 'PreAdmission'),
('17', 'BedAllocation');    


CREATE
	TABLE
		NewEDTracker.dbo.patient_referral ( id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
		patient_id int DEFAULT ( NULL ),
		referral_type int DEFAULT ( NULL ),
		patient_name varchar( 64 ) DEFAULT ( NULL ),
		board_number varchar( 16 ) DEFAULT ( NULL ),
		episode_number varchar( 16 ) DEFAULT ( NULL ),
		code varchar( 16 ) DEFAULT ( NULL ),
		sex varchar( 8 ) DEFAULT ( NULL ),
		age int DEFAULT ( NULL ),
		comment varchar( 255 ) DEFAULT ( NULL ),
		dob datetime NULL,
		address varchar( 250 ) NULL,
		Tracker_admission_ed_time datetime NULL,
		present_complaints varchar( 255 ) NULL,
                created_at datetime NULL, 
                updated_at datetime NULL );


CREATE TABLE NewEDTracker.dbo.mail_recipients (
	id int NOT NULL IDENTITY(1,1),
	name varchar(64) DEFAULT (NULL),
	email varchar(64) DEFAULT (NULL),
	CONSTRAINT PK__mail_rec__3213E83F2DBDF88A PRIMARY KEY (id)
);


INSERT
	INTO
		NewEDTracker.dbo.mail_recipients (
		name,
		email )
	VALUES (
	'Enda Furey',
	'enda.furey@twdc.ie' ) ,
	(
	'Molloy, Martin (UHG Information Services)',
	'Martin.Molloy@hse.ie' ) ,
	(
	'Murphy, Martin (Technology Manager GUH)',
	'Martin.Murphy@hse.ie' ) ,
	(
	'Richard Malone',
	'richard.malone@hse.ie' ) ,
	(
	'Ryder, Caroline, UHG',
	'Caroline.Ryder@hse.ie' ) ,
	(
	'O''Brien, Deirdre, GUH, CNM 3, ED /AMU',
	'DeirdreS.OBrien@hse.ie' ) ,
	(
	'Binchy, Mr. James, GUH',
	'james.binchy@hse.ie' ) ,
	(
	'ODonnell, John J (Consultant Emergency Medicine, UCHG)',
	'johnj.odonnell@hse.ie' ) ,
	(
	'Grehan, Cepta, UCHG',
	'cepta.grehan@hse.ie' ) ,
	(
	'Doyle, Trish, UHG',
	'Trish.Doyle@hse.ie' ) ,
	(
	'Chris Kane',
	'chris.kane@hse.ie' ) ,
	(
	'Marie Burns',
	'marie.burns2@hse.ie' ) ,
	(
	'Ann Cosgrove',
	'ann.cosgrove@hse.ie' ) ,
	(
	'Jeet Kalsi',
	'jeet.kalsi@hse.ie' ) ,
	(
	'Elaine Dobell',
	'Elaine.dobell@hse.ie' ) ,
	(
	'Enda Daly',
	'enda.daly@hse.ie' ) 
;

       
