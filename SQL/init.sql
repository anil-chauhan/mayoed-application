CREATE TABLE ned.dbo.[user] (
	id int NOT NULL IDENTITY(1,1),
	firstname varchar(255) DEFAULT (NULL),
	lastname varchar(255) DEFAULT (NULL),
	email varchar(255) DEFAULT (NULL),
	auth_key varchar(32) DEFAULT (NULL),
	password_hash varchar(255) DEFAULT (NULL),
	password_reset_token varchar(255) DEFAULT (NULL),
	[type] varchar(128) DEFAULT ('user') NOT NULL,
	status smallint DEFAULT ((10)) NOT NULL,
	created_at int NOT NULL,
	updated_at int NOT NULL,
	CONSTRAINT PK__user__3213E83FD75D2AF8 PRIMARY KEY (id)
);

INSERT INTO ned.dbo.[user] ( 
            firstname,
            lastname,
            email,
            auth_key,
            password_hash,
            password_reset_token,
            [type],
            status,
            created_at,
            updated_at 
        )
	VALUES ( 
            'Demo',
            'User',
            'abc@gmail.com',
            'Dsv0g-nuQ8uazcUS2nLb_mslin14wZ0D',
            '$2y$13$wXhXdYnXgcKPSo6My.ghru1EuJYLUCe3Srtsm8OcyLn2rtZsvA/Cm',
            NULL,
            'super',
            10,
            1433311181,
            1492503474 
        ) ,
	( 
            'Test',
            'test',
            'test@gmail.com',
            'lrRhgpxjTJobRxHujNzGGb2gvrGmFef-',
            '$2y$13$O6daRBG0FBo0nz81RHdEmu9iDTLmiFUz/GNdDdEvgDloPegitoEPe',
            NULL,
            'super',
            10,
            1528269313,
            1528269337 
        ) ;