
CREATE TABLE ned.dbo.ed_location (
	id int NOT NULL IDENTITY(1,1),
	name varchar(255) DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
	alter_user_id int DEFAULT (NULL),
	priority int DEFAULT (NULL),
	location_type smallint DEFAULT ('1'),
	location_order smallint DEFAULT (NULL),
	location_column_order smallint DEFAULT (NULL),
	location_column_suborder smallint DEFAULT (NULL),
	CONSTRAINT PK__ed_locat__3213E83FAFEA8ABA PRIMARY KEY (id)
) go;

CREATE TABLE ned.dbo.ed_doctor (
	id int NOT NULL IDENTITY(1,1),
	name varchar(255) DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
	alter_user_id int DEFAULT (NULL),
	CONSTRAINT PK__ed_docto__3213E83FEDCBB109 PRIMARY KEY (id)
) go;   

CREATE TABLE ned.dbo.ed_nurse (
	id int NOT NULL IDENTITY(1,1),
	name varchar(255) DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
	alter_user_id int DEFAULT (NULL),
	CONSTRAINT PK__ed_nurse__3213E83F2F714EF8 PRIMARY KEY (id)
) go;

CREATE TABLE ned.dbo.accesspass (
	id int NOT NULL IDENTITY(1,1),
	nurse_name varchar(255) DEFAULT (NULL),
	password varchar(50) DEFAULT (NULL),
	is_active int DEFAULT ('1') NOT NULL,
	user_id int DEFAULT (NULL),
	CONSTRAINT PK__accesspa__3213E83F959F88D3 PRIMARY KEY (id)
) go;

CREATE TABLE ned.dbo.referral_type (
	id int NOT NULL IDENTITY(1,1),
	code varchar(16) NOT NULL,
	name varchar(64) NOT NULL,
	CONSTRAINT PK__referral__3213E83F40A0FC93 PRIMARY KEY (id)
) go;
