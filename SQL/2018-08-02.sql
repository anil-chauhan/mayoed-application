CREATE TABLE ned.dbo.ed_stream (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	name varchar(64) DEFAULT (NULL),
	value varchar(32) DEFAULT (NULL),  
        color varchar(32) DEFAULT (NULL),  
) go;



CREATE TABLE ned.dbo.ews (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	code varchar(32) DEFAULT (NULL),
	value varchar(32) DEFAULT (NULL),  
) go;


INSERT INTO ned.dbo.ews (code,value) VALUES
('0',  '0' ),
('1',  '1' ), 
('2',  '2' ),
('3',  '2*'), 
('4',  '3' ),
('5',  '3*'),
('6',  '4' ),
('7',  '5' ),
('8',  '6' ),
('9',  '7' ),
('10', '8' ),
('11', '9' ),
('12', '10'), 
('13', '11');

CREATE TABLE ned.dbo.log_type (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
        log_id varchar(8) DEFAULT (NULL),
	value varchar(32) DEFAULT (NULL),
) go;


INSERT INTO ned.dbo.log_type (log_id,value) VALUES
('0',  'Doctor' ),
('1',  'Location' ), 
('2',  'Nurse' ),
('3',  'Stream'), 
('4',  'Comment' ),
('5',  'Discharge'),
('6',  'Admisson' ),
('7',  'Ward' ),
('8',  'RPAT' ),
('9',  'Referral' ),
('10', 'Breach' ),
('11', 'CoOrdination' ),
('12', 'Pulse'), 
('13', 'Respiration'),
('14', 'BloodPressure'),
('15', 'Temperature'),
('16', 'PreAdmission'),
('17', 'BedAllocation');    

CREATE TABLE ned.dbo.logs (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	user_id int DEFAULT (NULL),
	patient_id int DEFAULT (NULL),
	bunch_id int DEFAULT (NULL),
	log_type int DEFAULT (NULL),
	last_value varchar(512) DEFAULT (NULL),
	updated_value varchar(512) DEFAULT (NULL),
	description varchar(64) DEFAULT (NULL),
	patient_name varchar(64) DEFAULT (NULL),
	board_number varchar(16) DEFAULT (NULL),
	episode_number varchar(16) DEFAULT (NULL),
	admission_date date DEFAULT (NULL),
	code_name varchar(64) DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
) go;

INSERT INTO ned.dbo.ed_stream (name, value, color) VALUES
('resus', 'Resus/Major', 'red'),
('triage', 'RAN/RAT', 'Ambulatory Care'),
('minor_injury', 'Minor Injury', 'red'),
('paediatrics', 'Paeds ED', 'red');
