CREATE TABLE ned.dbo.patient_referral (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	patient_id int DEFAULT (NULL),
	referral_type int DEFAULT (NULL),
	patient_name varchar(64) DEFAULT (NULL),
	board_number varchar(16) DEFAULT (NULL),
	episode_number varchar(16) DEFAULT (NULL),
	code varchar(16) DEFAULT (NULL),
        sex varchar(8) DEFAULT (NULL),
        age varchar(8) DEFAULT (NULL),
        comment varchar(255) DEFAULT (NULL),
	created_at int DEFAULT (NULL),
	updated_at int DEFAULT (NULL),
) go;


SELECT * FROM [patients] WHERE [Patient_id] IN 
(
    SELECT max(Patient_id) FROM [patients] 
    WHERE 
        ( [PASAEDischargeDate] = '1900-01-01 00:00:00.000' )
     AND 
        (
            (
                ([PASLogoutDate] IS NULL) 
                OR 
                ([PASLogoutDate]='1900-01-01 00:00:00.000')
            ) 
        )
     AND 
        (
            PASAEAttendanceDate > CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21) 
            OR 
            (Not( [coordination_code] = '' ) )
        ) 
        GROUP BY [board_number], [episode_number]
)
SELECT * FROM [patients] WHERE [Patient_id] IN 
(
SELECT max(Patient_id) FROM [patients]
    WHERE 
        ([PASAEDischargeDate]='1900-01-01 00:00:00.000') 
    AND 
        (
            ([PASLogoutDate] IS NULL) 
            OR 
            ([PASLogoutDate]='1900-01-01 00:00:00.000')
        ) 
    AND 
        (
            PASAEAttendanceDate > CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21) 
            OR (Not( [coordination_code] = '' ) )
        )
    GROUP BY [board_number], [episode_number]
)