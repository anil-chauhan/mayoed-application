CREATE TABLE ned.dbo.mail_recipients (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	name varchar(64) DEFAULT (NULL),
	email varchar(64) DEFAULT (NULL),
) go;


CREATE TABLE ned.dbo.coordination_code (
	id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
	code varchar(32) DEFAULT (NULL),  
	code_order varchar(8) DEFAULT (NULL),  
        color varchar(32) DEFAULT (NULL),  
) go;


SELECT 
* 
FROM [patients] 
WHERE 
    ([PASAEDischargeDate]='1900-01-01 00:00:00.000') 
AND (
        (
            ([PASLogoutDate] IS NULL) 
            OR 
            ([PASLogoutDate]='1900-01-01 00:00:00.000')
        )
        OR (
            PASIncidentDetailsSeqNumber > 1
        )
    ) 
AND (
    PASAEAttendanceDate > CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21)
    OR (
        Not( [coordination_code] = '' ) 
        AND PASAEAttendanceDate < CONVERT(varchar, DATEADD(minute, -180, GETDATE()),21)
    )
)