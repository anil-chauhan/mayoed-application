<?php

namespace console\controllers;

use yii; 
use yii\console\Controller;
use yii\helpers\Console;
use common\components\SPHelper;
use yii\helpers\ArrayHelper;
use common\models\Patient;
use common\models\MailRecipient;
use common\models\Logs;
use common\models\CoordinationCode;
use common\models\EdLocation;


class CronController extends Controller {

    public function actionCreate()
    {
        $patientBedStatus = SPHelper::getBedStatus();
        $patientDatas = SPHelper::filterEdSP();
       
        $modelPatients = Patient::getActivePatients('board');
         
        //To save the new patient from SP to Ed Database
        foreach ($patientDatas as $key => $patient) {
       
            $model = new Patient();
            $categoryCode = trim($patient['CoordinationCategoryCode']);
            $model->coordination_code_order = CoordinationCode::getCoordinationCodeOrder($categoryCode);

            //Checking patient exist in local patient db or not & If not exist in db save the patient
            if (!isset($modelPatients[$patient['BoardNum']])) {
                
                $model = $this->fillModelFromSp($model, $patient);
                $current_age = $this->calculateAge($model->dob);
                
                if ( strpos($categoryCode , 'M') !== 0) {
                    if ($model->ed_stream == 'paediatrics') {
                        $model->is_paed_minor = 1;
                    } else {
                        $model->ed_stream = 'minor_injury';
                    }
                } 
                if ($model->coordination_code == 'C1' && $current_age > 16) {
                    $model->ed_stream = 'resus';
                }

                $model->save();
                //Updating Local Db Array
                $modelPatients[$patient['BoardNum']] = $patient['BoardNum'];
            }
       } 
 
        //Removal of Patients from local db which are not existing in current SP and updating existing patient bed status
        $models =  Patient::getActivePatients();

        if (!empty($models)) {  
            foreach ($models as $model) {
                //For awaiting admission (Patient Bed Status)
                $key = array_search($model->board_number, array_column($patientBedStatus, 'BoardNum'));
                if ($key !== false) { 
                    $patientBed = $patientBedStatus[$key];
                    
                    $logModel = new Logs();
                    $logModel->last_value = $model->current_ward_code . ' ' . $model->current_bed_code;
                    $model->current_ward_code = $patientBed['WardCode'];
                    $model->current_bed_code = $patientBed['RoomBed'];

                    $logModel->updated_value = $model->current_ward_code . ' ' . $model->current_bed_code;

                    $flagLog = false;
                    
                    $bedStatus = $patientBed['Bed Status'];
                    $bedData = $this->getBedData($bedStatus);
                    
                    $model->bed_status = $bedData['bed_status'];
                        
                    if (strpos($patientBed['Bed Status'], 'Awaiting Bed') !== false) {
                        
                        $model->is_awaiting_admission = 1; //setting flag in db for the patient is waiting for bed

                        if ($model->current_ward_code !== $patientBed['WardCode']) {
                            
                            $logModel->log_type = Yii::$app->params['logType']['admission'];
                            $flagLog = true;
                        }

                        $model->cons_name_current = $patientBed['ConsultantCode'];

                       
                        if ($bedData['bed_time']) {
                            $model->bed_awaiting_time = $bedData['bed_time'];
                        } 
                     } else {  

                        if ($model->current_ward_code !== $patientBed['WardCode']) {
                            
                            $logModel->log_type = Yii::$app->params['logType']['ward'];
                            $flagLog = true;
                        }


                        if ($bedData['bed_time']) {
                            $model->bed_time = $bedData['bed_time'];
                        }
                    }
                    if($flagLog){
                        Logs::add($logModel, $model);
                    }
                    $model->save();
                }   

                $keyPatient = array_search($model->board_number, array_column($patientDatas, 'BoardNum'));
                if ($keyPatient === false) {
                    //Delete patient if not exist in store procedure
                    $model->sp_discharge_time = strtotime('now');
                    
                    $model->is_deleted = 1;
                    $model->save();
                } else {
                    $patient = $patientDatas[$keyPatient];
                    $categoryCode = trim($patient['CoordinationCategoryCode']);
                    
                    if (!empty(trim($patient['CoordinatedDate']))) {
                        $model->coordinate_date = strtotime($patient['CoordinatedDate']);
                    }
                    if (!(trim($model->coordination_code) == $categoryCode) && !empty($categoryCode)) {

                        $model->coordination_code = $categoryCode;
                        if ($model->is_triage) {
                            $model->is_triage = 0;
                            if (!empty($categoryCode)) {
                                if (in_array($categoryCode, Yii::$app->params['coordinationCodeOrderPaedMinor'])) {
                                    if ($model->ed_stream == 'paediatrics') {
                                        $model->is_paed_minor = 1;
                                    } else {
                                        $model->ed_stream = 'minor_injury';
                                    }
                                }
                            }
                        }
                        $model->coordination_code_order = CoordinationCode::getCoordinationCodeOrder($categoryCode);
                    }
                    $model->save();
                }
            }
        }

        return true;
    }
    public function getBedData($bedStatus){
            $data = [
                'bed_time'   => '',
                'bed_status'   => '',
            ];
            preg_match('#\((.*?)\)#', $bedStatus, $matches);

            if (strpos($matches[1], '#') === false) {
                $now = strtotime('now');
                $exp_time = explode(':', $matches[1]);
                $data['bed_time'] = $now - $exp_time[0] * 60 * 60 - $matches[1] * 60;
            }
            $status = preg_split('/\(.*?\)/', $bedStatus);
            $data['bed_status'] = $status[0];
    }
    
    public function fillModelFromSp($model, $patient){
           
            $model->board_number = $patient['BoardNum'];
            $model->episode_number = $patient['EpisodeNum'];
            $model->patient_name = $patient['Forename'].' '. $patient['Surname'];
            $model->surname = $patient['Surname'];
            $model->forename = $patient['Forename'];
            $model->sex = $patient['Sex'];
            $model->dob = $patient['DateOfBirth'];
            $model->address = $patient['AddrLine1'] . ' ' . $patient['AddrLine2'] . ' ' . $patient['AddrLine3'] . ' ' . $patient['AddrLine4'];
            $model->age = $patient['AGE'];
            $model->coordination_code = trim($patient['CoordinationCategoryCode']);
            $model->infection_status = SPHelper::getInfectionStatus($patient['INF'].$patient['INH'].$patient['PASH']);
            $model->phone = $patient['Phone'];
            $model->mobile_number = $patient['MobileNum'];
            $model->admission_date = date('d-m-Y');
            $model->ed_location = EdLocation::getWaitingRoom();
            $model->is_triage = (($model->coordination_code === '')) ? 1 : 0;
            $model->timer = strtotime($patient['AEAttendanceDate']);
            $model->ae_mode_of_arrival = $patient['AEModeOfArrivalCode'];
            $model->register_with = $patient['PresentComplaint'];
            $model->sp_object = json_encode($patient);
            return $model;
    }
   
    public function calculateAge($dob){
        //age calculation
        $current = strtotime("now");
        $dob = strtotime($dob);

        $val = (int) abs(($current - $dob) / (60 * 60 * 24 * 30));
        $current_age = $val / 12;
    }  
    
    
    // To send updation mail
    public function actionSendMail() {
        try {
            $data = Patient::countMailInfoEd(); 
            $recipients = MailRecipient::find()->asArray()->all();          
            $html = $this->renderPartial('mail-template2',['data' => $data]);
            $i =1;
            foreach($recipients as $patient) {
                Yii::$app->mailer->compose()
                  ->setFrom('edtracker@hse.ie')
                  ->setTo($patient['email'])
                  ->setSubject("Current ED Statistics ")
                  ->setHtmlBody($html)
                  ->send();
                echo $i++;
            }  
            echo 'Mail Send';exit;
        } catch (InvalidParamException $e) {
                echo 'Error';exit();
            throw new BadRequestHttpException($e->getMessage());
        }        
  } 

// To send updation mail
    public function actionTestMail() {
        try{
            $data = Patient::countMailInfoEd(); 
            $html = $this->renderPartial('mail-template2',['data' => $data]);
            Yii::$app->mailer->compose()
              ->setFrom('edtracker@hse.ie')
              ->setTo('enda.furey@twdc.ie')
              ->setSubject("Current ED Statistics ")
              ->setHtmlBody($html)
              ->send();
            echo 'Mail Send';exit;
        } catch (InvalidParamException $e) {
                echo 'Error';exit();
            throw new BadRequestHttpException($e->getMessage());
        }        
   } 

// To remove patients that are deleted from SP
    public function actionDelete() {
        
        $patientDatas = SPHelper::getAllEdPatients();
        
       //Removal of Patients from local db which are not existing in current SP and updating existing patient bed status
        $models = Patient::find()->all();
        
        if (!empty($models)) {  
            foreach ($models as $model) {
                $keyPatient = array_search($model->board_number, array_column($patientDatas, 'BoardNum'));
                if ($keyPatient === false) {
                    //Delete patient if not exist in store procedure
                    $model->is_deleted = 1;
                    $model->save();
                }
             }
         }
    }
    
}
