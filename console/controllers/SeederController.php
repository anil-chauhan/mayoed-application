<?php

namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Console;

class SeederController extends Controller {

    // Action to fill accesspass table
    public function actionAccessPassTable() {
        $seeder = new \tebazil\yii2seeder\Seeder();

        $array = [
            [1, 'test1', '12344', 1],
            [2, 'Enda Furey', '1234', 1],
            [3, 'Sile Kelly', '3456', 1],
            [4, 'test account', '7564', 1],
            [5, 'test account', '7564', 1],
            [6, 'Ann Sloyan', '5484', 1],
            [7, 'Imelda Mathews', '4355', 1],
            [8, 'Margaret McDonagh', '2376', 1],
            [9, 'Brid OHalloran', '7678', 1],
            [10, 'Sosamma John Mathew', '7231', 1],
            [11, 'A Menor', '5629', 1],
            [12, 'N Tumulos', '4523', 1],
            [13, 'Clare Nolan', '1759', 1],
            [14, 'Terri Ryan', '3597', 1],
        ];

        $columnConfig = ['id', 'nurse_name', 'password', 'is_active'];

        $seeder->table('accesspass')->data($array, $columnConfig)->rowQuantity(count($array));

        $seeder->refill();

        $message = $this->ansiFormat('Accesspass table data seeding successful', Console::FG_GREEN);

        echo "\n" . $message . "\n";
        return 0;
    }
    
    // Action to fill user table
    public function actionUserTable() {
        $seeder = new \tebazil\yii2seeder\Seeder();

        $array = [
            [1, 'Demo', 'User', 'abc@gmail.com', '123', NULL, 'asdas', NULL, NULL, '', '0ton96B1B9q39YIzW__YtsVNTzNzHwHn', '$2y$13$fth3tGDDf2f8Naxp544HhuQAzZWZ0ipzQdwxs8d88dpcf5E5A4gRK', NULL, 'super', 10, 1433311181, 1433311181],
            [2, 'ABC', 'LMN', 'abc1@gmail.com', 'asd', NULL, 'dsf', NULL, NULL, '', '2HEnf21UFa6zaCiVeUZqYwFLL6dkK_vh', '$2y$13$lfDh77RximorQpeF6SKwsuiQDcMklpxQK0az5hRrmFKNw/mf2htQS', NULL, 'user', 10, 1433423142, 1433423142],
            [3, 'Test', 'Test', 'test@gmail.com', NULL, NULL, NULL, NULL, NULL, '', 'd2OD_P9l8lMAMP9fXOOuO92Oair_w6JP', '$2y$13$f/H1n1.U1.IQDVIoPny9.OXHBsIqbNbXTkO3oDhKLsZHH0wPaY/B2', NULL, 'user', 10, 1500361494, 1500361494],
        ];

        $columnConfig = ['id', 'firstname', 'lastname', 'email', 'address1', 'address2' ,'county', 'phone', 'dateofbirth', 'username', 'auth_key', 'password_hash', 'password_reset_token', 'type', 'status', 'created_at', 'updated_at'];

        $seeder->table('user')->data($array, $columnConfig)->rowQuantity(count($array));

        $seeder->refill();

        $message = $this->ansiFormat('User table data seeding successful', Console::FG_GREEN);

        echo "\n" . $message . "\n";
        return 0;
    }

}
