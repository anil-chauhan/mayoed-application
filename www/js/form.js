var currentElement = null;

function tgForm(element){
     currentElement = element;
     var requiredFiels = $(element).data('required');
     if(requiredFiels){
         
         if(requiredFiels == 'coordinationCode'){
             flag = true;
             if(!$('#patient-coordination_code').val()){
                $('.field-patient-coordination_code').addClass('has-error').find('.help-block').html('Please Select Coordination code');
                flag = false;
            } else {
                $('.field-patient-coordination_code').removeClass('has-error').find('.help-block').html('');
                //$('#patient_name').html($('#handover-patient_name').val());
            }
            
            if(flag)  
             document.getElementById("save-coordination-form").submit();
         }
     }
}


function toggleForm(element){
   
    customValidator(element);
}

function validatorCallBack(element, validate){
    if(validate) {
        var showId = $(element).data('show_id');
        $('.toggle').addClass('hide');
        
        $('#'+showId).removeClass('hide');
       
    }
}

function customValidator(element) {
//    alert($(element).data('show_id'));
    currentElement = element;
    var requiredFiels = $(element).data('required');
    if(requiredFiels) {
        
        if(requiredFiels == 'discharge-patient') {
            flag = true;
            
            if(!$('#consultant-discharge_patient_name').val()){
                $('.field-consultant-discharge_patient_name').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-discharge_patient_name').removeClass('has-error').find('.help-block').html('');
                $('#patient_name').html($('#consultant-discharge_patient_name').val());
                $('#discharge-patient_name').html($('#consultant-discharge_patient_name').val());
            }
            if(!$('#consultant-discharge_admission_date').val()){
                $('.field-consultant-discharge_admission_date').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-discharge_admission_date').removeClass('has-error').find('.help-block').html('');
                $('#admission_date').html($('#consultant-discharge_admission_date').val());
                $('#discharge-admission_date').html($('#consultant-discharge_admission_date').val());
            }
            if(!$('#consultant-discharge_dob').val()){
                $('.field-consultant-discharge_dob').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-discharge_dob').removeClass('has-error').find('.help-block').html('');
                $('#discharge-dob').html($('#consultant-discharge_dob').val());
            }
            if(!$('#consultant-discharge_cons_name_current').val()){
                $('.field-consultant-discharge_cons_name_current').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-discharge_cons_name_current').removeClass('has-error').find('.help-block').html('');
                $('#cons_name_current').html($('#consultant-discharge_cons_name_current').val());
                $('#discharge-consultant_name').html($('#consultant-discharge_cons_name_current').val());
            }
            if(!$('#consultant-discharge_address').val()){
                $('.field-consultant-discharge_address').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-discharge_address').removeClass('has-error').find('.help-block').html('');
                $('#discharge-address').html($('#consultant-discharge_address').val());
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'verify') {
            flag = true;

            if(!$('#emergency-code').val()){
                $('.field-emergency-code').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
				
                //if($('#emergency-code').val() != 'A21B') {
				if($('#checkcode').val() != '1') {
                    $('.field-emergency-code').addClass('has-error').find('.help-block').html('Enter correct code.');
                    flag = false;
                } 
                else { 
                    $('.field-emergency-code').removeClass('has-error').find('.help-block').html('');
                }
            }
    
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'patient') {
            flag = true;
            
            if(!$('#consultant-patient_name').val()){
                $('.field-consultant-patient_name').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-patient_name').removeClass('has-error').find('.help-block').html('');
                $('#patient_name').html($('#consultant-patient_name').val());
            }
            
            if(!$('#consultant-age').val()){
                $('.field-consultant-age').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-age').removeClass('has-error').find('.help-block').html('');
                $('#age').html($('#consultant-patient_name').val());
            }
            
             if(!$('#consultant-dob').val()){
                $('.field-consultant-dob').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-dob').removeClass('has-error').find('.help-block').html('');
                $('#dob').html($('#consultant-dob').val());
            }
            if(!$('#consultant-address').val()){
                $('.field-consultant-address').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-address').removeClass('has-error').find('.help-block').html('');
                $('#address').html($('#consultant-address').val());
            }
            if(!$('#consultant-admission_date').val()){
                $('.field-consultant-admission_date').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-admission_date').removeClass('has-error').find('.help-block').html('');
                $('#doa').html($('#consultant-admission_date').val());
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'patient-referral') {
            flag = true;
            
            if(!$('#patientreferral-patient_name').val()){
                $('.field-patientreferral-patient_name').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patientreferral-patient_name').removeClass('has-error').find('.help-block').html('');
                $('#patient_name-referral').html($('#patientreferral-patient_name').val());
            }
            
            if(!$('#patientreferral-age').val()){
                $('.field-patientreferral-age').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patientreferral-age').removeClass('has-error').find('.help-block').html('');
                $('#age-referral').html($('#patientreferral-patient_name').val());
            }
            
             if(!$('#patientreferral-dob').val()){
                $('.field-patientreferral-dob').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patientreferral-dob').removeClass('has-error').find('.help-block').html('');
                $('#dob-referral').html($('#patientreferral-dob').val());
            }
            if(!$('#patientreferral-address').val()){
                $('.field-patientreferral-address').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patientreferral-address').removeClass('has-error').find('.help-block').html('');
                $('#address-referral').html($('#patientreferral-address').val());
            }
            if(!$('#patientreferral-admission_date').val()){
                $('.field-patientreferral-admission_date').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patientreferral-admission_date').removeClass('has-error').find('.help-block').html('');
                $('#doa-referral').html($('#patientreferral-admission_date').val());
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'ed_patient') {
            flag = true;
            
            if(!$('#consultant-ed_patient_name').val()){
                $('.field-consultant-ed_patient_name').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_patient_name').removeClass('has-error').find('.help-block').html('');
                $('#ed_patient_name').html($('#consultant-ed_patient_name').val());
            }
            
            if(!$('#consultant-ed_age').val()){
                $('.field-consultant-ed_age').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_age').removeClass('has-error').find('.help-block').html('');
                $('#ed_age').html($('#consultant-ed_age').val());
            }
            
             if(!$('#consultant-ed_dob').val()){
                $('.field-consultant-ed_dob').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_dob').removeClass('has-error').find('.help-block').html('');
                $('#ed_dob').html($('#consultant-ed_dob').val());
            }
            if(!$('#consultant-ed_address').val()){
                $('.field-consultant-ed_address').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_address').removeClass('has-error').find('.help-block').html('');
                $('#ed_address').html($('#consultant-ed_address').val());
            }
            if(!$('#consultant-ed_admission_date').val()){
                $('.field-consultant-ed_admission_date').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_admission_date').removeClass('has-error').find('.help-block').html('');
                $('#ed_doa').html($('#consultant-ed_admission_date').val());
            }
            if(flag) validatorCallBack(element,  true);
        }
        
        if (requiredFiels == 'discharge-date') {
            flag = true;
            if(!$('#consultant-discharge_date').val()){
                $('.field-consultant-discharge_date').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-discharge_date').removeClass('has-error').find('.help-block').html('');
                $('#discharge_date').html($('#consultant-discharge_date').val());
            }
            if(!$('#consultant-discharge_comment').val()){
                $('.field-consultant-discharge_comment').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-discharge_comment').removeClass('has-error').find('.help-block').html('');
                $('#discharge-comment').html($('#consultant-discharge_comment').val());
            }
            

            if(flag) validatorCallBack(element,  true);
        }
        if (requiredFiels == 'reason') {
            flag = true;
            if(!$('#consultant-date_referred').val()){
                $('.field-consultant-date_referred').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-date_referred').removeClass('has-error').find('.help-block').html('');
                $('#date_referred').html($('#consultant-date_referred').val());
            }
            if(!$('#consultant-location option:selected').val()){
                $('.field-consultant-location').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-location').removeClass('has-error').find('.help-block').html('');
                $('#location').html($('#consultant-location option:selected').text());
            }
            if(!$('#consultant-referral_reason').val()){
                $('.field-consultant-referral_reason').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-referral_reason').removeClass('has-error').find('.help-block').html('');
                $('#referral_reason').html($('#consultant-referral_reason').val());
            }
            if(!$('#consultant-diagnosis').val()){
                $('.field-consultant-diagnosis').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-diagnosis').removeClass('has-error').find('.help-block').html('');
                $('#diagnosis').html($('#consultant-diagnosis').val());
            }
            
            

            if(flag) validatorCallBack(element,  true);
        }
        
        if(requiredFiels == 'summary') {
            flag = true;
            
            if(!$('#consultant-summary').val()){
                $('.field-consultant-summary').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-summary').removeClass('has-error').find('.help-block').html('');
                $('#summary').html($('#consultant-summary').val());
            }
            
            if(!$('#consultant-follow_up').val()){
                $('.field-consultant-follow_up').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-follow_up').removeClass('has-error').find('.help-block').html('');
                $('#follow_up').html($('#consultant-follow_up').val());
            }
            if(!$('#consultant-discharge_plan').val()){
                $('.field-consultant-discharge_plan').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-discharge_plan').removeClass('has-error').find('.help-block').html('');
                $('#discharge_plan').html($('#consultant-discharge_plan').val());
            }
            if(!$('#consultant-comment').val()){
                $('.field-consultant-comment').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-comment').removeClass('has-error').find('.help-block').html('');
                $('#comment').html($('#consultant-comment').val());
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'consultant') {
            flag = true;
            
            if(!$('#consultant-cons_name_current').val()){
                $('.field-consultant-cons_name_current').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-cons_name_current').removeClass('has-error').find('.help-block').html('');
                $('#cons_name_current').html($('#consultant-cons_name_current').val());
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'referralType') {
            flag = true;
            
             if(!$('#consultant-referral_type option:selected').val()){
                $('.field-consultant-referral_type').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-referral_type').removeClass('has-error').find('.help-block').html('');
                $('#referral_type').html($('#consultant-referral_type option:selected').text());
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'referralType-referral') {
            flag = true;
            
             if(!$('#patientreferral-referral_type option:selected').val()){
                $('.field-patientreferral-referral_type').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patientreferral-referral_type').removeClass('has-error').find('.help-block').html('');
                $('#referral_type').html($('#patientreferral-referral_type option:selected').text());
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'map-consultant') {
            flag = true;
            focusInputs();
            $('#ed_stream').html($('#consultant-ed_stream option:selected').text());
            $('#ed_doctor').html($('#consultant-ed_doctor option:selected').text());
            $('#ed_nurse').html($('#consultant-ed_nurse option:selected').text());
            $('#ed_location').html($('#consultant-ed_location option:selected').text());
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'replace-nurse') {
            flag = true;
            
            if(!$('#consultant-ed_nurse option:selected').val()){
                $('.field-consultant-ed_nurse').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_nurse').removeClass('has-error').find('.help-block').html('');
                $('#ed_nurse').html($('#consultant-ed_nurse option:selected').text());
                $('.replace-preview').addClass('hide');
                $('#div-nurse').removeClass('hide');
                
            }
            
            if(!$('#consultant-commentnurse').val()){
                $('.field-consultant-commentnurse').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-commentnurse').removeClass('has-error').find('.help-block').html('');
                $('#comment').html($('#consultant-comment').val());
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'replace-stream') {
            flag = true;
            
            if(!$('#consultant-ed_stream option:selected').val()){
                $('.field-consultant-ed_stream').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_nurse').removeClass('has-error').find('.help-block').html('');
                $('#ed_stream').html($('#consultant-ed_stream option:selected').text());
                $('.replace-preview').addClass('hide');
                $('#div-stream').removeClass('hide');
                
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'replace-comment') {
            flag = true;
            
            if(!$('#consultant-comment').val()){
                $('.field-consultant-comment').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-comment').removeClass('has-error').find('.help-block').html('');
                $('#replace-comment').html($('#consultant-comment').val());
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'replace-doctor') {
            flag = true;
            
            if(!$('#consultant-ed_doctor option:selected').val()){
                $('.field-consultant-ed_doctor').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_doctor').removeClass('has-error').find('.help-block').html('');
                $('#ed_doctor').html($('#consultant-ed_doctor option:selected').text());
                $('.replace-preview').addClass('hide');
                $('#div-doctor').removeClass('hide');
            }
            if(!$('#consultant-commentdoctor').val()){
                $('.field-consultant-commentdoctor').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-commentdoctor').removeClass('has-error').find('.help-block').html('');
                $('#comment').html($('#consultant-comment').val());
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'replace-location') {
            flag = true;
            
            if(!$('#consultant-ed_location option:selected').val()){
                $('.field-consultant-ed_location').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_location').removeClass('has-error').find('.help-block').html('');
                $('#ed_location').html($('#consultant-ed_location option:selected').text());
                $('.replace-preview').addClass('hide');
                $('#div-location').removeClass('hide');
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'referral_mail') {
            flag = true;
            
            if(!$('#consultant-subject').val()){
                $('.field-consultant-subject').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-subject').removeClass('has-error').find('.help-block').html('');
                $('#subject').html($('#consultant-subject').val());
            }
            if(!$('#consultant-comment').val()){
                $('.field-consultant-comment').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-comment').removeClass('has-error').find('.help-block').html('');
                $('#comment').html($('#consultant-comment').val());
            }
            if($('#consultant-mail').is(':checked')){
                $('#mail').html('Yes');
            } else {
                $('#mail').html('No');
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'referral_mail-referral') {
            flag = true;
            
            if(!$('#patientreferral-subject').val()){
                $('.field-patientreferral-subject').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patientreferral-subject').removeClass('has-error').find('.help-block').html('');
                $('#subject').html($('#patientreferral-subject').val());
            }
            if(!$('#patientreferral-comment').val()){
                $('.field-patientreferral-comment').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patientreferral-comment').removeClass('has-error').find('.help-block').html('');
                $('#comment').html($('#patientreferral-comment').val());
            }
            if($('#patientreferral-mail').is(':checked')){
                $('#mail').html('Yes');
            } else {
                $('#mail').html('No');
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'ed_referral_mail') {
            flag = true;
            
            if(!$('#consultant-subject').val()){
                $('.field-consultant-subject').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-subject').removeClass('has-error').find('.help-block').html('');
                $('#subject').html($('#consultant-subject').val());
            }
            if(!$('#consultant-ed_comment').val()){
                $('.field-consultant-ed_comment').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-ed_comment').removeClass('has-error').find('.help-block').html('');
                $('#ed_comment').html($('#consultant-ed_comment').val());
            }
            if($('#consultant-mail').is(':checked')){
                $('#mail').html('Yes');
            } else {
                $('#mail').html('No');
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'assesment') {
            flag = true;
            
            if(!$('#consultant-current_medical_issue').val()){
                $('.field-consultant-current_medical_issue').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-current_medical_issue').removeClass('has-error').find('.help-block').html('');
                $('#handover-current_medical_issue').html($('#consultant-current_medical_issue').val());
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'recommendation') {
            flag = true;
            
           if(!$('#consultant-handover_comment').val()){
                $('.field-consultant-handover_comment').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-consultant-handover_comment').removeClass('has-error').find('.help-block').html('');
                $('#handover-comment').html($('#consultant-handover_comment').val());
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'present_user') {
            flag = true;
            if($('#handovermeeting-present_user_ids').val() == null){
                $('#error-present_user_ids').removeClass('hide');
                flag = false;
            } else if($('#handovermeeting-present_user_ids').val() != null && $('#handovermeeting-present_user_ids').val()){
                 $('#error-present_user_ids').addClass('hide');
                 console.log($(element).find('span'));
                $('#present_user_ids').html($('button.selectpicker ').attr('title'));
            }
            
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'discharge_data') {
            flag = true;
                     
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'handover_data') {
            flag = true;
                     
            if(flag) validatorCallBack(element,  true);
        }
        
        if(requiredFiels == 'completing_user') {
            flag = true;
            console.log($('#handovermeeting-user_id').val());
             if(!$('#handovermeeting-user_id').val()){
                $('#error-user_id').removeClass('hide');
                flag = false;
            } else {
                $('#error-user_id').addClass('hide');
                $('#user_id').html($('#handovermeeting-user_id option:selected').text());
            }       
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'from_nurse') {
            flag = true;
            console.log($('#patient-ed_nurse_from option:selected').val());
            if(!$('#patient-ed_nurse_from option:selected').val()){
                $('.field-patient-ed_nurse_from').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patient-ed_nurse_from').removeClass('has-error').find('.help-block').html('');
                $('#review-ed_nurse_from').html($('#patient-ed_nurse_from option:selected').text());                
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'to_nurse') {
            flag = true;
            
            if(!$('#patient-ed_nurse_to option:selected').val()){
                $('.field-patient-ed_nurse_to').addClass('has-error').find('.help-block').html('Field cannot be blank.');
                flag = false;
            } else {
                $('.field-patient-ed_nurse').removeClass('has-error').find('.help-block').html('');
                $('#review-ed_nurse_to').html($('#patient-ed_nurse_to option:selected').text());                
            }
            if(flag) validatorCallBack(element,  true);
        }
        if(requiredFiels == 'patient_list') {
            flag = true;
            
            if($("#form-handover-select input:checkbox:checked").length < 1){
                $('#list_error').removeClass('hide');
                flag = false;
            } else {
                $('#list_error').addClass('hide')                
            }
            if(flag) validatorCallBack(element,  true);
        }
    }
    
    if(!requiredFiels) {
        validatorCallBack(element,  true);
    }
   
}
