/*
* Bootstrap Growl - Notifications popups
*/ 
function notify(message, type){
   $.growl({
       message: message
   },{
       type: type,
       allow_dismiss: false,
       label: 'Cancel',
       className: 'btn-xs btn-inverse',
       placement: {
           from: 'top',
           align: 'right'
       },
       delay: 2500,
       animate: {
               enter: 'animated bounceIn',
               exit: 'animated bounceOut'
       },
       offset: {
           x: 20,
           y: 85
       }
   });
};
var ajaxCallbacks = {
    'saved': function (response) {
        jQuery('button[type="submit"]').removeAttr('disabled');
        
        if(response.success) {
            notify('Successfully Saved', 'success');
        } else {
            notify('There is some issues right now. Please try later.', 'warning');
        }
    },
  
    
    'saved-comment': function (response) {
        jQuery('button[type="submit"]').removeAttr('disabled');
        
        if(response.success) {
            notify('Successfully Saved', 'success');
            
            jQuery('#create-comment').modal('hide');
            jQuery('#create-comment-form').trigger("reset");
            
            //loadActionItem();
            jQuery('#comments-' + response.core_id).append(response.html);
            attachDatePicker();
            preFillTextBoxes();
            attachUploader();
        } else {
            notify('There is some issues right now. Please try later.', 'warning');
        }
    },
    
    'saved-consultant' : function (response) {
        jQuery('button[type="submit"]').removeAttr('disabled');
        
        if(response.success) {
            notify('Successfully Saved', 'success');
            jQuery('#consultant-model').modal('hide');
            jQuery('#create-consultant-form').trigger("reset");
            location.reload();
        } else {
            notify('There is some issues right now. Please try later.', 'warning');
        }
    }
    
};

function submitFormHandle(e) {
    jQuery('button[type="submit"]').attr('disabled','disabled');
    e.preventDefault();
    e.stopImmediatePropagation();
    var
            $link = $(e.target),
            callUrl = $link.attr('action'),
            formId = $link.attr('id'),
            onDone = $link.data('onDone'),
            onFail = $link.data('onFail'),
            onAlways = $link.data('onAlways'),
            ajaxRequest;

   var form = $('#' + formId);
    if(form.find('.has-error').length) {
         console.log(formId,form.find('.has-error').length);   
        return false;
    } 
    //return false;

    ajaxRequest = $.ajax({
        type: "post",
        dataType: 'json',
        url: callUrl,
        data: (typeof formId === "string" ? form.serializeArray() : null)
    });

    // Assign done handler
    if (typeof onDone === "string" && ajaxCallbacks.hasOwnProperty(onDone)) {
        ajaxRequest.done(ajaxCallbacks[onDone]);
    }

    // Assign fail handler
    if (typeof onFail === "string" && ajaxCallbacks.hasOwnProperty(onFail)) {
        ajaxRequest.fail(ajaxCallbacks[onFail]);
    }

    // Assign always handler
    if (typeof onAlways === "string" && ajaxCallbacks.hasOwnProperty(onAlways)) {
        ajaxRequest.always(ajaxCallbacks[onAlways]);
    }
    
    return false;
}


function popupHandler(e) {
    
}

function loadCoreFeature() {
    //$('#core-feature-items').append(coreFeatureUrl);
    $.get(coreFeatureUrl, {'single' : arguments.length}, function(response){
        $('#core-feature-items').append(response);
        preFillTextBoxes();
        preFillCollapse();
        attachDatePicker();
        attachUploader();
    });
}

function attachUploader() {
    $(".action-uploader").fileinput({
        showRemove:false,
        showUpload: false,
        showPreview: false,
        showCaption: false,
        showUploadedThumbs: false,
        allowedFileExtensions: ['jpg', 'gif', 'png', 'gif', 'txt', 'doc', 'docx', 'pdf', 'xlsx', 'pptx'],
        uploadUrl: actionAttachmentUrl, // server upload action
        dropZoneEnabled: false,
        uploadAsync: false,
        browseIcon: '<i class="md md-file-upload"></i>',
        browseLabel: ' Upload',
        uploadExtraData: function() {
            return {
                'actionId' : this.$element.attr('actionid')
            };
        }
    }).on("filebatchselected", function(event, files) {
        // trigger upload method immediately after files are selected
        $(this).fileinput("upload");
    }).on('filebatchuploadsuccess', function(event, data) {
        $('#attchment-list-' + $(this).attr('actionid')).append('<li><a target="_blank" href="' + attachmentUploadUrl + data.response.name + '">' + data.response.original + '</a></li>');
        return true;
    });
}

function fnshowradio(obj){
    //alert();
    if(obj.checked == true){
        $('.field-patients-referred_by_gp').show();
    }else{
        $('#referred_by_gp').val('1');
        $('.field-patients-referred_by_gp').hide();
        $('#patients-referred_by').val('');
        $('.field-patients-referred_by').hide();
    }
}

function fnreferred_by(obj){
    //alert();
    if(obj.value == '0'){
        $('.field-patients-referred_by').show();
    }else{
        $('#patients-referred_by').val('');
        $('.field-patients-referred_by').hide();
    }
}
function fnshowradioreferral(obj){
    //alert();
    if(obj.checked == true){
        $('.field-patients-referral_by_gp').show();
    }else{
        $('#referral_by_gp').val('1');
        $('.field-patients-referral_by_gp').hide();
        $('#patients-referral_by').val('');
        $('.field-patients-referral_by').hide();
    }
}

function fnreferral_by(obj){
    //alert();
    if(obj.value == '0'){
        $('.field-patients-referral_by').show();
    }else{
        $('#patients-referral_by').val('');
        $('.field-patients-referral_by').hide();
    }
}

$(document).ready(function(){
    /* code snippet to fix select2 issue in modal popup */
    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
    var gp = $('#referred_by_gp').val();
    var rp = $('#referral_by_gp').val();
    //alert(gp);
    if(gp == 0){
        $('.field-patients-referred_by_gp').show();
        $('.field-patients-referred_by').show();
    }
    if(rp == 0){
        $('.field-patients-referral_by').show();
        $('.field-patients-referral_by_gp').show();
    }
});